# Sensori
e' un oggetto basato su un materiale che cambia le sue proprieta' elettriche (per esempio la sua resistenza) al variare delle condizioni ambientali a cui e' sottoposto.
I sensori piu semplici possono essere "letti" controllando il cambiamento di queste loro proprieta (misurandone per esempio la capacita o la resistenza).
Nel caso di sensori piu complessi questi possono addirittura esporre i risultati come informazione digitale (bitstream) secondo qualche tipo di protocollo ad esempio un treno di impulsi