# Circuiti RC

^2e5c5f

Nella loro forma piu semplice sono composti da una sola resistenza e da un solo condensatore accoppiati in serie/parallelo in due combinazioni:
- *passa-basso* o *integratore*
- *passa-alto* o *differenziatore*

Se all'interno vengono iniettati segnali d'onda sinusoidali, all'uscita si avranno segnali con la stessa forma d'onda (anche se non in fase) e con diverso gradi di attenuazione in funzione della frequenza:
- passa alto fa passare, e quindi non attenua i segnali ad alta frequenza
- passa basso fa l'opposto
Quando invece in ingresso viene applicata una tensione variabile nel tempo di forma quadra inn uscita si otterranno forme d'onda molto diverse dall'originale, in questi casi i nomi dei circuiti RC diventano *integratore* e *differenziatore*
## integratore
- il condensatore impiega del tempo per caricarsi quindi all'inizio la forma d'onda della tensione in uscita ha una salita piu dolce di quella in ingresso (se il ciclo dell'onda in ingresso e molto veloce non raggiungera mai la carica massima)
- Durante la discesa dell'onda quadra il condensatore si scarica lentamente creando quindi l'effetto *dente di sega*  
## differenziatore
- durante la fase alta della quadra il condensatore si carica
- ai capi della resistenza si forma un potenziale negativo che man mano che il condensatore si carica tende a 0
- il condensatore si scarica sulla resistenza (invertendo il verso di scorrimento degli elettroni) e viene generata una tensione positiva decrescente nel tempo ai capi di R (discesa dolce della tensione in uscita)
Se il ciclo dell'onda quadra in ingresso e' molto lungo gli impulsi sono sottili e distanti tra loro

## costante di tempo del circuito RC

$$\tau= R\times C$$
$\tau$ e' la quantita di tempo che un condensatore impiega per arrivare al 63.2% della sua capacita, dopodiche impiega un altro $\tau$ per il succsessivo 63.2% della capacita rimanente e cosi via avvicinandosi al limite alla sua capacita totale. ^1e71b2