# differenza fra controllo a loop chiuso e aperto, problemi del loop aperto
La differenza tra i due modelli e' che in quello senza retroazione il comportamento atteso del sistema e' hardcoded, non c'e' nessuna lettura dell'effettiva efficacia dell'azione, mentre nel modello a retroazione e' possibile codificare azioni correttive in funzione dell'output effettivamente ottenuto.
Per esempio posso definire la velocita' di rotazione di motore in un modello a loop aperto come 
$\text{Giri} = 500\times\text{tensioneIngressoInVolt}$
Questo modello pero' non tiene per esempio conto del carico applicato al motore o di altri fattori esterni. 
Possiamo correggerlo in due modi:
* continuare sulla strada del loop aperto inserendo nella formula anche tutte le cose esterne ma non sempre e' possibile
* leggere il numero di giri effettivo e correggere il tiro in modo empirico

La prima purtroppo non sempre e' possibile da applicare nella vita vera in cui non abbiamo il controllo su tutti gli agenti esterni
