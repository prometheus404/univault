# differenze fra i vari tipi di memoria (RAM/ROM/flash/EPROM/sequenziale/casuale/SDRAM/DRAM/DDR/etc.)
## RAM
e' volatile (ma non sempre) e ad accesso casuale. Generalmente veloce ma costosa. E' in grado di garantire lo stesso tempo di accesso a qualsiasi sua area interna, generalmente associata alla memoria di sistema. Viene suddivisa in tre sottocategorie
### SRAM
Static RAM, Static perche non necessita di refresh come le RAM normali ma comunque volatile. Molto veloce ma molto costosa quindi utilizzata generalmente per le cache. Ottime prestazioni dal punto di vista energetico e termico
### DRAM
Dynamic ram, Richiede refresh sono asincrone ovvero non devono essere necessariamente sincronizzate con il clock del sistema.
### SDRAM
Variante sincrona delle DRAM (S sta per Syncronous), la cui velocita e' quindi collegata al clock dell'elaboratore. Ampiamente utilizzata nei sisteemi desktop (le DDR sono SDRAM). Da tempo viene anche inclusa in alcuni sistemi embedded

## Memoria a accesso diretto
Persistente e generalmente lenta ma molto piu economica delle ram. Offre tempo variabili di accesso a seconda della zona di memoria che si vuole leggere. Usata per memorie di massa (per esempio hard disk)
## Memoria ad accesso sequenziale
Molto lenta, persistente, molto economica, tempo di accesso molto variabile. Usata nei sistemi di backup a nastro.
## ROM
Sono memorie in sola lettura. In principio si trattava i memorie hard-wired direttamente sul silicio e non riscrivibili successivamente. In seguito si sono evolute in diverse forme aggirando almeno in parte il limite della impossibilita di riscrittura. Sono molto veloci in lettura ma decisamente molto lente in riscrittura. Il loro uso e limitato ad usi di avvio del sistema e funzionalita base. In molti prodotti nonostante siano riscrivibili sono protette da scrittura.
### PROM (Programmable ROM)
sono memorie programmabili solo una volta tramite l'uso di corrente elettrica ad alto potenziale.
### EPROM (Erasable PROM)
sono riscrivibili tramite azzeramento con luce ultravioletta ma solo un numero limitato di volte (in molti casi un migliaio)
### EEPROM (Electrically EPROM)
sono cancellabili e riscrivibili elettricamente un numero limitato di volte.
## Flash Memory
[[Flash-based SSD]]