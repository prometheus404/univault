# MMU
Memory Management Unit. Si occupa principalmente di tre compiti:
- Traduzione indirizzi virtuali
- Protezione della memoria (evita accesso ad aree di memoria sbagliate)
- Gestione della cache