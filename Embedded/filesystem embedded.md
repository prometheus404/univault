# quali tipi di filesystem vengono usati nell'embedded? pregi e difetti
Siccome le memorie tipiche in ambito embedded sono memorie flash e visto che spesso i controller vengono rimossi per motivi pratici o economici non si possono usare filesystem ideati per filesystem a blocchi, per questo ci sono come alternative:
- *jffs*: un filesystem a log disegnato per lavorare su NAND ha dei limiti nel fatto che deve eseguire le fasi di clean ogni volta che viene montato
- *yaffs*: simile a jffs con il quale condivide alcuni limiti. Molto usato dai produttori dei primi smartphone Android
- *ubifs* introduce molte ottimizzazioni legate al wear leveling con minori tempi di accesso in fase di mount