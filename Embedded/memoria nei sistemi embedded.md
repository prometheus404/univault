# descrivere la gestione della memoria in un sistema embedded
La gestione della memoria e' particolarmente importante nei sistemi embedded perché questi sono dotati di una memoria notevolmente più limitata. Per questo motivo e' bene studiare e conoscere i *tipi di dato* per risparmiare spazio dove possibile. Bisogna inoltre fare attenzione all'*utilizzo di librerie* in quanto non sempre e' facile sapere quanta memoria utilizzeranno. Un altro modo in cui viene usata la memoria e' *invocando funzioni*, infatti devo allocare dello spazio nello stack e rischio di sfondare lo stack (stack overflow). Infine dobbiamo tenere conto anche della *memoria di programma* e dobbiamo stare attenti in fase di compilazione alla dimensione occupata da quest'ultima.
Molti sistemi embedded inoltre non hanno a disposizione una *MMU*, senza la quale non si ha alcuna protezione della memoria ne' possibilita di gestire memoria virtuale. Per questo per esempio bisogna fare attenzione a:
* accesso ad array (il programma non controlla se l'indice viene sforato o no)
* sforamento stack/heap: riempire lo stack oltre il suo limite non scatena nessun avviso 
in entrambi i casi rischio di sovrascrivere aree di memoria importanti ai fini del programma.
# argomentare validità della ricorsione in ambito embedded, vantaggi e svantaggi
Siccome invocare funzioni alloca spazio sullo stack finche questa non ha finito l'esecuzione non e' consigliabile fare uso di funzioni ricorsive in quanto puo essere complesso assicurarsi che non sfondino lo stack
