# Arduino
Nasce a Ivrea intorno al 2005 come progetto di prototipazione elettronica a basso costo per fini didattici. La sua caratteristica principale è che è open hardware e sul mercato esistono kit che permettono di personalizzare la board di sviluppo o di assemblarla interamente.

Tra i motivi del grande successo di Arduino nel campo hobbistico c'è anche l'ambiente di sviluppo derivato da wiring. Sia l'ide che il linguaggio di programmazione sono pensati e sviluppati al fine di risultare estremamente semplici mascherando il più possibile gli aspetti di basso livello fornendo una grande accessibilità allo sviluppo.

## Punti di forza

^5ef89f

- licenze libere che lo rendono un ambiente a bassissimo gap economico di ingresso e altissimo livello di community
- architettura KISS (Keep It Simple Stupid) sia dal punto di vista hardware (processore 8 bit, poca rom/ram, no MMU, no SO) che software (linguaggio semplificato ma familiare e processo di sviluppo mascherato dall'interfaccia utente)
- linguaggio di programmazione con sintassi molto simile a C e Java mentre vengono mascherati i costrutti più complessi come oggetti e puntatori
- produttori e community hanno sviluppato una pletora di librerie ed e' raro ricorrere a qualcosa di più complicato di un "download library" dalla GUI per far interagire una periferica con Arduino
- dotato di pin di I/O sia digitali (anche in [[PWM]]) che analogici disposti secondo una collocazione che col tempo e' diventata uno standard de facto
## Punti di debolezza

^96a476

- la legalità e l'enorme facilita di riproduzione dell'hardware e del software ha creato un'amplia disponibilità di produttori ma non sempre con gli stessi standard qualitativi
- il gran numero di appassionati rende molto florida la disponibilità di librerie... anche troppe sia a causa del sano forking dell'open source sia a causa dei produttori che devono realizzare per forza le proprie librerie hardware senza rispettare standard esistenti

## Struttura di uno sketch
nella forma minimale consta di due sole funzioni: `setup()` e `loop()`
### Setup
all'interno di questa funzione vanno messe tutte le istruzioni di configurazione iniziale da fare una volta sola ^0655fa
### Loop
qui vanno messe tutte le istruzioni vere e proprie del programma che si vuole produrre, queste verranno riprodotte appunto in loop ^72a638

### Tipi di dato

^9eb6ed

- `boolean` vale solo true o false, occupa un byte in memoria
- `char` un singolo carattere, occupa un byte e ha un range di valori da -127 a +127 ma i valori negativi non hanno rappresentazione ASCII
- `unsigned char` char senza segno va da 0 a 255 (ma meglio usare byte)
- `byte` numero a 8 bit, da 0 a 255
- `int` intero con segno, 16 bit sulle ATMega e 32 sulle SAMD
- `unsigned int` la versione senza segno di int
- `word` analogo a int
- `long` intero con segno a 32 bit
- `unsigned long` intero senza segno a 32bit
- `short` intero con segno a 16 bit
- `float` numeri decimali in virgola mobili occupa 4 byte
- `double` su quasi tutte le piattaforme sinonimo di float, su altre rappresenta numeri in virgola mobile in doppia precisione occupando 8 byte
- `array` è un vettore di variabili omogenee (con lo stesso tipo) con accesso posizionale occhio che Arduino non protegge da errori di programmazione nell'accesso ad un array ^ef4e7e
- `char array` stringa di testo di dimensione fissa
- `String` classe stringa. Meglio non abusarne perché comporta allocazione non sempre prevedibile della memoria

### Operatori

^494716

Si dividono in categorie:
#### Aritmetici
- `=` assegnamento
- `+` somma
- `-` sottrazione
- `*` prodotto
- `/` divisione
- `%` modulo

#### Confronto
- `= =` uguale a 
- `! =` diverso da
-  `<` minore di
- `>` maggiore di
- `< =` minore o uguale
- `> =` maggiore o uguale
#### Booleani
- `&&` and
- `||`or
- `!` not
#### Bitwise
- `&` and bit a bit
- `|`or bit a bit
- `^` xor bit a bit
- `~` not bit a bit
- `<<` shift a sinistra
- `>>` shift a destra
#### Composti
- `++` incremento
- `--` decremento
- `+=` somma
- `-=` sottrazione
- `*=` prodotto
- `/=` divisione
- `%=` modulo
- `&=` and bit a bit
- `|=`or bit a bit

### Direttive di compilazione
Si trovano frequentemente nei sorgenti, si chiamano cosi perché è proprio la fase precedente la compilazione del codice che le risolve prima di passare il sorgente al compilatore vero e proprio. 
^5f9460
- `#include<libreria.h>` in questo caso il precompilatore sostituisce questa riga con tutto il contenuto del file indicato tra `< >` . Il sorgente trasformato viene poi passato al compilatore in modo automatico e trasparente allo sviluppatore
- `#define NOMECOSTANTE valore` il precompilatore sostituisce tutte le occorrenze di `NOMECOSTANTE` con `valore`

### Interrupt

^1e211b

In uno sketch se si vuole gestire un interrupt bisogna
- definire almeno una funzione "speciale" chiamata ISR(Interrupt Service Routine) che non è altro che una semplice funzione `void` e senza parametri in ingresso. Inoltre deve essere più corta possibile
- agganciare tramite `attachInterrupt()` la funzione a uno o più interrupt per attivare la reattività dello sketch al segnale di ingresso

Da questo punto in  poi lo sketch si comporterà normalmente e, all'arrivo di un segnale su uno dei piedini agganciati ad una ISR l'esecuzione verrà sospesa per dare seguito alla funzione ISR. Al ritorno da questa funzione l'esecuzione normale verrà ripresa nello stato originale.
Le funzioni di libreria per gli interrupt sono:
- `attachInterrupt(interrupt, ISR, mode)` per agganciare una funzione a un segnale. Mode specifica quando chiamare la funzione: se il segnale sale (RISING), se il segnale scende (FALLING), quando il segnale cambia (CHANGE) o quando il segnale è in uno stato (LOW o HIGH)
- `detachInterrupt()`
- `noInterrupts()`
- `interrupts()`
