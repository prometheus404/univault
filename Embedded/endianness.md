# Endiannes
I calcolatori utilizzano la logica binaria memorizzando e utilizzando dati sotto forma di bit. Gruppi di 8 bit prendo il nome di byte.

0xBEEF viene rappresentato in binario come 101111110 11101111
Gli 8 bit a sinistra sono i bit piu significativi mentre gli 8 a sinistra sono i piu significativi.

Il calcolatore puo seguire due tipi di endiannes a seconda dell'ordine in cui legge e memorizza i byte nella memoria.
- Little Endian: i byte sono memorizzati a partire dal byte meno significativo (il nostro numero viene memorizzato come EF BE)
- Big Endian: i byte sono memorizzati a partire dal byte piu significativo (il nostro numero viene rappresentato come BE EF)
