# TCP-IP
|        TCP/IP        |
|:--------------------:|
|     Applicazione     |
|      Trasporto       |
|         Rete         |
| Network Access Layer |

Si comporta come lo stack di rete ISO/OSI ma i livelli 5,6,7 sono fusi in un unico livello Applicazione dove possiamo trovare protocolli come per esempio HTTP, FTP, ecc. A livello di trasmissione si utilizzano prevalentemente i protocolli TCP e UDP. Infine i livelli 2 e 1 vengono fusi insieme in un unico Network Access Layer