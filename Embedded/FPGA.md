# FPGA
Acronimo di Field Programmable gate array. Si tratta di un insieme variabile per numero di porte logiche con connessioni programmabili.
In realta in molti casi e' probabile che si usino logic block ovvero l'unione funzionale di piu porte logiche.

La loro struttura interna, la possibilita di programmazione e il grande numero di connessioni tra i vari blocchi logici le rende estremamente configurabili ma proporzionalmente piu costose rispetto ad una MCU o un Soc, quindi e' particolarmente adatto in fase di progettazione dell'hardware ma non adatto al prodotto finale

puo essere programmato con vari linguaggi di programmazione (definiti HDL)