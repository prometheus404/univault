# Serial Peripheral Interface
Usato per comunicazione seriale sincrona per comunicazioni a breve distanza e a velocita relativamente bassa.
Si basa su 4 segnali il cui nome puo variare:
- SCLK: Serial Clock
- MISO: Master Input Slave Output
- MOSI: Master Output Slave Input
- CS: Chip select

$I^2C$ non e' multimaster e i segnali sono attivo alto
SPI e' molto piu semeplice di I2C: quando il clock viene abilitato dal master, vengono trasmessi i bit in modo seriale. non sono previste condizioni di inizio ne' di fine trasmisione ne' segnali di ack o nack.