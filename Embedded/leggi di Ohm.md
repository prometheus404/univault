# descrivere leggi di Ohm e loro significato
Le leggi di Ohm descrivono la relazione fra tensione e intensita di corrente:
$$ I = \frac{V}{R}$$
ovvero l'intensita di corrente in un conduttore e direttamente proporzionale alla tensione e inversamente proporzionale alla resistenza del conduttore.

Inoltre la Potenza e' descritta dalla seguente formula:
$$P=R\times I^2=V\times I$$
Dalla quale deriva che il calore dissipato da un circuito e' proporzionale al quadrato della corrente che vi scorre. ^4b342b