# General Purpose Input/Output
E' la rappresentazione di un pin e del segnale digitale che passa su di esso. Semplificando si puo immaginare un GPIO come un interruttore digitale, che ha due stati "alto" o "basso" in base alla presenza di una determinata tensione sun un pin. Piu questa tensione e' vicina a una determinata tensione di riferimento $V_{ref}$  (da 1.8 a 5v tipicamente a seconda del modello), quando la tensione sul pen e' prossima a un intorno di $V_{ref}$ il GPIO e' nello stato alto, se e' in un intorno di 0 e' in uno stato basso (questo per i gpio in modalita active-high).
In un SoC i pin non utilizzati da altri device vengono allocati come GPIO.
Le modalita' di utilizzo dei GPIO sono:
- *Input*: Usato per leggere valori binari dal pin
- *Output*: utilizzato per scrivere valori binari su un pin
- *IRQ*: modalita di input in cui la variazione di valore sul pin viene utilizzata per scatenare un interrupt.
I GPIO possono essere usati per il monitoraggio di presenza di unaa scheda SD/MMC, il controllo di led e  pulsanti.,...