# definire categorie "real time"
con real time si intende un sistema composto da hardware e software in grado di fornire una risposta o eseguire una operazione secondo tempistiche certe (indicate con il termine inglese di "Time Constraints")
- *Hard real-time*: il mancato rispetto di una deadline corrisponde al totale fallimento del sistema
- *Soft real-time*: il sistema non fallisce in caso di mancate deadline ma degrada all'aumentare delle tempistiche non rispettate
- *Firm real-time*: il sistema può tollerare un numero molto limitato di mancate deadline, ma fallisce in caso superino un certo limite

Per rispettare queste deadline i sistemi real-time sono dotati di un hardware compatto e affidabile (a scapito delle prestazioni) e un software molto ridotto e ottimizzato su singoli compiti specifici (sistemi operativi RTOS)
