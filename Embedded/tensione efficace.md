# Tensione efficace
Siccome la tensione di un segnale varia nel tempo, la tensione efficace $V_{eff}$ e' l'integrazione della potenza istantanea nel tempo. Nel caso di un segnale perfettamente sinusoidale questa si calcola tramite la formula:
$$V_{eff}=\frac{V_{picco}}{\sqrt{2}}$$
Nel caso della [[PWM]] basta invece:
$$V_{eff}=V_{picco}\times DurataPicco$$
