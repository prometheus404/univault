In generale, sia per quanto riguarda i SoC che per quanto riguarta le MCU, si tende a integrare piu componenti possibili all'interno del chip. Cio nonostante il sono chip sarebbe difficilmente utilizzabile per prototipare e sviluppare il prodotto finale. Oltre alla comune gestione energeteca molti dispositivi necessitano di storage o di avere alcune porte di I/O o ancora di sensori.

Per questo i Produttori di chip forniscono delle board di "evaluation" o "development" a seconda dei casi
Una evaluation board e' solitamente realizzata su un PCB di piccole dimensioni che dispone di tutti o quasi i collegamenti fisici per l'hardware presente nel MCU o nel SoC

Una validation Board invvece e' una board di grande dimensioni usata per validare un SoC o un MCU, e' dotata di molte porte di debug, pin e test point . Lo scopo e' in genere effettuare test e verifica per evidenziare la presenza o meno di errori di progettazione all'interno del chip. Raramente esce sul mercato.