# ISA
e' il modello astratto di un'architettura e comprende la tipologia di dati che puo trattare, i registri, la modellazione di input/output e le istruzioni macchina. Da un singolo ISA possono derivare piu implementazioni, ovvero piu processori possono condividere la stessa ISA.

In base alla tipologia di ISA e' possibile distinguere 4 differenti tipi di architetture:
- *CISC* Complex Instruction Set Computing
- *RISC* Reduced Instruction Set Computing
- *VLIW* Very Long Instruction Word
- *EPIC* Explicitly Parallel Instruction Computing

VLIW e EPIC sono architetture ideate per raggiungere ottimi livelli di parallelizzazione  nella esecuzione delle istruzioni

CISC come dice il nome usa molte istruzioni low-level di lunghezza variabile e che possono richiedere vari cicli di clock per essere eseguite. Fornisce un linguaggio macchina più completo e apparentemente più facile da programmare ma e' molto costoso in termini di silicio. alcuni esempi sono (Intel 8080, x86).

RISC usa un instruction set semplificato con istruzioni semplici, molto ottimizzate e in grado di essere eseguite con pochi cicli di clock. Il linguaggio macchina e' molto più a basso livello quindi necessitano di compilatori più elaborati.
