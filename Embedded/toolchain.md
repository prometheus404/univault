# Toolchain
Composta da:
- Compilatore: un software che trasforma i codice sorgente nel cosiddetto codice oggetto
- Linker: collega i vari codici oggetto prodotti dal compilatore e le librerie utilizzate trasformandoli in codice eseguibile
- Librerie: librerie necessarie alla fase di linking, in particolare per quanto riguarda le librerie d'interfacciamento con il sistema operativo
- Debugger: opzionale in alcune toolchain. E' lo strumento usato per eseguire il debug software dei software compilati

Una toolchain inoltre deve essere compilata per l'architettura selezionata, in quanto in generale non e' possibile utilizzare la medesima toolchain su architetture differenti. Per questo motivo si fa una distinzione in:
- Toolchain Native: e' una toolchain compilata per un sistema con la medesima architettura hardware software del target (nel caso dei sistemi pc e' la norma ma e' abbastanza nuova nei sistemi embedded). Piu semplice per i sistemi di compilazione ma allo stesso tempo richiede una notevole potenza compitazionale
- Cross-Toolchain: costituita dagli stessi elementi di una toolchain nativa compilati per una architettura ospite ma configurata per produrre binari per una architettura differente. Questo e' l'approccio piu comune sui sistemi embedded. Richiede piu lavoro di configurazione ma permette di sfruttare sistemi dotati di maggiore potenza per produrre i binari inoltre non ha come prerequisito un sistema gia funzionante per l'architettura target.