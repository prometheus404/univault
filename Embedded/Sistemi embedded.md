# Sistemi embedded
## Definizione di sistema embedded
Con la terminologia Sistema Embedded si tende a indicare l'insieme composto da hardware e softwase dedicato a specifici scopi i cui elementi siano tutti quanti integrati e incorporati.  E' una definizione molto generica che comprende una quantita elevata di elementi molto diversi tra loro sia dal punto di vista dell'hardware, sia dal punto di vista software, sia dal punto di vista delle funzionalita. ^2eaf7b

# Panoramica generale sui sistemi embedded, classi di device, caratteristiche, ordini di grandezza (dimensioni, potenze, consumi, ecc.), ambiti di utilizzo

^05a4e9

Esistono tre principali classi di device:
* PLC (Programmable Logic Controller) 
* Microcontrollori
* SoC (System on Chip)

### PLC
I PLC sono dispositivi relativamente semplici pensati per lavorare su sistemi  di automazione e non destinati alla produzione su vasta scala; In molti casi vengono programmati individualmente per svolgere una sola funzione specifica. Sono generalmente dotati di una buona connettivita I/O per consentire la l'interazione con sensori di vario genere. Per quanto i vari  prodotti sul mercato condividono i concetti di base di programmazione dei PLC tali dispositivi non sono interscambiabili o compatibili tra di loro. ^9d91d5
### MCU
I microcontrollori nascono dall'idea di fornire un sistema hardware completo all'interno di un singolo chip. Raramente su questi dispositivi e' possibile sfruttare dei sistemi operativi simili a quelli utilizzati in ambito domestico sia per le scarse performance computazionali (clock in MHz invece che GHz) sia per la scarsa memoria(misurata in kb). Tale carenza dal punto di vista delle prestazioni e' ripagata da ottimi valori di consumo energetico(molto inferiori a 1W contro i 30W di una cpu general purpose) e da eccezionali fattori di efficienza termica oltre che da un costo per unita' piu' assai inferiore. Buoni per compiti poco intensivi dal punto di vista computazionale tipo misurazioni ambientali/ controllo regolatori.
### SoC
I SoC rappresentano l'ultimo stadio evolutivo dei sistemi embedded. Rappresentano una vasta gamma di prodotti dalle maggiori potenze computazionali rispetto ai PLC e alle MCU e dotazioni piu ricche (per esempio  contenendo CPU e persino RAM a volte). La definizione di System on Chip deriva dal fatto che in molti casi si tratta di singole unita che al loro interno contengono tutte o quasi le componenti del sistema. Sono usati praticamente ovunque (ambito militare, domotica, infotainment,...). Recentemente la distinzione tra MCU e SoC si e' fatta piu sottile. ^2e860a