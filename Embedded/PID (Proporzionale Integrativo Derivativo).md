# spiegare tecnica PID per controllo a loop chiuso
Una delle tecniche per controllare un sistema a loop chiuso, modella la funzione di trasferimento come una somma pesata di tre componenti dipendenti dalla funzione di errore $e(t)$: ovvero la differenza tra lo stato desiderato e quello misurato. Nel caso di un motore per esempio $$e(t)=\text{GiriMotore} - \text{RegimeDesiderato}$$
Le tre componenti del PID rappresentano:
* La distanza assoluta dallo stato desiderato
* La storia dell'evoluzione dello stato del sistema
* La velocita' di variazione dello stato

La forma generale della funzione e' la seguente:
$$ P\cdot e(t)+ I\cdot \int_0^t {e(t')dt'}+D\cdot\frac{de(t)}{dt} $$
dove P, I e D sono i coefficienti di peso che stabiliscono l'apporto delle tre componenti alla formula. Questi valori vengono settati a valori sensati in fase di progettazione e vengono successivamente aggiustati per ottenere il comportamento desiderato.

I principali rischi di questo approccio sono:
* mancato raggiungimento dell'obiettivo (se per esempio P e' troppo basso o addirittura sbagliato di segno)
* oscillazione (correzione troppo forte, per esempio se P e' troppo elevato)
