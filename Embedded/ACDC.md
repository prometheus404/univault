# Corrente continua e alternata
La corrente esiste in due forme: continua e alternata. Nel caso della prima gli elettroni si muovono da un polo all'altro sempre nella stessa direzione, mentre nella corrente alternata cambiano verso (andando avanti e indietro secondo una frequenza costante)

## Produzione 
Una prima differenza notevole tra i due tipi di corrente e' la produzione:
* La corrente alternata viene prodotta meccanicamente muovendo un insieme di conduttori all'interno di un campo magnetico creando una corrente per induzione elettromagnetica (puo anche essere trasformata da corrente continua tramite inverter)
* La corrente continua viene prodotta chimicamente (batterie), per accumulo elettrostatico e anche meccanicamente da dinamo (in questo caso pero e' corrente pulsante)
## Immagazzinamento
* Nel caso della corrente alternata questa non puo essere immagazzinata se non trasformandola in energia di altro tipo (per esempio potenziale nei bacini idroelettrici o in corrente continua)
* La corrente continua invece puo essere immagazzinabile negli accumulatori (batterie) o nei condensatori
## Trasformabilita
* La corrente alternata e' facilmente trasformabile tramite i trasformatori: componenti elettronici abbastanza semplici formati da  due avvolgimenti di filo vicini in modo che il campo elettromagnetico prodotto da una possa influenzare l'altra
* La corrente continua non e' invece facilmente trasformabile se non tramite circuiti a semiconduttore che pero' si scaldano molto al crescere della tensione e della corrente
La trasformabilita incide sulla trasmissibilita in quanto per via delle [[leggi di Ohm]] $P=R \times I^2$ e quindi per trasportare piu corrente e piu conveniente alcare la tensione