# Componenti
## Interruttori
Servono per interrompere il flusso di corrente a comando e può essere chiuso (se fa passare corrente) o aperto (se invece la blocca). Gli interruttori dotati di una molla di ritorno vengono chiamati pulsanti e esistono in due versioni: 
- normalmente aperto
- normalmente chiuso
## Lampadine
Composta da un filamento che diventa incandescente al passare della corrente generando calore e luce

## Rele
Non e altro che un pulsante comandato da un elettromagnete. Viene tipicamente usato per comandare un flutto elettrico importante usando un flusso elettrico con una tensione/corrente più bassa. Inoltre garantisce l'isolamento elettrico tra il circuito di controllo e i contatti di uscita

## Resistenze
Una resistenza resiste al passaggio di corrente. Se attraversata da un flusso di correnti provoca una caduta di tensione e una perdita di potenza che viene dissipata in calore

Il modo più semplice per utilizzarla e appunto per generare calore (per esempio forni e phon). Nei circuiti invece le resistenze vengono utilizzate per:
- La loro caratteristica *caduta di tensione* per la [[leggi di Ohm]]. Un esempio particolare e' il partitore di tensione, ovvero uno strumento per suddividere una tensione di alimentazione in varie sotto-tensioni
- La loro capacita di *limitare la corrente* massima applicata ad un circuito/componente
## Condensatori
E' un componente che permette l'accumulo temporaneo di elettroni. Idealmente e' composto da due piastre metalliche conduttive ampie e ravvicinate spesso separate da un dielettrico (materiale isolante). Un condensatore può essere caricato applicando una differenza di potenziale ai capi. Questa carica accumulata su una delle due piastre crea una differenza di potenziale che può essere usata applicando un carico ai capi del condensatore.

Un condensatore si può applicare a valle di un segnale sinusoidale o, meglio, di un segnale pulsante per appiattirlo e renderlo quindi meno variabile nel tempo ^eea1a3

La vera utilità dei condensatori e' nei circuiti RC che permettono la gestione di segnali variabili nel tempo

## LED
sono *diodi* (componenti la cui principale funzione e' permettere il passaggio della corrente in un'unica direzione) particolari in quanto come effetto collaterale producono luce al passaggio di corrente (effetto per il quale trovano poi effettivo utilizzo) e vengono drogati appositamente per aumentare questo effetto collaterale.

## Transistor
sono semiconduttori formati da una Base, un Emettitore e un Collettore. Il transistor si comporta da rubinetto della corrente: la corrente che fluisce tra Emettitore e Collettore e' funzione della corrente che scorre tra Emettitore e Base. La corrente che fluisce tra E e B e inferiore a quella che scorre tra E e C. Una piccola corrente influenza quindi una grande corrente.

In elettronica digitale i transistor si usano in maniera booleana similmente a dei rele a stato solido con il vantaggio di non avere parti meccaniche e di lavorare con tensioni e correnti più basse

Esistono transistor particolari (FET) che invece di essere pilotati da corrente vengono pilotati da un campo elettrico