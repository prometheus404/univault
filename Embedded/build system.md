# Build System
e' linsieme di tool, sistemi di revisione codici sorgente, script, Makefile, eseguibili e librerie atti alla compilazione di pacchetti software  che andranno a comporre il sistema embedded finale. Puo comprendere o meno la toolchain, il kernel e i componenti del root filesystem.
Alcuni esempi:
- LFS: Linux From Scratch tende a indicare tutti gli script e le procedure operative per compilare un sistema operativo GNU/LINUX
- OpenEmbedded: un sistema di build basato su layer e su recipes