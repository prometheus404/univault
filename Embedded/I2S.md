# I2S
$I^2S$ e' l'acronimo di inter-IC Sound ed e' stato progettato per trasportare dati audio e controllare apparati relativi alla registrazione e riproduzione del suono. E' semplice strutturalmente (un collegamento basato su almeno tre linee) e' molto utilizzato tutt'oggi nei dispositivi embedded e mobile.
Le line di bus sono:
- Countinuous Serial Clock (SCK): noto anche come Bit clock line (a ogni impulso corrisponde un bit sulla linea SD)
- Word Select (WS): noto anche come Left-Right clock, in base al valore seleziona il canale destro o il sinistro
- Serial Data: puo opzionalmente avere piu linee ma in generale lo si considera come una linea multiplexed

La frequenza del bit clock equivale al prodotto del sample rate, del numero di canali e del numero di bit per canale