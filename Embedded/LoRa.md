# LoRa (Long Range) e LoRaWAN
Standard di rete wireless molto noti e usati nel contesto IoT. Il primo standard si riferisce al livello fisico, cioè definisce le modalità di trasmissione (modulazione) e le frequenze utilizzate. Il secondo invece descrive lo strato MAC cioè il protocollo vero e proprio di trasmissione. Sostituiscono gli standard WiFi IEEE802.11x per supportare la comunicazione tra sensori e sensor gateway nei contesti in cui servono bassi consumi e lunghe portate. Le principali proprieta di LoRa sono
- bidirezionalita della comunicazione (anche se il pattern predominante è quello in cui i device inviano dato verso i gateway)
- buona resistenza alle interferenze
- consumi ridotti
- comunicazioni a lungo raggio (anche nell'ordine dei km)
- sicurezza delle comunicazioni (sono cifrate)
- funzionamento anche indoor (anche misto out/indoor)
- bassi costi di adozione

La specifica della LoRa Alliance descrive le reti LoRAWAN come organizzate in topologia a "stella di stelle" in cui esistono nodi gateway che fanno da accentratori e instradatori di messaggi fra nodi terminali. Inoltre i gateway fungono opzionalmente da ponte verso reti tradizionali essendo tipicamente connessi tramite TCP/IP. Le comunicazioni tra nodi e gateway vengono effettuate su varie frequenze e a velocità molto variabili. La scelta viene fatta in modo da ottimizzare la portata, durata dell'invio e possibili interferenze con i nodi vicini. Ogni nodo può trasmettere su uno qualsiasi dei canali disponibili per la propria configurazione ma deve sceglierlo a caso per minimizzare il rischio di collisioni e deve rispettare le finestre temporali di trasmissione per il profilo scelto. Per risparmiare energia sui nodi la ricezione è schedulata: il modulo viene alimentato solo quando prevede di poter leggere i messaggi
- Classe A: nodi bidirezionali semplici, ogni invio prevede due finestre temporali di ricezione (può ricevere aggiornamenti solo dopo aver inviato)
- Classe B: nodi bidirezionali con ricezione schedulata con in più la possibilità di definire finestre temporali di ricezione slegate dall'invio di dati. Per questo e' necessario un meccanismo di sincronizzazione degli orologi
- Classe C: nodi a ricezione continua