# Strumenti di misura
si dividono in:
- *passivi* (si limitano a misurare)
- *attivi* (producono un segnale da iniettare in un circuito)

Ogni strumento di misura applicato ad un sistema sotto osservazione ne modifica lo stato (alcuni riescono a influenzare poco il sistema altri devono per  forza farlo, per esempio per misurare la corrente erogabile da una batteria bisogna cortocircuitare anche se per poco facendole estrarre tanta corrente)
## Voltmetro
Misura la tensione tra due punti di un circuito. Si puo usare *in parallelo* (senza staccare nulla o tagliare collegamenti)

### Amperometro
Misura la corrente che scorre in un determinato ramo del circuito. Salvo casi particolari (pinze amperometriche) l'amperometro va inserito *in serie*
_NB_ la resistenza interna di un amperometro è bassissima, quindi non va mai collegato in parallelo ad una fonte di alimentazione perché causerebbe un cortocircuito

## Ohmetro
Misura i valori di resistenza e può essere usato per testare i diodi
A differenza di Voltmetro e Amperometro è uno strumento attivo
## Multimetro
Fa le stesse funzioni di un Voltmetro, un Amperometro e un Ohmetro. Questi ultimi esistono ancora singolarmente solo in contesti in cui serve molta precisione
## Oscilloscopio
Serve ad osservare le forme d'onda di un segnale entrante