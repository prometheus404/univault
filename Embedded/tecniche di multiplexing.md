#  spiegare tecniche di multiplexing (divisione tempo, frequenza), shift register
Il concetto di *multiplexing* e' generico, deriva dal contesto della scienza della trasmissione di informazioni e definisce quali sono le metodologie per condividere un canale per trasportare piu flussi informativi contemporaneamente (pagando in termini di complicazione tecnica, banda passante, rumore, ecc..).
## Divisione di frequenza
I segnali da trasmettere contemporaneamente vengono inviati sul canale usando frequenze diverse, ognuno dei riceventi deve sintonizzarsi sulla frequenza che gli compete come nel caso delle trasmissioni radio.

## Divisione di tempo
I segnali da trasmettere contemporaneamente vengono inviati sul canale in alternanza veloce, il ricevente redistribuisce l'informazione facendo da centralino. Il prezzo di questo risparmio di pin viene pagato in termini di complicazione del software (prima devo comandare il multiplexer e poi posso fare l'azione).
