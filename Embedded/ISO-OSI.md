# ISO-OSI
|ISO-OSI|
|:----------:|
|Applicazione|
|Presentazione|
|Sessione|
|Trasporto|
|Rete|
|Data Link|
|Fisico|

Modello per definire uno stack di rete formato da 7 livelli:
1. *Fisico*: gestione della trasmissione fisica dell'informazione a livello di segnali, tensioni, frequenze e pin dei connettori
2. *Collegamento*: livello che  prepara i dati (sotto forma di pacchetti) alla trasmissione fisica
3. *Rete* livello che permette di indirizzare elaboratori che utilizzano livelli fisici e collegamento diversi. Si occupa anche del routing del pacchetto
4. *Trasporto*: livello che si occupa di stabilire, mantenere e terminare connessioni tra elaboratori
5. *Sessione*: livello che si occupa di stabilire mantenere e terminare connessioni tra le varie applicazioni
6. *Presentazione*: livello che trasforma i dati dell'applicazione in modo standard, gestendone la protezione e la compressione
7. *Applicazione*: il livello a cui i dati vengono generati

Facendo un esempio: i dati inseriti tramite tastiera vengono passati al livello 6 dove sono compressi e crittografati. Il livello 5 fa partire un tentativo di connessione verso l'omologo livello 5 dell'elaboratore bersaglio. Per fare ciò devono essere identificati gli elaboratori a livello 4 e deve essere indirizzata la comunicazione a livello 3. Nel livello 2 le informazioni vengono trasformate in una forma trasmissibile dal livello 1 che a sua volta le trasforma in segnali fisici.