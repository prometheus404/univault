# Controller Area Network Bus
![[CAN_dataframe1.png]]
e' uno standard di comunicazione seriale non particolarmente veloce e con raggio di azione limitato ma molto affidabile e semplice da implementare. Ottimo per ambienti critici o in cui la comunicazione puo essere compromessa da interferenze. E' uno standard master-slave con possibilita di multi-master e invia piccoli messaggi invece di grandi frame a tutti i nodi collegati al bus.
CAN e' un CSMA/CD+AMP (Carrier Senese Multiple Access, Collision Detection and Arbitration Message Priority) quindi ogni nodo deve aspettare un tempo prestabilito prima di inviare il proprio messaggio e nel bus vengono rilevate le potenziali collisioni tra i frame decidendo tramite un confronto bit a bit sui valori di priorita il messaggio da far arrivare.
