# I2C
il nome rappresenta $I^2 C$ e e' l'acronimo di Inter Integrated Circuit. e' un bus seriale ideato per la comunicazione a breve distanza e in generale all'interno dello stesso computer (nonostante in teoria possa attraversare qualche metro e' raro che venga usato).
Il voltaggio e la frequenza possono essere variate a seconda del caso ma quelle usuali sono descritte come:
- low-speed 10kb/s
- standard mode 100kbit/s
- fast mode 400kbit/s
- fast mode plus 1Mbit/s
- High speed mode 3.4Mbit/s

Utilizza due linee bidirezionali note come SDA(Serial Data Line) e SCL(Serial Clock Line) per connettere i nodi che possono essese
- Master: Genera il clock e fa partire la comunicazione con lo slave
- Slave: Riceve il clock e risponde quando indirizzato dal master

I2C e un bus multimaster e il ruolo puo essere cambiato tra un messaggio e l'altro

Il Segnale di clock viene abilitato ogni volta che si intenda mandare un singolo bit. Il clock e' attivo basso.
Dopo la condixione S di stard (SCL attivo, SDA disattivato) viene specificato un indirizzo di destinazione da 7 bit piu un bit atto ad indicare se si tratti di una read o di una write. A questo risponde lo slave con un ACK. A questo punto avvienen la trasmissione del dato vero e proprio impacchettato in una o piu frame da 8bit l'una seguite ogni volta da un altro segnale di ack o di nack. La fine della trasmissione e' marcata dalla stop condition che e' linverso di quella di start.