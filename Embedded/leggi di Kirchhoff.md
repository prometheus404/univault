# Leggi di Kirchhoff
Sono due leggi di *conservazione* 
- della corrente
- della tensione:
## Legge delle correnti
Formalizza il fatto che *in un nodo* (punto di incontro di piu percorsi all'interno di un circuito) le correnti entranti e le correnti uscenti si bilanciano:
$$\sum_{\text{segmenti k}}I_k=0$$
## Legge delle tensioni
*In un circuito* la somma algebrica delle tensioni e' 0 (non si creano ne spariscono differenze di potenziale)
$$\sum_{\text{segmenti k}} V_k = 0$$