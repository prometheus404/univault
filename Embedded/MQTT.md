# Message Queuing Telemetry Transport
E' un protocollo di comunicazione leggero basato su TCP/IP che si basa sul pattern publish-subscribe: i messaggi vengono inviati da una sorgente (un nodo client della rete) verso un dispatching broker che li ridistribuisce a tutti i nodi che si erano dichiarati interessati a quel tipo (topic) di messaggio.
Ogni messaggio è composto da:
- *topic* : che serve al broker per la distribuzione, è una stringa il cui contenuto è libero, ma viene specificata la forma sintattica da usare per costruirlo (parole separate dal carattere "/")
- *payload* : il corpo effettivo del messaggio. Una semplice stringa UTF-8 di semantica stabilita tra chi manda e chi riceve

Il broker gestisce una lista di nodi interessati a vari topic e ogni messaggio che riceve viene mandato solo ai nodi che si erano iscritti a quel topic. I nodi possono usare anche due wildcard per specificare un insieme di topic
- `+` indica una qualsiasi parola senza `/`
- `#` indica una qualsiasi parola che non inizia per `/` ma che puo contenerli

Le principali funzioni sono:
```c
/*
permette la dichiarazione di interesse verso un topic
e la funzione da chiamare quando arriva un messaggio
*/
Subscribe(String subTopic, Function callback);

//permette l'ivio di un messaggio associato a un topic
Publish(String topic, String message);
```
