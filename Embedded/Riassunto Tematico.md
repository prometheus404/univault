# Sistemi Embedded

## Intro: tutto, in dettaglio.
* [[Sistemi embedded#^2eaf7b|Definizione di sistema embedded]]
* [[Sistemi embedded#^05a4e9|panoramica generale sui sistemi embedded]], classi di device, caratteristiche, ordini di grandezza (dimensioni, potenze, consumi, ecc.), ambiti di utilizzo
* [[componenti interni di un MCU]]
* descrivere [[Sistemi embedded#^9d91d5|PLC]]
* definire [[Sistemi embedded#^2e860a|SoC]], cosa contiene, come si "combina" per la realizzazione dei vari prodotti reali

## Concetti: tutto, in dettaglio.
* [[sistemi monoprogrammati e multiprogrammati]], differenze, caratteristiche, cosa c'entra la Macchina di Turing?
* conversione [[AD e DA]], caratteristiche, pregi e difetti, considerazioni sul flusso di dati campionati
* spiegare [[tecniche di multiplexing ]](divisione tempo, frequenza), shift register
* differenza fra controllo a [[loop chiuso e aperto]], problemi del loop aperto
* spiegare tecnica [[PID (Proporzionale Integrativo Derivativo) |PID]] per controllo a loop chiuso
* considerazioni sull'uso della [[memoria nei sistemi embedded|memoria in un Arduino]], rispetto ai tipi di dati disponibili
* architettura [[Von Neumann vs. Harvard]]
* argomentare validità della [[memoria nei sistemi embedded|ricorsione in ambito embedded]], vantaggi e svantaggi
* descrivere la [[memoria nei sistemi embedded|gestione della memoria in un sistema embedded]]
* spiegare [[meccanismo dell'interrupt]]
* definire [[categorie "real time"]]
* spiegare [[licenza]] proprietaria vs. libera
* fare esempio di [[licenza]] software libera, descrivendone le caratteristiche salienti
* descrivere il concetto di [[watchdog]]

## Richiami: tutto, in dettaglio.
* descrivere a grandi linee il funzionamento di un [[componenti|componente]] resistore/condensatore/transistor/etc./... citare utilizzi
* panoramica delle [[unita di misura (elettricita elettronica)]] e loro significato
* spiegare i collegamenti [[serie e parallelo]] per resistori e condensatori
* descrivere un [[circuiti RC#^2e5c5f|filtro passa alto/basso]], elencare utilizzi
* spiegare $V_{eff}$ ([[tensione efficace]])
* descrivere [[leggi di Kirchhoff]]
* differenza fra [[ACDC|corrente alternata e continua]]
* cos'è la [[leggi di Ohm#^4b342b|potenza elettrica]]?
* cos'è un [[segnali|segnale]]?
* quale effetto ha un [[componenti#^eea1a3|condensatore su un segnale variabile]]?
* cosa si intende con "[[forma d'onda]]"?
* spiegare [[circuiti RC#^1e71b2|costante di tempo RC]]
* descrivere [[circuiti RC|integratore (passa basso) e differenziatore (passa alto)]] RC
* descrivere [[leggi di Ohm]] e loro significato
* panoramica generale su [[sensori|sensori "fisici"]], esempi
* panoramica generale su [[sensori|sensori "numerici"]], esempi
* panoramica generale su [[attuatori]], esempi
* panoramica generale su [[motori]], esempi
* panoramica [[strumenti di misura]], esempi
* spiegare [[PWM]] e utilizzi
* panoramica [[strumenti di misura]] e loro uso, problemi possibili
## Architetture: tutto, in dettaglio.
* cos'è un [[ISA]]?
* differenze fra [[ISA|CISC, RISC, VLIW, EPIC]]
* esempi [[ISA|CISC e RISC]]
* cosa si intende con "[[endianness]]"?
* cos'è un [[FPGA]]?
* descrivere architettura [[AVR]] con esempi
* descrivere architettura [[Xtensa]] con esempi
* cosa si intende con SoC/demo board/eval board/validation [[Mcu Soc e Board|board]]?
* storia e architettura [[Arduino]]
* descrivere piattaforma [[STM32]]
* descrivere piattaforma ESP8266/ESP32
* cos'è [[NodeMCU]]?
* cosa si intende con [[PCB(elettronica)]]?
## Mem e I/O: tutto, in dettaglio.
* [[differenze fra i vari tipi di memoria]] (RAM/ROM/flash/EPROM/sequenziale/casuale/SDRAM/DRAM/DDR/etc.)
* cos'è e cosa fa una [[MMU]]?
* panoramica supporti memoria di massa nell'embedded, pregi e difetti
* cosa si intende con [[XIP]]? pregi e difetti?
* quali tipi di [[filesystem embedded|filesystem]] vengono usati nell'embedded? pregi e difetti
* cos'è il "[[Flash-based SSD#^558d33|wear leveling]]"?
* cos'è una [[RS232]]? che caratteristiche ha?
* descrivere [[NMEA0183]]
* descrivere [[I2C]]
* descrivere [[I2S]]
* descrivere [[SPI]]
* descrivere [[CAN-BUS]]
* descrivere [[Ethernet]]
* a cosa serve una linea di CLOCK? se ne può fare a meno?
* vantaggi e svantaggi del "chip select"
* cosa si intende con [[GPIO]]? come viene usato?
* pullup e pulldown
* cos'è il "[[bit banging]]"?
* [[bit banging#^47d07d|vantaggi]] e [[bit banging#^8d2217|svantaggi]] del bit banging
* perchè negli alimentatori switching non riesco a notare le oscillazioni di corrente?
* cos'è il [[JTAG]]?
## S.O.: overview
* cos'è un S.O.?
* descrivere [[tipologie kernel|architettura]] monolitica/microkernel/etc.
* cosa si intende con "preemptive multitasking"?
* cos'è un filesystem?
* cos'è il "[[root filesystem]]"?
* cos'è un "[[init system]]"?
* cos'è una [[shell]]?
* cos'è e cosa serve un [[bootloader]]?
## Linux: overview
* cos'è una [[toolchain]]?
* cos'è un [[build system]]? citarne qualcuno e descrivere
* cosa sono le [[partizioni]]? che tipi puoi citare e descrivere?
## FreeRTOS: overview
* descrivere cos'è FreeRTOS e come viene utilizzato
* cos'è un task in FreeRTOS?
* cos'è e a cosa serve un semaforo?
## Arduino: tutto, in dettaglio
* origine di [[Arduino]]
* [[Arduino#^5ef89f|pro]] e [[Arduino#^96a476|contro]] della piattaforma
* spiegare meccanismo di boot di un Arduino (classico: UNO)
* spiegare il funzionamento di [[Arduino#^0655fa|setup()]] e [[Arduino#^72a638|loop()]]
* panoramica [[Arduino#^9eb6ed|tipi di dato]]
* panoramica [[Arduino#^494716|operatori]]
* cos'è un [[Arduino#^ef4e7e|array]]?
* spiegare [[Arduino#^5f9460|direttive compilatore]]
* panoramica costrutti di controllo di flusso
* cos'è una funzione?
* descrivere il meccanismo di gestione degli [[Arduino#^1e211b|interrupt]] facendo esempi di codice
* descrivere [funzionalità del piedino](https://www.circuito.io/blog/arduino-uno-pinout/) AREF/VIN/D0-1-2-...-N/GND/A0-1-2-...-N/RX/TX/SDA/SCL/... di Arduino/Wemos/...
* spiegare differenza fra GPIO digitale e analogico
## Rete: tutto, in dettaglio
* spiegare livelli [[ISO-OSI|ISO/OSI]] e [[TCP-IP|TCP/IP]]
* citare protocolli "normali" e "IoT", differenze, pro e contro
* panoramica [[topologie di rete]]
* Descrivere a grandi linee il protocollo OSC/[[MQTT]]...
* cos'è [[LoRa]]?