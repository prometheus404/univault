# Shell
Si possono distinguere due tipi di interfacce con l'utente:
- CLI
- GUI

Nella pratica comune con il termine shell si tende a indicare la prima tipologia. Da notare che la shell e' un software atto all'interpretazione dei comandi, mentre il terminale e' un hardware (vero o emulato) di comunicazione. Gli ambienti UNIX sono da sempre stati dotati di una o piu shell che permettono l'esecuzione interattiva (inserendo comandi) e non (usando un file di testo) addirittura definendo un vero e proprio linguaggio interpretato durante l'esecuzione.