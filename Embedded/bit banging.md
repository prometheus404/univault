# Bit banging
È una tecnica per implementare comunicazioni seriali generando treni di impulsi via software evitando hardware specifico. Si basa sul fatto che i processori odierni cono abbastanza veloci da poter emulare un circuito dedicato alla generazione di segnali non eccessivamente complessi.
Per inviare su una linea un treno di impulsi corrispondente a un byte è sufficiente ciclare su tutti i bit del byte alzando il piedino di output ogni volta che si incontra un uno. Il ciclo deve essere eseguito però con una velocità definita altrimenti serve anche una linea di clock.

Il vantaggio è nell'economia dell'implementazione e nella flessibilità sul protocollo di comunicazione (basta aggiornare il software del device per supportare nuovi protocolli) ^47d07d

Lo svantaggio risiede nella minore robustezza: l'implementazione software viene influenzata da quello che sta girando sul device (ad esempio la gestione di interrupt potrebbe distrarre il processore abbastanza a lungo da far perdere dati) ^8d2217