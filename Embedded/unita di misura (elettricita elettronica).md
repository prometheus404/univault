# panoramica delle unità di misura (elettricità/elettronica) e loro significato
## Volt
misurano il potenziale elettrico (o tensione) ovvero la forza di attrazione che cerca di spostare degli elettroni da un punto all'altro di un circuito
## Ampere
misura la quantità di corrente che scorre in un'unita di tempo (intensità).
Corrisponde alla quantità di carica (Coulomb) trasportata in un secondo

## Ohm
Unita di misura della resistenza, ovvero una proprietà tipica di un conduttore dovuta al tipo di materiale di cui é formata