# Collegamenti in serie e parallelo
Il collegamento in serie e' quello in cui i componenti sono accodati uno con l'altro, in questo caso il flusso della corrente passa prima in un componente e poi nell'altro.
Il collegamento in parallelo e' quello in cui ogni componente e' affiancato all'altro e in questo caso il flusso della corrente si partiziona nei due rami (non necessariamente 50% e 50%)

## Resistenze
### serie
Due o piu resistenze in serie sommano i loro effetti: la resistenza totale del componenti aggregato e' semplicemente la somma delle singole resistenze
$$R_{tot} = R_1+R_2+...+...R_n$$
per esempio collegando in serie le lampadine la resistenza di queste sara piu alta di quella della singola lampadina con il risultato che possiamo alimentarle con una tensione maggiore senza fare saltare il tutto

### parallelo
Nel caso del collegamento in parallelo di resistenze il flusso di corrente si distribuuisce proporzionalmente al valore delle resistenze stese secondo la formula
$$\frac{1}{R_{tot}}=\frac{1}{R_1}+\frac{1}{R_2}+...+\frac{1}{R_n}$$
## Condensatori
### serie
La capacita totale di due o piu condensatori in serie si calcola come il parallelo delle resistenze
$$\frac{1}{C_{tot}}=\frac{1}{C_1}+\frac{1}{C_2}+...+\frac{1}{C_n}$$
### parallelo
Due o piu condensatori in parallelo sommano i loro effetti, cioe la capacita totale del componente aggregato e la somma delle singole capacita
$$C_{tot}=C_1+C_2+...+C_n$$
