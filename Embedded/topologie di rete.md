# Topologie di rete
A seconda di come sono connessi i nodi possiamo avere reti:
- Lineari: anche dette daisy-chain ogni nodo è collegato a due nodi adiacenti tranne il primo e l'ultimo
- Anello: ogni nodo è collegato a due nodi adiacenti
- Albero: da ogni nodo possono partire catene lineari partendo dalla radice muovendosi sulle foglie
- Stella: tutti i nodi sono collegati a un unico nodo centrale
- Bus: tutti i nodi sono collegati a un unico canale condiviso di comunicazione
- Maglia: tutti i nodi trasmettono dati ad altri nodi (non per forza tutti) 