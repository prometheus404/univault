# Componenti interni di un MCU
* Unita di elaborazione
* Memoria dati (RAM o EPROM)
* Oscillatore (circuito per generare forme d'onda di frequenza usato per il clock)
* Memoria Programma (ROM, EPROM, FLASH)
* GPIO
* Porte di comunicazione base (USART, I2S, SPI, I2C, USB)
* Porte analogiche (DAC, ADC, PWM)

I modelli piu avanzati possono avere anche interfacce piu complesse come WiFi, Ethernet, ...
