# Ethernet
Il nome in realta non e' corretto e si riferisce alla prima implementazione dello standard, il nome corretto sarebbe IEEE802.3
Non e' considerabile un unico elemento hardware, e' comune che questo componente sia diviso in due parti separate e fornite da produttori differenti:
- MAC: (Media Access Control) rappresenta il livello 2 della pila ISO/OSI
- PHY: (Physical layer) rappresenta invece il livello 1

E' abbastanza comune che il MAC si trovi all'interno del SoC, mentre il PHY sia esterno, posizionato sulla board. Questo permette di testare un SoC dotato di connessione di rete nelle fasi di sviluppo per poi rimuovere il PHY in fase finale visto che e' un componente ingombrante.
Questi due componenti hanno varie modalita standard di collegamento che genericamente prendono il nome di Media-Indipendent Interface.
- MII: richiede 18 segnali e raggiunge i 100Mbit con un clock di 25 MHz
- RMII: R sta per Reduced, richiede solo 9 segnali ma per funzionare con la stessa frequenza di un MII richiede clock doppio
- GMII: G sta per Gigabit, e' retrocompatibile con MII e utilizza clock piu veloci per raggiundere velocita di 1Gbit
- RGMII: versione di GMII con un numero di segnali ridotto rispetto a GMII