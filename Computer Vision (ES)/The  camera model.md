# Introduction
If we want to take a picture we need to unterstand the **geometric** and **photometric** properties of the real world (3D) to make a projection in the picture (2D).

## Geometric properties
Understanding geometric properties is needed to link a 3D point with a 2D projection
#TODO  aggiungere immagine sulle slide e formule del modello base di camera

There are two types of reflection:
- the reflection is in every direction -> in this case the incident angle of light doesn't matter for determining the direction of the reflecting beam but only for the intensity. The reflected light will change its color
- In this case the reflection is dependent from the incident angle: in particular the reflected beam will form an angle $-\alpha$ with the normal of the reflecting surface, where $\alpha$ is the value of the incident angle with respect to the normal of the reflecting surface. #TODO aggiungere foto riflessione
## Photometric properties
These are needed to assign a color to a point given the properties of its material and light.

# Pinhole camera model
To take a picture we need some type of surface to project the 2D representation of the 3D world -> a film. The simplest possible type of film is a white straight surface like for example a wall (take as an example shadow play or ombre cinesi in italian).

But normally it's not possible to see any reflection of the world on a wall. That is because normally an object generate diffuse lights, so on every point we have a mix of all point in the room -> so all the light beam blend into a medium value of light and the wall remains of a consistent color. This is not useful because there's no univoque relation between a point on the wall and a point in the 3D world (so it's not a projection).

We can introduce a barrier between the scene and the film with a very small aperture (ideally a point) so that only one ray of light passes for every point (for two points passes only one straight line) 
#TODO aggiungere foto pinhole camera model

This way every point of the object has $(X,Y,Z)$ coordinates and is projected on $(x,y)$ coordinates. If we call $O$ the hole we can say that the two triangles #TODO  controllare questa parte sulle slide perche' non mi tornano i conti

since $z$ is fixed inside a camera and is the focal length f we have that:
$$x = f \frac{X}{Z}, y= f\frac{Y}{Z}$$
note: from the coordinate of 3D world we can unically define coordinates of corresponding pixel but the contrary is not true.

In the real world the aperture is not a perfect point so it has a dimension and this dimension has an impact on the resulting photo:
- **BIG APERTURE** -> more blurry image
- **SMALL APERTURE** -> the image become sharper but with less light pictures are darker
- **TOO SMALL APERTURE** -> if the aperture is too small light stop behaving like a particle and start behaving like a wave, so the aperture causes diffraction and becomes a source of diffracted light itself.

# Thin lens camera model

Lenses are made to use the principle of **refraction**, which is when the light travels through different medium and because of that its angle changes.

#TODO aggiungi immagine omino e lente

we want to design the camera to make it converge on the image field so lenses are disigned to focus a point at a fixed distance $u$ (depth of field)
