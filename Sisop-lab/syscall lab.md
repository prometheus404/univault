# Assembly
```nasm
%include 'file_included.asm'

SECTION .data 								; sezione dedicata ai dati statici
nome	db 		'stringa', 0Ah, 0h			; 0Ah a capo, 0h è \0
size_nome	equ	$-nome						; calcola la dimensione di nome

SECTION .bss 								; sezione dedicata ai dati dinamici
nomebuff: 	resb	255						; riserva un buffer di 255 byte

SECTION .text								; sezione dedicata al codice
global _start								; indica il punto di partenza nel codice

_start: 									; punto do partenza del codice
; chiamata di sistema per stampare:
; codice chiamata (4) -> eax
; file su cui scrivere (stdout = 1) -> ebx
; puntatore a messaggio in ecx
; numero di byte da scrivere in edx
mov eax, 4									; sposta 4 in eax
mov ebx, 1
mov ecx, nome
mov edx, size_nome
int 80h										; esegue l'interrupt

push eax 									; mette il contenuto di eax sullo stack
pop eax										; mette il primo elemento dello stack in eax
; chiamata per leggere
; eax 3
; ebx 0 (standard input)
; ecx nomebuff
; edx 255 (numero byte da leggere)
; int 80h

; chiamata per uscire
; eax 1
; ebx 0 (codice di ritorno)
; int 80h

inc eax 									; incrementa il valore di eax
cmp eax, ebx 								; compara i valori di eax e ebx
jz tag										; jump a 'tag' se il cmp di prima da 0 (se sono uguali)

; chiamata a procedura
; call nomeprocedura
; ritorno alla fine della procedura
; ret
```