---
aliases: [regular expression]
---

# Regexp
## Posix
+ ``^`` : matcha l'inizio di una stringa
+ ``$`` : matcha la fine di una stringa
+ ``.`` : matcha qualsiasi carattere una sola volta
+ ``[]``: matcha un carattere singolo tra quelli contenuti nelle parentesi ```[a-z]`` indica tutti i caratter tra a e z, mentre ``[-az]`` matcha con '-', 'a', 'z'
+ ``[^]``: matcha un carattere sigolo tra quelli non contenuti tra le parentesi
+ ``()``: definisce una sottoespressione
+ ``*``: indica che il carattere precedente può essere ripetuto da 0 a n volte ``ab*c`` matcha con ac, abc, abbc, ...
+ ``{m,n}``: indica che il carattere precedente deve essere ripetuto almeno m volte e al massimo n
## Extended Posix
+ ``?`` indica che il carattere precedente può essere ripetuto 0 o 1 volta
+ ``+`` indica che il carattere precedente può essere ripetuto da 1 a n volte
+ ``|`` indica una scelta ``(c|h)at`` matcha con cat e con hat (in questo caso uguale a [hc]at)

