---
aliases: [bash, sh, shell, riga di comando]
---
# Comandi Shell
## Sintassi base
- ``x=...`` assegna un valore alla variabile `x`
- ``$x`` 	riferimento alla variabile `x`
- ``>`` 	semplice output redirection
- ``>>`` 	aggiunge l'output in coda al file
- ``2>`` 	redirige l'output su standard error
- ``<`` 	semplice input redirection
- ``()`` 	raggruppa un insieme di comandi (eseguiti da una sottoshell)
- ``$()`` 	viene sosstituito con lo standard output del comando tra parentesi
- ``;`` 	esegue comandi in sequenza
- ``&`` 	esegue i due programmi separati da essa in parallelo
- ``|`` 	crea una pipe che esegue i due programmi in parallelo e indirizzando l'output di uno nell'altro
- ``'`` 	delimitatore di stringhe semplice (`` echo '$VARIABILE'`` ~> ``$VARIABILE``)
- ``"`` 	delimitatore di stringhe che permette la sostituzione delle variabili (``echo "$VARIBILE" ``~> ``CONTENUTOVARIABILE``)
- ``c*`` 	matcha tutte le sequenze di caratteri che iniziano con c
## Controlli di flusso
### If
```bash
# nota che puoi scriverlo tutto sulla stessa riga semplicemente mettendo ';' invece che andare a capo
if [ x -eq 0 ]
	then echo ok
	else echo no
fi
# possibili confronti sono:
# -eq  =
# -ne  !=
# -ge  >=
# -gt  >
# -le  <=
# -lt  <

```
### For
```bash
for i in a b c d e; do echo $i; done
# oppure
for i in a b c d e
	do echo $i 
done
```
### While
```bash
while true; do echo ciao; sleep 1; done
# oppure
while true
do
 	echo ciao
	sleep 1
done
```
## Programmi base
### Rm
- ``-r`` ~> rimuove ricorsivamente all'interno della directory
- ``-f`` ~> forza la rimozione
```bash
# rimuove tutti i file che iniziano con tmp
rm tmp* 	# solo nella diirectory corrente
rm -r tmp*  # anche nelle sottodirectory
```
### Touch
```bash
# crea il fle f
touch f
```
### Mkdir
```bash
# crea la directory d
mkdir d
```
### Rmdir
```bash
#rimuove la directory vuota emptydir
rmdir emptydir
```
### Cat
```bash
# stampa il contenuto di un file (non usare sui file binari)
cat file.txt
# stampa il contenuto del file dall'ultima alla prima riga
tac file.txt
```
### Echo
```bash
# stampa "testo da stampare"
echo "testo da stampare"
```
### ps 
elenca i processi in esecuzione
`-e` -> elenca **tutti** i processi in esecuzione
`-o` -> per scegliere che colonne motrare. Le possibili colonne sono
- pid
- ppid
- uname
- args
- cmd (nome del processo)
### which
indica il percorso di un comando
### test
per espressioni booleane
### expr
valuta espressioni aritmetiche
### sort
ordina le righe di un file
### sleep
dorme per i secondi indicati
```bash
# dorme per un secondo
sleep 1
```
### kill
manda un segnale di terminare a un processo
### tail
stampa le ultime righe di un file 
### head
stampa le prime righe di un file
### dc
calcoli complessi
### wc 
word count, conta parole righe e caratteri
### cut 
divide il testo in colonne
### paste
unisce colonne di testo
### cp 
copia file
### mv
muove o rinomina file 
### mkdir
crea una directory
### rmdir
rimuove una directory vuota
### chmod
cambia i permessi
### chown
cambia il proprietario
### chgrp
cambia il gruppo
### tr
traspone i caratteri
### less
mostra un insieme di dati su una schermata alla volta
### ln 
crea un link a un file 
### stat
informazioni su un file
### uniq
elimina i duplicati da una serie di linee ordinate
`-c` -> restituisce n parola dove n è il numero di occorrenze di 'parola'
### tee 
riversa lo stdin sullo stdout e su un file 
### basename
elimina un suffisso da una stringa (ed eventuali prefissi simili a un path `/.../../`
### dirname
stampa i prefissi di una stringa simili a un path `/.../..`
### seq
stampa una sequenza di numeri (utile per i for)
### tar 
archivia un elenco di file
`-c` -> crea un archivo (deve esserci anche `-f`)
`-f` -> nome archivio
`-z` -> comprime l'archivio
```bash
tar -zcf archivio.tar
```
### gzip
comprime un file (spesso un archivio tar)
### dd 
crea uno stream di dati, producendone uno nuovo (eventualmente con conversioni)
`if=INPUTFILE`
`ibs=BYTE` -> i blocchi valgono `BYTE` byte
`count=N` -> legge N blocchi
### xargs
usa le lineee provenienti da stdin come argomenti di un comando (spesso usato in combinazione con find 
- `-0`-> conta 0 come separatore invece degli spazi 
- `-n`-> indica il numero massimo di argomenti. Se = 1 ripete il comando con argomento diverso ogni volta
```bash 
cat elenco_file_da_rimuovere.txt | xargs rm 
```
da notare come `comando1 |xargs comando2` è uguale a `comando2 $(comando1)` come effetto ma xargs spezza in modo corretto nel caso l'output del primo comando sia troppo lungo
### du
calcola l'occupazione dei file
`-t N`-> ignora i file con dimensione più piccole di N se N>0 altrimenti i fle più grossi di N
`-b` -> dimensione in byte

## sed
`-i` -> esegue le operazioni in-place sul file
`-n` -> annulla il normale output
```bash
#cambia tutte le occorrenze di find con replace nel file file.txt
#se voglio sostituire solo la prima non metto g
sed -i "s/find/replace/g"
# cambia la prima occorrenza di find con replace solo sulle righe che contengono pattern
sed -i '/pattern/s/find/replace/' file.txt
# cancella le righe contenenti pattern del file.txt
sed -i '/pattern/d' file.txt
# sostituisce find con replace e find2 con replace2
sed -e 's/find/replace/' -e 's/find2/replace2/'
# stampa le righe che matchano il pattern
sed -n '/pattern/p'
# cancella gli spazi alla fine delle righe
sed 's/ *$//'
# cancella linee vuote
sed '/^$/d'
```
## find
Visita il sottoalbero a partire da una direcory e seleziona i file che rispettano i predicati indicati.
- `print0`-> in combinazione con xargs -0 funziona anche se ci sono spazi
```bash
#trova tutti i file nel percorso che rendono vero il predicato
find percorso predicato 
```
## grep
`-r`-> controlla ricorsivamente nelle sottodirectory
`-l`-> invece del normale output stampa il file il cui contenuto sarebbe stato stampato
`-i`-> case insensitive match
`-h`-> toglie dall'output il nome del file
`-v`-> inverte il match
`-o`-> only matching 
```bash
estrae tutti gli indirizzi ip dai file
grep -roh -e "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"
```
## Awk
+ ``-F`` -> permette di scegliere un altro separatore
+ ``'regexp'`` -> seleziona le stringhe che matchano [[regexp|regexp]]
+ ``'{print $x}'`` -> stampa la colonna x

### Variabili
- `ARGC` numero degli argomenti passati da terminale
- `ARGV` array dgli argomenti passati da terminale
- `FILENAME` non definito nel blocco BEGIN
```bash
# conta le occorrenze delle parole
awk '{for(i=1; i<=NF; i++) words[tolower($i)]++}; END{for(i in words) print i, words[i]}' testo.txt | sort -nrk2
# stampa le righe dell'output di df in cui la prima colonna è più lunga di 10
df | awk 'length($1) > 10'
# stampa le prime tre colonne separate da tab dlle righe che iniziano (^) con /dev loop
# nota che \ è usato per fare escape di /
df | awk '/\/dev\/loop/ {print $1"\t"$2"\t"$3"}'
# usa come separatore delle colonne ':' invece che ' ' e stampa le prime due colonne di /etc/passwd separate da spazio
awk -F ":" '{print $1 ,$2}' /etc/passwd
# somma i numeri dispari che vengono dati da standard input
awk '($1 % 2)!=0  {sum += $1}; END {print sum}
```
