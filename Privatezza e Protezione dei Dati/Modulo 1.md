# INFO GENERALI
ti conviene vedere le lezioni 2021/22 che sono quelle fatte meglio
non ci sono temi d'esame online. Le domande sono domande aperte con piccoli esercizi semplicemente per capire la teoria
le lezioni dovrebbero finire a novembre e l'esame dovrebbe essere per prima di natale

----


# privacy and data protection nella pubblicazione dei dati
In questo periodo storico c'e' una grande crescita di dati che vengono generati.
La condivisione di questi dati non e' di per se una cosa negativa ma anzi puo' essere positiva.
esistono due tipi di pubblicazione dei dati:
- Pubblicazione dati
	 - per fare inferenza statistica
	 - studi
	-  permettere accesso a servizi online
- Condivisione dati con terze parti
	- permette di risparmiare sullo storage
	- permette di avere una migliore protezione
	- non devo preoccuparmi di gestire la parte dei dati se non e' lo scopo della mia impresa

devo trovare un modo per rendere la condivisione di dati sicuri e per garantire che i dati condivisi con terze parti siano effettivamente quelli che avevamo condiviso quando qualcuno va a leggerli da loro. 

## privacy nella pubblicazione dei dati
Se devo rilasciare i dati pubblicamente posso farlo in due modi diversi: rilasciando dati statistici (tabelle di conteggio, di frequenza o di grandezza) in cui ogni cella della tabella contiene un dato aggregato da un numero di rispondenti > 1 (macrodati) o rilasciando tabelle di grandezza in cui ogni cella contiene i dati di un singolo rispondente.
- [[Macrodati]]: possono essere di tipo [[*statistical DBMS*]] o di tipo [[*statistical data*]] a seconda di come vengono condivisi. Nel primo caso metto a disposizione un'interfaccia che permette a un utente di fare delle query statistiche controllate direttamente sul dbms. Nel secondo condivido solamente una serie di statistiche precalcolate. Nel primo caso ho il problema di fare controlli dinamici sulle query controllando anche la storia delle query. Inoltre bisogna renderlo resistente alla collusione, quindi fare in modo che un piccolo gruppo di persone messe insieme possano fare un set di query tale che sommando le informazioni restituite possano ottenere informazioni riservate.
- [[Microdati]]: Condivido direttamete i dati anonimizzati. Sono soggetti ad attacchi di linking a altri microdati dati da altri gestori che condividono gli stessi dati o dati che si sovrappongono con i dati condivisi.

Cosa vuol dire proteggere la privacy dei dati? Vol dire evitre la *disclosure* di dati personali delle persone rappresentate nella tabella. In particolare si vuole evitare:
- identity disclosure -> proteggere l'Identità' delle persone rappresentate
- attribute disclosure -> proteggere le informazioni sensibili (per esempio che malattia hanno)
- inferential disclosure -> proteggere l'analisi dei dati da correlazioni sensibili. 

### Identity disclosure
Quando qualcuno che guarda i dati puo capire chi e' un rispondente dei dati. Questo e' un problema solo se l'appartenenza al database sia un dato sensibile. Per esempio non ci sarebbe nessun problema se qualcuno scoprisse che mario rossi appartiene alla lista di studenti di una universita', ma le cose cambiano se si parla della lista di persone curate per una determinata malattia. In generale 
- nei macrodati: rivelare l'identita non e' generalmente un problema a meno che non permetta di fare attribute disclosure
- nei microdati: e' generalmente considerata un problema perche' siccome i record sono molto dettagliati la conoscenza della presenza di una persona porta spesso all'attribute disclosure.

### Attribute disclosure
Vengono rivelati in modo diretto o indiretto i dettagli di un attributo sensibile. Questo e' un problema sia che il dato sia preciso sia che possa essere inferito con una precisione tale da essere un problema (per esempio riuscire a sapere lo stipendio di una persona con precisione di 5euro puo essere abbastanza problematico)

### Inferential disclosure
A volte le correlazioni stesse sono sensibili. Generalmente non e' considerato un rischio, anzi i dati vengono condivisi per scoprire corrispondenze. Ovviamente questo  non vale per tutte le corrispondenze e alcune vanno protette

## restricted data and restricted access
rilascio solo un sottoinsieme delle informazioni contenute dei dati e le rilascio solamente a un sottoinsieme delle persone che potrebbero essere interessate (per esempio solo a istituti di ricerca) e soprattutto solo per uno scopo ben preciso. Vanno fatte entrambe le cose e non sono esclusive.

## problema dell'anonimita'
questa e' una cosa ben diversa dal deidentificare (ovvero togliere nome e altre cose identificative) ma proteggere proprio dalla possibilita di risalire a una persona dai dati rilasciati insieme a altri dati pubblicamente disponibili.
Non bisogna prendere in considerazione solo gli identificatori e i dati sensibili ma anche i quasi-identificatori che linkati con dati esterni possono permettere di identificare una persona sia precisamente sia con un certo livello di incertezza.
Per erempio con la data di nascita completa e il codice di avviamento postale si possono identificare univocamente il 63% della popolazione americana. solo togliendo il giorno di nascita la percentuale scende al 4.2%.  Piu attributi hai in comune, più hai dati precisi e più hai dati all'esterno più c'e' possibilita di linking.
Non tutti sono esposti allo stesso modo. Quelli che hanno caratteristiche particolari per esempio sono piu esposte. 
a contribuire all'aumento di rischio:
- dati ad alta visibilita (basti pensare che sapere che uno fa l'insegnante non ci dice molto sulla persona ma sapere che e' un papa si)
- il numero di attributi che possono facilitare linking
- la precisione dei dati (se ho la data completa di nascita piu facilmente posso risalire alla persona)
- la quantita di dati presenti all'esterno
a contribuire alla protezione dei dati invece:
- usare un sottoinsieme dei dati da pubblicare
- errori nei dati
- se i dati sono espressi in formato diverso e non direttamente linkabili

Vanno tenute in considerazione anche la possibilita che la persona che non e protetta sia quella che vorrebbe vedere l'attaccante,la probabilita che le variabili siano linkabili, la probabilita che esista qualcosa per stabilire la correlazione con la persona che si vuole attaccare.

Se una combinazione e' unica nell'universo mondo e' unica sicuramente anche in tutti i dataset che la contengono. Il contrario ovviamente non e' vero ma per essere sicuri trattiamo tutte le tuple uniche nel nostro dataset come uniche globalmente perche' se questo fosse vero sono univocamente riconducibili a un rispondente
### k-anonymity
Cerca di impedire la capacita di fare correlazioni. Non possiamo togliere tutto ma ci piacerebbe che guardando i dati non si possa re-identificare le tuple che sono state anonimizzate senza un'incertezza. In particolare non voglio che una tupla sia collegabile a un singolo o pochi individui ma vogliamo che ciascuna tupla non possa essere attribuita a meno di k rispondenti.
Guardando i quasi identifier per ogni combinazione di valori devono esserci almeno k tuple con quella combinazione di valori.
le due famiglie di tecniche per evitare di sporcare i dati in questo caso sono:
- generalizzazione -> non sporco i dati ma tolgo un po' di dettaglio dalle informazioni
- soppressione  -> tolgo i record sensibili (bisogna fare attenzione perché togliendo record si rischia di rendere inutili le analisi fatte su quei dati)

#### Generalizzazione
Una generalizzazione e' una relazione $\leq_D$ che definisce un mapping  tra un dominio $D$ e le sue generalizzazioni.
la generalizzazione e' a livello gerarchico una catena di generalizzazione:
$$D_i \leq_D D_j, D_i \leq_D D_z \Rightarrow D_j \leq_D D_z \vee D_z \leq_D D_j$$
Quindi 
per ciascun dominio devo avere la definizione dei domini generalizzati e la mappatura del valore specifico alla sua generalizazione

La gerarchia dei valori e' un albero quindi
- i valori specifici sono foglie
- ciascun nodo ha un solo padre quindi una sola generalizzazione (questo in realta potrebbe non essere cosi)

se $T_i$ e $T_j$, $T_j$ e' una generalizzazione di $T_i$ se 
1.  $|T_j| \leq |T_i|$
2. per ogni attributo che consideri nella tabella genenralizzata hai o un elemento allo stesso dominio o una generalizzazione di questo
3. e' possibile definire una funzione iniettiva che associa ogni elemento di $T_j$ a un elemento di $T_i$ ma non biiniettiva perche alcune tuple potrebbero essere soppresse