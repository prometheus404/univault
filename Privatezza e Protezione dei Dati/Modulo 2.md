.... continuazione degli appunti della volta scorsa sui microdati ...

# tecniche di  masking
si dividono in tecniche perturbative e non perturbative. Le prime sporcano i dati in modo che i dati risultanti non siano veri. Le seconde mantengono dati veri ma nascondono alcune parti o aggiungono un po di rumore come per esempio aumentando di qualche euro la busta paga ecc... possono essere tecniche di protezione di microdati sia per la pubblicazione diretta dei microdati sia per l'utilizzo di questi ultimi per creare dati aggregati

## Tecniche non perturbative
### Sampling
prendo solamente un campione della popolazione 

### Soppressione locale
se un dato e' sensibile lo tolgo. Quando faccio soppressione devo stare attento: posso usarla solo se il fatto che la cella sia sensibile non dipende dal suo valore. Se per esempio gli stipendi sopra a 10k sono sensibili facendo soppressione si dice che lo stipendio e' sopra 10k. Inoltre devo sempre avere una soppressione secondaria per coprire la primaria.
### Global recoding
raggruppo per fasce e invece di pubblicare i valori specifici pubblico i valori nella fascia
### Top coding  e Bottom codiing
per proteggere gli outlier sopra o sotto certi livelli raggruppo tutti in un unico calderone

## Tecniche perturbative
## Random noise
introduco rumore nei miei dati. Ma quanto rumore metto e cosa puo causare questo rumore?
Innanzitutto il rumore deve essere controllato per preservare proprieta che mi interessano (tipo la media)
### Swapping
Se ho diverse porzioni dei dati e scambio una parte di dati appartenenti a una porzione e li scambio con una parte di dati appartenente a un altra. Devono essere anche questi simili e devono cercare di mantenere le proprieta importanti a seconda di come lavorero' successivamente per calcolare i dati aggregati. come per esempio la media di un valore oppure il valore stesso di un attributo (in modo da preservare il numero di elementi nel gruppo con un determinato attributo)

### Micro aggregazione
selezioni gruppi di poche tuple che condividono una serie di valori e rilascio un'aggregazione di queste tuple in modo che il dato sensibile sia frutto dell'aggregazione. Per esempio il reddito diventa la media del reddito delle persone con lo stesso sesso e che lavorano nello stesso posto
|nome| cognome| sesso| luogo| stipendio|
|---|---|---|----|---|
|giovanni | rossi | m | laveno | 1000
| marco     | verdi | m | laveno | 0

diventa

|nome| cognome| sesso| luogo| stipendio|
|---|---|---|----|---|
|giovanni | rossi | m | laveno | 500
| marco     | verdi | m | laveno | 500

e cosi per tutti i sottogruppi della tabella

# Synthetic thechniques
usare dati sintetici vuol dire usare dati non reali, estratti casualmente ma che rispettano le proprieta dei dati reali. Le proprieta che non vengono catturate neel modello pero come al solito potrebbero differire da quelle dei dati reali. Se per esempio non ho calcolato la correlazione tra malattia e luogo di nascita nei dati sintetici questa non sara rispettata. 