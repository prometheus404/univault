# General concepts
- show the pipeline of the project and our goals
	- want to see how good selected features are to classify stock based on
	the expected threshold dividend rate given an appropriate contextual embedding
	- we want to clasify using graph analysis to predict expected dividend rate
- what yahoo finance is and what kind of information does provide
- how we will scrape
	- scrape the symbols
	- scrape the corresponding features

https://es.finance.yahoo.com/quote/AAPL/key-statistics?p=AAPL
2min each

----
# Slide 1: our goals
>[!discorso]
> with this project we have two main objectives: 
> The first one is to predict the expected dividend rate of stocks
> The second one is to see how well the 
# Slide 2: how we will procede
*map of the project pipeline*
> [!Discorso]
>  to accomplish those goals we will first start by scraping yahoo finance to get stocks' features.
>  Then for the first goal we will attempt stock classification using assisted knn
>  while for the second one well use dimensionality reduction to provide a 2d 
# Slide 3: yahoo finance and scraping
> [!Discorso]
> for the data we are going to scrape yahoo finance. We chose it mainly because its one of the most famous and reliable platform for this kind of data.
>- first we start by scraping the symbols of the trending stocks 
>- then for each symbol we scrape the corresponding features that will be explained in more detail by filippo
