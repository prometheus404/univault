```
Le SOM derivano dalle RBFN, ovvero delle reti con le seguenti caratteristiche:
- rete a tre layer
- la $f_{net}$ dei neuroni hidden e' una funzione di distanza
- la $f_{act}$ dei neuroni hidden e' una funzione radiale: ovvero una funzione monotona non-crescente
- la funzione di attivazione di ogni neurone di output e' una funzione lineare

Sono chiamate funzioni radiali perche ogni neurone nascosto descrive con la sua funzione di attivazione e i pesi delle connessioni con il layer di input una regione di attivazione:
- il vettore dei pesi indica le coordinate della regione
- la funzione di input e' una funzione di distanza che ne definisce la forma
- la funzione di attivazione definisce il raggio della regione

```
Le **self-organizing maps** sono delle feed-forward network a due layer interpretabili come RBFN dove l'hidden layer svolge anche la funzione di output layer. Come nelle RBFN la $f_{net}$ dei neuroni di output e' una funzione di distanza tra il vettore di input e quello dei pesi, la $f_{act}$ e' una funzione radiale e i pesi delle connessioni dei neuroni di input ai neuroni di output definiscono le coordinate di un centro (**reference vector**) da cui viene misurata la distanza di un pattern di input. Tanto sara maggiore questa distanza tanto sara minore il valore di attivazione del neurone corrispondente.
Tipicamente tutti i neuroni avranno la stessa $f_{act}$, lo stesso raggio di riferimento e la stessa $f_{net}$.
La $f_{out}$ dei neuroni di output e' la funzione di identità anche se l'output può essere discretizzato facendo in modo che il neurone con la massima attivazione restituisca come output il valore 1 mentre tutti gli altri neuroni restituiscano il valore 0.

Una importante aggiunta rispetto alle RBFN e'  la **relazione di vicinanza** tra i neuroni dell'output layer descritta da una funzione di distanza.
Questa funzione assegna un numero reale non negativo a ogni coppia di neuroni di output e potrebbe anche essere assente ponendo a 0 la distanza da se stesso e distanza infinita da tutti gli altri neuroni. Può essere rappresentata da una griglia bidimensionale nella quale ogni punto identifica un neurone di output.
Se una relazione di vicinanza e' assente e l'output e' discretizzato(non basta che l'output sia discretizzato?) una SOM descrive una quantizzazione vettoriale dello spazio di input e questa tassellazione può essere rappresentata da un diagramma di Voronoi.
La relazione di vicinanza dei neuroni di output vincola la quantizzazione vettoriale e l'obiettivo viene raggiunto quando i vettori di riferimento vicini tra di loro nello spazio di input appartengono ai neuroni di output relativamente vicini l'un l'altro. In questo modo la SOM descrive una **topology preserving map** ovvero una mappa che preserva la posizione relativa tra i punti del dominio anche su spazi a dimensioni diversi. Questo rende le SOM particolarmente utili per mappare strutture multidimensionali in spazi con dimensioni minori.



## Training
Il training delle SOM si discosta da quello delle RBFN ed e' derivato da quello delle **learning vector quantization network**, un tipo di rete che permette di trovare in modo automatico una tassellazione dello spazio di input.  Queste sono formate allo stesso modo delle SOM con la differenza che l'output e' sempre discretizzato secondo il principio del **winner takes all**. I vettori di riferimento vengono aggionati tramite competitive learning: per ogni training pattern presente nel training set viene aggiornata la posizione del vettore di riferimento collegato al neurone vincitore tramite la regola: $$r_{new} = r_{old} + \eta(x-r_{old}) $$
In questo modo il vettore di riferimento si avvicinerà al training pattern proporzionalmente alla distanza da quest'ultimo e al learning rate $\eta$.
Le SOM, a differenza dei LVQN per mantenere l'informazione della topologia invece di aggiornare il singolo neurone vincitore aggiornano tutti i neuroni vicini ad esso in modo proporzionale alla loro distanza relativa:
$$r_{new} = r_{old} + \eta(t)f_{nb}(d_{neuroni}(u,u_*), \rho(t))(x-r_{old})$$
E' possibile notare che nella formula il learning rate e' dipendente dal tempo. Questo e' per evitare situazioni in cui il learning rate costante porterebbe a oscillare ciclicamente verso uno dei punti possibili. Riducendo il learning rate nel tempo l'oscillazione collassa mano a mano facendo convergere l'algoritmo. Bisogna pero' fare attenzione a non far diminuire il learning rate troppo rapidamente altrimenti avviene la cosiddetta **starvation** ovvero il caso in cui i passi di adattamento diventano piccoli cosi rapidamente che il vettore di riferimento non riesce a raggiungere la sua destinazione naturale.

Un altro problema che può sorgere con la versione classica di questo algoritmo e' che il processo di adattamento potrebbe portare i vettori di riferimento ad allontanarsi sempre di più tra loro. Per evitare questo comportamento e' possibile introdurre una **window rule** tale per cui un vettore di riferimento non viene aggiornato a meno che il punto del training set non sia posizionato vicino al bordo della classificazione.

E' inoltre possibile adattare questo algoritmo per poter assegnare in modo soft i punti ai neuroni rinunciando alla strategia del winner takes all e descrivendo i dati attraverso insiemi di gaussiane. In questo modo vengono attratti tutti i vettori di riferimento che appartengono alla stessa classe del punto (e di conseguenza i loro vicini) in modo proporzionale alla loro appartenenza mentre vengono respinti gli altri.