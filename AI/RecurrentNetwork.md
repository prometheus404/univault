sono network il cui grafo contiene al suo interno dei cicli tra uno o piu neuroni. Per via di questi cicli la computazione continua finche non viene raggiunto uno stato stabile e a quel punto viene generato l'output.
Alcuni esempi di Recurrent Network sono le boltzman machine e gli hopefield network

## training
I recurrent network vengono allenati allo stesso modo dei MLP, ovvero tramite backpropagation. Questo metodo non puo pero essere applicato direttamente a causa dei cicli della rete. Un modo per risolvere questo problema consiste nel dispiegare nel tempo la rete tra due pattern di allenamento e applicare la backpropagation come se fosse un feed forward network combinando gli aggiustamenti calcolati rispetto ai neuroni duplicati nel procedimento. Questo procedimento prende il nome di **error backpropagation through time**.

## scopo
I recurrent network sono comodi per rappresentare e risolvere (approssimativamente) arbitrarie equazioni differenziali: date delle equazioni differenziali in forma ricorsiva e' possibile sfruttare la derivata della funzione nell'istante di tempo precedente per calcolarne il valore successivo. Verranno poi creati tanti neuroni nella rete quante sono le variabili.