Sono una classe di tecniche di ottimizzazione che imitano i principi dell'evoluzione biologica: ovvero che i tratti che sono considerati vantaggiosi tendono a essere favoriti dalla selezione naturale aumentando le probabilita di chi li possiede di procreare e moltiplicarsi.
Gli elementi di un algoritmo evolutivo sono:
1. una **codifica** per i candidati in cromosomi (intesi come termini di oggetti computazionali chiamati geni). La rappresentazione data da questi ultimi e' detto **genotipo** ($\Gamma$) dell'individuo e puo essere una sequenza binaria (si parla in questo caso di algoritmi genetici) o un insieme di strutture dipendenti dal problema trattato (algoritmi evolutivi propriamente detti)
2. Un metodo per creare una popolazione iniziale tramite un algoritmo costruttivo
3. Una funzione di fitness per valutare i candidati. Il valore risultante rappresenta la qualita dell'individuo. Questa viene valutata sul **fenotipo**($\Omega$) dell'individuo e percio sara necessario avere una funzione di decodifica ($dec: \Gamma \to \Omega$) che permetta di passare dalla rappresentazione del genotipo a quella del fenotipo.
4. Dei metodi di selezione degli individui che dovranno procreare nella successiva generazione in relazione ai valori di fitness 
5. Un insieme di operatori genetici che modifichino i cromosomi in modo da introdurre mutazioni che permettono di esplorare lo spazio di ricerca. 
6. Parametri addizionali come la dimensione della popolazione, la probabilita di mutazione, ecc
7. Una condizione di terminazione (per esempio il numero massimo di generazioni, approssimazione all'ottimo, tempo massimo di computazione, ecc)

## Codifica
Non esiste un unico metodo, la scelta della codifica e' specifica per ogni problema. Esistono pero alcuni principi generali da seguire:
1. Fenotipi simili devono avere genotipi simili. In questo modo viene assicurato il fatto che un individuo generato con una piccola mutazione da un altro mantenga un fenotipo simile e il fatto che cambiamenti drastici nel genotipo permettano di evadere da minimi locali.
2. La funzione di fitness deve restituire valori simili per candidati simili. In questo modo si evita che la fitness sia troppo dipendente da un singolo gene. L'eccesso in un senso puo causare cambiamenti casuali di fitness, nell'altro puo ridurre l'efficienza dell'algoritmo
3. Lo spazio $\Omega$ deve essere intuitivamente chiuso rispetto agli operatori genetici. Infatti che non lo fosse un cromosoma modificato potrebbe non essere piu decodificato.
## Fitness
Per permettere che gli individui migliori abbiano maggiori possibilita di riprodursi bisogna esercitare la cosiddetta **selective pressure**. Se quest'ultima e' bassa molti individui possono riprodursi e questo permette di esplorare maggiormente lo spazio delle soluzioni. Se la selective pressure e' alta si parla invece di sfruttamento degli individui migliori e l'algoritmo convergera piu velocemente anche se con maggiore rischio di finire in minimi locali. Le principali metriche per valutare la selective pressure sono:
- **selection intensity** : la differenza tra qualita media tra prima e dopo la selezione
- **time to takeover**: il numero di generazioni perche la popolazione converga
I metodi di selezione possono essere diversi e possono variare a seconda della pressione evolutiva.

#TODO  aggiungere la parte dopo una volta finito l'esame
## Selezione
- **roulette-wheel selection** si calcola per ogni individuo il suo valore di fitness relativo.  La probabilita per un individuo di essere selezionato e proporzionale al suo fitness relativo #TODO aggiungere dominanza
- **rank-based-selection**: gli individui vengono ordinati in ordine decrescente e ad ognuno di essi viene assegnata una probabilita di essere selezionato che dipende dalla sua posizione nella classifica. Dopodiche vengono selezionati gli individui come nella roulette-wheel selection. In questo modo viene superato il problema della scomparsa della diversita che aveva il metodo precedente ma lo svantaggio e' che essendo necessario ordinare tutti gli individui e' computazionalmente pesante
- **tournament selection**: vengono estratti k individui casualmente e tramite vari scontri si decide il migliore che ricevera un discendente nella prima generazione. Tutti i partecipanti tornano nella popolazione e il processo viene ripetuto finche non viene creata la generazione successiva. Questo metodo permette di selezionare anche individui buoni ma non tra i migliori e si riesce a regolare la pressione di selezione grazie alla grandezza del torneo.
- **elitismo**: vengono scelti gli individui migliori per costituire la generazione successiva. Questi vengono comunque mutati tramite gli operatori genetici. Questo metodo permette una maggiore rapidita di convergenza ma rischia di essere incastrato in ottimi locali.
E' importante prevenire il **crowding** ovvero l'affollamento di singole aree dello spazio di ricerca che impediscono un'esplorazione efficace. Per evitare questo fenomeno e' necessario che individui della nuova generazione sostituiscano individui simili della generazione precedente.

-----------
/// questa parte non serve ///
I metodi di selezione possono avere le seguenti proprieta:
- **Static**: la probabilita di selezione rimane costante
- **Dynamic**: la probabilita di selezione varia
- **Extinguishing**: la probabilita di selezione puo essere 0
- **Preservative**: la probabilita di selezione e' sempre maggiore di 0
- **Pure-bred**: gli individui possono avere discenedendi in piu di una generazione
- **Under-bred**: gli individui possono avere discendenti solo in piu di una generazione
- **Right**: tutti gli individui possono riprodursi
- **Left**: i migliori individui possono non riprodursi
- **Generational**: i genitori possono mutare fino a quando i loro discendenti non vengono generati
- **On the fly**: i discendenti sostituiscono i genitori
//////////////////////
-----------

## Mutazione
Durante questa fase gli operatori genetici vengono applicati a una frazione della popolazione. A seconda dell'arieta degli operatori si dividono in:
- One-parent operator
- Two-parent operator
- multiple-parent operator
L'peratore di mutazione fa parte della prima classe e funziona introducendo piccoli cambiamenti randomici nel genoma. Risulta utile per introdurre biodiversita nel pool delle soluzioni. Esistono vari modi per applicare una mutazione:
- Standard mutation: il valore di un gene o piu viene mutato
- **Pair swap**: viene scambiata la posizione di coppie di geni
- **Shift**: si shifta un gruppo di k geni di n posizioni
- **Arbitrary permutation**: si permuta randomicamente un gruppo di geni
- **Inversion**: si inverte l'ordine di apparizione di un gruppo di geni
- **Binary mutation**: si cambia il valore di alcuni bit da 0 a 1
- **Gaussian mutation**: si sceglie per un gene un valore randomico a partire da una distribuzione gaussiana centrata nel valore originale del gene
Per quanto riguarda gli operatori a due genitori invece il piu usato e quello di ricombinazione. Lo scopo e' cercare di unire i vantaggi di due individui e anche in questo caso esistono vari modi per effettuare una ricombinazione:
- **one-point crossover**: viene indicato un punto casuale all'interno del cromosoma e vengono scambiati tutti i geni che si trovano da un lato del taglio
- **two-point crossover**: come il metodo precedente ma in questo caso si individuano due punti e si scambiano i geni individuati dall'intervallo compreso
- **n-point crossover** : in questo caso si scelgono n punti randomici e si scambiano i geni all'interno degli intervalli
- **uniform crossover**: per ogni gene si determina se scambiarlo o meno in modo casuale
- **shuffle crossover**: si permutano randomicamente i due cromosomi, si applica un one-point crossover e si riordinano i due cromosomi cosi ottenuti
- **uniform order-based crossover**: come per uniform based per ogni gene si decide se tenerlo o cambiarlo. Gli spazi dei geni da cambiare sono riempiti nell'ordine di apparizioni dei geni nell'altro cromosoma
- **edge-recombination crossover**: il cromosoma e' rappresentato come un grafo. Ogni gene e' un vertice che ha archi verso i suoi vicini. Questi archi vengono mischiati tra i due grafi. Utile perche preserva la relazione di vicinanza.
Un operatore multiple-parent e' il **diagonal crossover**. Dati k genitori si scelgono k-1 punti per il crossover e si esegue uno shift in diagonale le sequenze rispetto ai punti scelti. Il risultato sono k figli. Aumentando il numero di genitori  aumenta l'esplorazione dello spazio.
Bisogna fare particolare attenzione a due problemi che possono sorgere con gli operatori di crossover:
- **Positional bias**: la probabilita che due geni vengano ereditati insieme dipende dalla loro posizione relativa nel cromosoma. Rende la disposizione dei differenti geni cruciale per la riuscita dell'algoritmo
- **Distributional bias**: se la probabilita che i geni vengano scambiati non e' la stessa per tutti i geni
Per migliorare le performance delle soluzioni si applicano due strategie:
- **Interpolating recombination**: il valore di geni viene interpolato in modo da creare nuovi valori per il gene. Ne beneficiano particolarmente gli individui con migliore fitness.
- **Extrapolating recombination**: si inferiscono informazioni da una moltitudine di individui e si creano nuovi alleli (nuovi valori dei geni) nella presunta direzione di miglioramento. Prende in considerazione il fitness value.

#TODO  aggiungere metaeuristiche dopo l'esame

#TODO aggiungere gli algortimi culturali, swarm, ecc.. dopo l'esame

## Parallelizzazione degli algoritmi evolutivi
Rispetto alle altre metaeuristiche si osserva che gli algoritmi evolutivi spesso portano a risultati migliori ma con un tempo di esecuzione molto lento.
Si possono parallelizzare alcune fasi del procvesso per velocizzarlo, ad esempio:
- la **generazione iniziale** stando attenti a eventuali duplicati
- il **calcolo della fitness degli individui** tranne per la parte di calcolo di fitness relativo
- la **selezione** se si sceglie una fatta da elementi indipendenti come tournament selection
- le **mutazioni**
- il controllo di raggiungimento del criterio di terminazione
In letteratura vengono usate principalmente due architetture:
- **island model**: viene suddivisa la popolazione su isole in cui ognuna seguira il suo processo evolutivo. E' possibile introdurre la migrazione di individui sia in modo randomico (da una qualsiasi isola a qualsiasi altra) o grafato (si collegano le isole tramite archi sui quali puo avvenire la migrazione)
- **cellular evolution**: I processori vengono organizzati a griglia e ognuno e' responsabile di un cromosoma. Per la selezione ogni processore sceglie il migliore tra i quattro processori adiacenti. Gli operatori genetici vengono applicati solo tra il cromosoma scelto e il proprio. Il miglior figlio risultante diventa il cromosoma del processore.