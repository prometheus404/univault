I fuzzy set sono un'estensione della definizione di insieme classico: mentre in questi ultimi gli elementi possono solo appartenere o no all'insieme nel caso degli insiemi fuzzy e' possibile modellare l'appartenza parziale di un elemento a uno o piu insiemi.
Dato un dominio del discorso X, un insieme fuzzy $\mu$ e' infatti una funzione che assegna a ogni elemento $x \in X$ un grado di appartenenza rispetto all'insieme $\mu$ (invece che semplicemente 1 o 0 come nel caso degli insiemi crisp).
Il grado di appartenenza di un elemento e' convenzionalmente (ma non sempre) un numero tra 0 e 1 e puo essere interpretato in modi diversi a seconda dell'applicazione:
- somiglianza
- preferenza
- possibilita

La prima e' l'interpretazione piu popolare e vede il grado di appartenenza come **grado di vicinanza** rispetto a un elemento che appartiene definitivamente all'insieme. Questa interpretazione e' utilizzata nei problemi di pattern classificatione e cluster analysis.
Il secondo caso indica una **preferenza** nella scelta degli elementi di un insieme inteso come ordine di preferenza. Questa interpretazione viene utilizzata nei problemi di ottimizzazione fuzzy e nella teoria decisionale ed e' spesso derivata dalla somiglianza.
L'ultima delle tre intrepretazioni esprime la **possibilita** che un elemento $u$ sia il valore del parametro $X$ e ha l'obiettivo di distinguere cosa sia plausibile da cosa sia impossibile: piu grande $\mu$ piu e' plausibile $u$. Questa interpretazione viene usata in data analysis

Oltre alla definizione di insiemi fuzzy come funzione (rappresentazione **verticale**) e' possibile definire un insieme fuzzy definendo una famiglia di alph-cut  (rappresentazione **orizzontale**)
Un alpha-cut di un insieme $\mu$ e' definito per un qualsiasi valore di $\alpha \in [0,1]$ come $$[\mu]_\alpha = \{x \in X | \mu(x) \geq \alpha\}$$ e per ricavare la funzione e' possibile calcolare l'inviluppo superiore dei suoi alpha cut.
Questa definizione e' particolarmente utile per la rapperesentazione degli insiemi fuzzy nei computer: ci si limita a considerare un numero finito di alpha-cut rilevanti e gli insiemi possono essere cosi conservati in memoria come catene di liste lineari dove ogni lista e' l'unione di intervalli rappresentati dai loro estremi.

Concetti importanti sono:
- **supporto** -> l'insieme booleano che contiene tutti e soli gli elementi del dominio del discorso aventi un grado di apparteneza non nullo all'insieme fuzzy $\mu$
- **core** -> l'insieme booleano che contiene tutti e soli gli elementi del dominio aventi un grado di appartenenza uguale a 1
- **altezza** -> il piu alto grado di appartenenza ottenibile da un elemento di $\mu$. Se l'altezza di un insieme $h(\mu)$ e' 1 l'insieme viene definito *normale* altrimenti viene definito *subnormale*.
- **convesso** -> un insieme fuzzy viene definito convesso se e solo se tutti i suoi alpha cut sono continui.
- **numero fuzzy** -> un insieme fuzzy normale i cui alpha cut sono tutti chiusi, limitati e convessi

# Logica fuzzy
E' possibile ridefinire gli operatori logici booleani per adattarli alla logica fuzzy e costruire sopra di essi una teoria degli operatori insiemistici fuzzy. Questi operatori booleani non sono univoci: e' infatti possibile definirne diversi a patto che rispettino le proprieta' che intuitivamente dovrebbe avere quell'operatore. Ovvero:

---
-  per il **complemento fuzzy/negazione fuzzy**
	1. $\neg(0) = 1$
	2. $\neg(1) = 0$
	3. $x\leq y \to \neg x \geq \neg y$
	
	oltre a queste proprieta base la negazione puo soddisfarne altre come il fatto di essere strettamente decrescente ($x < y \to \neg x > \neg y$) o che sia continua o ancora involutiva ($\neg \neg x = x$) o stretta (se e solo se e' strettamente decrescente e continua) o forte (se e solo se e' stretta e involutiva)
-  per la **T-norma**
	1. $\top(x,1) = x$;
	2. **commutativa**: $\top(x,y) = \top(y,x)$;
	3. **associativa**: $\top(x,\top(y,z)) = \top(\top(x,y),z)$;
	4. **monotonicità** $y \leq z \to \top(x,y) \leq \top(x,z)$.
-  per la **T-conorma**
	1. $\bot(x,0) = x$;
	2. **commutativa**: $\bot(x,y) = \bot(y,x)$;
	3. **associativa** $\bot(x,\bot(y,z)) = \bot(\bot(x,y),z)$;
	4. **monotonicità** $y \leq z \implies \bot(x,y) \leq \bot(x,z)$.
- per l'**implicazione fuzzy** che $I(a,b) = \neg a \vee b$ che a seconda di come sono stati definiti gli altri operatori puo diventare 
	1. S-implication: $I(a,b) = \bot(\neg a, b)$
	2. R-implication: $I(a,b) = sup\{x \in [0,1] | \top(a,b) \leq b\}$
	3. QL-implication: $I(a,b) = \bot(\neg a, \top(a,b))$

---

Nella logica fuzzy il grado di appartenenza all'intersezione di due insiemi fuzzy e' dato dalla t-norma del grado di appartenenza dell'elemento ai due insiemi fuzzy e analogamente il grado di appartenenza all'unione di due insiemi fuzzy e' dato dalla t-conorma del grado di appartenenza dell'elemento ai due insiemi fuzzy.
Si può notare come l'operazione di minimo sia la più  grande t-norma e il massimo sia la piu' piccola t-conorma ma si possono usare altre t-norme e t-conorme come per esempio quelle di Łukasiewicz o il prodotto e somma drastica.

# Fuzzy controller
Un'importante applicazione dei sistemi fuzzy e' quella dei cosiddetti fuzzy controller. Questi permettono di definire un controller non lineare basato su tabelle con i diversi stati del sistema dove la funzione di transizione non-lineare puo essere definita senza specificare ogni singola entry della tabella. In questo modo e' possibile modellare sistemi complessi le cui dinamiche possono sfuggire a un'analisi matematicamente precisa.
Un fuzzy controller e' costituito da **fuzzyfication interface** che riceve i valori in input e li converte in un dominio adeguato (termini linguistici o fuzzy set). Questa interfaccia usa le funzioni di appartenenza definite dall'architetto del controller basandosi sulla conoscenza dell'ambiente. Contiene inoltre una **knowledge base** composta da data base (che consiste di informazioni riguardanti intervalli, trasformazioni di dominio e a quali insiemi fuzzy corrisponderanno i termini linguistici) e rule base (controlli del tipo if-then). Una **decision logic** che rappresenta l'unita processore che si occupa di computare l'output in base all'input misurato e alla knowledge base. Infine una **defuzzyfication interface** che traduce l'output fuzzy in un output booleano che verra inviato come segnale ai controlli di sistema. Esistono vari metodi di defuzzyficazione:
- Max Criterion Method (**MCM**): sceglie il valore di output y tale per cui $\mu(x_1,..x_n)$ raggiunga il massimo grado di appartenza. E' un modo facilmente applicabile in ogni contesto ma in caso di valori multipli con lo stesso grado di appartenenza non risulta deterministico
- Mean of Maxima (**MOM**): considera tutti i valori con il massimo grado di appartenenza e ne calcola la media. Il vantaggio e' la facilita di computazione e ha un comportamento deterministico Ma non e' sempre applicabile (il set di $y_i$ deve essere necessariamente un intervallo e deve esistere un insieme di valori misurabili)
- Center of Gravity (**COG**): invece di considerare solo i valori massimo li considera tutti e calcola la media dei valori di output pesata per il grado di appartenenza. Il vantaggio di questa tecnica consiste nel considerare tutti i valori cosi da ottenere un comportamento molto piu preciso ma ha elevati costi computazionali e il risultato puo non essere intuitivo
## Mamdani controller
e' il primo modello di fuzzy controller ed e' basato su un insieme finito R di regole if-then della forma
$$R: \text{ If }x_1 \text{ is } \mu_{R}^{(1)} \text{ and } \dots \text{ and } x_n \text{ is } \mu_{R}^{(n)} \text{, then } y \text{ is } \mu_R$$
dove le x sono le variabili di input del controller, y e' il valore di output. Queste devono essere interpretate come funzioni definite a tratti e possono assumere come valori intervalli crisp o valori fuzzy.
Le tre fasi di computazione del controller sono:
1. fase di fuzzyficazione dove l'input crisp esterno viene trasformato in un grado di appartenenza agli insiemi fuzzy delle differenti regole del controller
2. viene calcolato per ogni regola il valore minimo tra i gradi di appartenenza del vettori di input alle differenti componenti della regola e salvare il vutoff nella rispettiva output rule. Per ogni valore del dominio dell'output si sceglie il massimo tra i cut off ottenuti.
3. Una fase di defuzzyficazione tramite MCM, MOM o COG