# Transmission systems
## Rotation transmission
In robotics we usually have electric motors with high velocities and small torques. On the other hand for the typical robot application we need small movements with high torques.

We know that, given $N$ as the ratio between the number of teeth of the input gear and the number of teeth of the output gear ($\frac{Q_{in}}{Q_{out}}$), the resulting torque $T_{out}$ is:
$$T_{out} = \eta NT_{in}$$
And that the resulting rotation $\theta_{out}$ of the outer gear is:
$$ \theta_{out} = \frac{\theta_{in}}{N}$$
N can also be defined as:
$$N = \frac{\theta_{in}}{\theta_{out}}=\frac{r_{out}}{r_{in}}=\frac{Q_{in}}{Q_{out}}$$
>[!note]
> If $N>1$ the gear is called __reducer__ and as the name suggests reduces the rotation speed of the motor and also increases its torque. This is almost always the case in robotics applications
> if $N<1$ the gear increases speed and reduces torque

> [!note]
> $\eta$ is the efficiency of the gear. While two gears with different $\eta$ will have the same output velocity, the same is not true with torque and if we want a high torque system we should find gears with $\eta$ as close to $1$ as possible
### Energy and efficiency
The efficiency $\eta$ of a gear is equivalent to the ratio between the output energy and the input energy. Since in real system some energy is lost (turning into heat) $\eta$ is always less than one.
$$\eta= \frac{E_{out}}{E_{in}} < 1$$
The rotational energy of a rotation is defined by the torque times the rotation
$$E_{rot} = \mathcal{T}\cdot\theta$$
So we can also define $\eta$ as:
$$\eta = \frac{E_{out}}{E_{in}}=\frac{\mathcal{T}_{out}\cdot\theta_{out}}{\mathcal{T}_{in}\cdot\theta_{in}} = \frac{\mathcal{T}_{out}}{\mathcal{T}_{in}}\cdot\frac{1}{N}$$
## Rack & pinion
#TODO aggiungere immagine rack and pinion system
These kind of systems are used to transform a rotation movement into a linear movement.
$$\theta=\frac{d}{r}$$
$$N = \frac{\theta}{d}$$
$N$ is measured in $[ rad/m ]$

### Energy and efficiency
In linear system is defined as the force times the distance
$$E_{lin} = F \cdot d$$
So we can define the efficiency of a rack and pinion system as:
$$\eta= \frac{E_{out}}{E_{in}} = \frac{F_{out}\cdot d}{\mathcal{T}_{in}\cdot\theta_{out}} = \frac{F_{out}}{\mathcal{T}_{in}\cdot N}$$
