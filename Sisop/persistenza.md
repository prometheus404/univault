# Persistenza
Due astrazioni fondamentali:
- file: un array lineare di byte che può essere scritto o letto associato a un nome di basso livello (solitamente un numero) chiamato **inode number**. Nella maggior parte dei sistemi il SO non sa molto sul file ma il suo compito è solo quello di mantenere qel file in modo persistente.
- Directory: come un file ha un inode number ma i suoi contenuti sono specifici e gestiti dal file system. Contiene una lista di coppie user name/low level name dei file in essa contenuti.

La gerarchia delle directory inizia nella root directory e usa dei separatori per indicare le successive sottodirectory fino al nome del file desiderato. Possiamo riferirci a un fle con il suo **absolute pathname**. Directory e file possono avere lo stesso nome solo se sono in differenti posizioni dell'albero delle directory (non nella stessa cartella).
Il file `ciao.txt` è formato da due parti separate da un punto: la prima è un nome a scelta mentre la seconda è l'estensione del file che idica che tipo di file è (in questo esempio un file di testo). Da notare che solitamente non viene richiesto dal SO, il quale riesce a capire lo stesso che tipo di file è.
# Interfaccia file system
[[API]]