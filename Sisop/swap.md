# Swap
Fino ad ora abbiamo sempre ipotizzato che tutte le pagine fossero nella memoria fisica, Però per poter supportare [[address space]] grossi il [[SO|sistema operativo]] deve avere un posto dove poter mettere via delle porzioni di memoria che non sono utilizzate al momento.
Tutto quello che dobbiamo fare è riservare dello spazio sul disco per spostare avanti e indietro le pagine, questo spazio viene generalmente chiamato swap space. Intuitivamente il sistema operativo dovrà tenere in memoria da qualche parte l'indirizzo su disco delle pagine. La dimensione dello swap space è molto importante perchè determina il massimo numero di pagine che possono essere usate in un sistema.
## Meccanismi
### Present Bit
Supponiamo di avere un sistema con un [[TLB]] gestito dall'hardware. Al fine di permettere lo swap in memoria dobbiamo fare in modo che le PTE contengano un bit che indica se la pagina è presente o no in memoria. Se il VPN non viene trovato all'interno del [[TLB]] si ha una [[TLB]] miss e l'hardware cerca in memoria (usando il page table base register) e cerca la page table Entry. Se la pagina è valida e presente in memoria l'hardware estrae il PFN dalla PTE, lo carica nel [[TLB]] e riprova l'istruzione.
### Page Fault
La pagina però potrebbe non essere presente in memoria, in questo caso viene generata una **Page Fault**, a questo punto a prescindere da chi gestisca il [[TLB]] entra in gioco il [[SO|sistema operativo]] per gestirla tramite il **page fault handler**. Come fa il sistema operativo a capire dove si trova la pagina? La page table è il posto più naturale in cui segnare questa informazione, quindi il [[SO|sistema operativo]] può usare i bit nella PTE che normalmente vengono usati per il PFN per mettere l'indirizzo su disco. Quando l'[[IO devices|I/O]] su disco termina il [[SO]] aggiornerà la page table per segnare la pagina come presente e riesegurà l'istruzione che genererà un miss (oppure aggiornerà direttamente anche il [[TLB]]). Alla fine l'ultimo tentativo troverà la traduzione corretta e recupererà il dato desiderato. Nota che mentre sta recuperando la pagina da disco il [[processo]] che l'ha richiesta è bloccato, quindi il [[SO]] è libero di eseguire un qualsiasi altro [[processo]] pronto.
```c
//page fault handler
PFN = FindFreePhysicalPage();
if (PFN==-1) 						//no free page found
	PFN = EvictPage() 				//run replacement algorithm
DiskRead(PTE.DiskAddr, PFN); 		//sleep waiting for I/O
PTE.present = True; 				//update page table with present
PTE.PFN = PFN; 						//bit and traslation
RetryInstruction(); 				//retry
```
#TODO aggiungere il codice di quello che fa il [[TLB]]
### Memoria piena
Fino ad ora abbiamo sempre ipotizzato che la nostra memoria avesse abbastanza spazio per caricare la nuova pagina, ma questo non è detto. Il [[processo]] di scegliere una pagina da mandare su disco è definito page-replacement policy. Non per forza il sistema operativo aspetta che la memoria sia completamente piena prima di swappare. Per avere sempre una piccola porzione di memoria libera, la maggior parte dei [[SO|sistemi operativi]] ha due livelli chiamati High-watermark e Low-watermark, quando il [[SO]] nota che ci sono meno di LW pagine libere, un [[Thread]] in background (swap o page daemon) che si occupa di liberare memoria viene eseguito finchè non ci sono almeno HW pagine libere.
Liberandosi di abbastanza pagine in contemporanea il [[SO|sistema operativo]] può raggruppare più scritture su disco ottimizzando i tempi.
## Policy
La decisione di quale pagina sostituire è compito della page replacement policy. La memoria può essere vista come una cache per le pagine di memoria virtuali presenti sul sistema. Il nostro obiettivo nello scegliere una replacement policy è quello di minimizzare i cache miss. Perciò una metrica che introdugiamo è l'**AMAT**(Average memory access time):
$$AMAT = T_M + (P_{Miss}\times T_D)$$
dove
- $T_M$ = costo per accedere alla memoria
- $T_D$ = costo per accedere a disco
- $P_{Miss}$ = probabilità di non trovare la pagina in memoria ($\frac{\text{n pagine su disco}}{\text{tot pagine}}$)

Siccome il costo di accesso a disco nei sistemi moderni può essere fino a tre ordini di grandezza più lento del tempo di accesso a memoria, domina su tutte le altre misure.

### Policy ottimale
Per poter valutare l'efficienza delle policy di cui parleremo conviene valutare come si comportano in riferimento a una policy ottimale. Fortunatamente questa policy ottimale è molto semplice: basta swappare la pagina che verrà usata più in là nel tempo, Sfortunatamente questa policy è impossibile da eseguire su un sistema come quello di un personal computer (non sappiamo il futuro, quindi non sappiamo quando saranno usate le pagine).
### FIFO
Molti tra i primi sistemi per evitare la complessità di un algoritmo che si avvicinasse all'ottimo implementavano una struttura a coda in cui venivano messe le pagine quando entravano nel sistema e quando serviva swappare si toglieva quella meno recente. Fifo funziona decisamente peggio del caso ottimale perchè non riesce a dare importanza alle pagine: se è vecchia ma viene usata tanto verrà buttata fuori comunque.
### Random
Allo stesso modo una policy che veniva utilizzata era quella random: semplicemente viene ogni volta scelta una pagina a caso che viene scartata.
In questo caso le performance sono molto dipendenti dal caso ma in generale nel caso medio sono molto peggio delle prformance del caso ottimo
### LRU
Sfortunatamente le policy come FIFO o Random hanno un problema comune: potrebbero sbarazzarsi della pagina più importante in quel momento. Una soluzione è mantenere una sorta di traccia dell'utilizzo di una pagina. Per esempio LRU tiene conto di quando è stata utilizzata una pagina e butta fuori quella usata più indietro nel tempo. Questa policy si basa sul concetto di località temporale. esiste anche LFU che si libera della pagina usata meno spesso. LRU e LFU si comportano abbastanza bene rispetto a Fifo e Random
### Workload 
Proviamo ora a testare i nostri algoritmi su tre tipi di workload differenti. 
#### Random
![[random_workload.png]]
Notiamo che quando si usa un algoritmo random non importa l'algoritmo, performano tutti in maniera simile se non possono conoscere il futuro. L'ottimo invece performa decisamente meglio.
#### 80-20
![[80-20_workload.png]]
In questo workload l'80% dei riferimenti viene fatto al 20% delle pagine e il 20% al restante 80%. LRU in questo caso performa un po' meglio di random e FIFO e un po' peggio dell'ottimo. La domanda spontanea a questo punto è: importa così tanto quel minimo di cambio di performance tra LRU e Random o FIFO? se ogni miss è molto costoso (e di solito è questo il caso visto che come detto prima il disco è fino a 1000 volte più lento della ram) questa differenza ha un grosso impatto sulle performance, altrimenti ovviamente non ha molto senso avere un algoritmo più complesso.
#### Looping sequence
![[looping_workload.png]]
In questo caso accediamo a delle pagine a ciclo dalla prima all'ultima e ricominciando. Questo è un caso pessimo sia per FIFO che per LRU (e non è un workload così strano in molti contesti tipo nei database) perchè viene sempre buttata via una pagina che sarebbe stata utile a breve. Random invece performa abbastanza bene ma comunque molto distante dall'ottimo.
### Implementazione di LRU
Sfortunatamente gli algoritmi storici non sono banali da implementare: A ogni accesso ad una pagina dovremmo eseguire un update di una struttura dati in cui specifichiamo quando abbiamo usato l'ultima volta quella pagina. FIFO, che comunque deve fare un update a ogni caricamento, è più semplice. Inoltre quando dobbiamo swappare una pagina il [[SO]] dovrebbe cercare per TUTTA la struttura dati per cercare l'entry usata prima. Come abbiamo visto però le pagine su un sistema possono essere tante quindi questo diventa presto problematico.
Una soluzione è approssimare LRU in modo da rendere più fattibile l'implementazione.
Questa idea ha bisogno di un po' di supporto da parte dell'hardware che deve tenere un reference bit per ogni pagina e ogni volta che la pagina viene utilizzata deve cambiare il valore di quel bit a 1. L'hardware non cambia mai il valore del bit a 0. A questo punto possiamo applicare un qualsiasi algoritmo nella parte del [[SO|sistema operativo]], noi prendiamo in considerazione il clock algorithm:
Immaginiamo di disporre tutte le pagine in una struttura circolare e facciamo puntare al [[SO]] una di esse (come una lancetta punta l'ora), a questo punto il [[SO]] controlla il reference (o use) bit della pagina e se è 0 la swappa, altrimenti lo mette a 0 e passa alla successiva. In questo modo siamo sicuri di aver tolto una pagina che non serviva (o male che vada una pagina a caso nel caso peggiore in cui siano tutte occupate). Questo algoritmo performa quasi come LRU  ma è decisameente meno complesso da implementare.
![[80-20_clock_workload.png]]
Una piccola modifica a questo algoritmo potrebbe essere quella di controllare anche il dirty bit. Infatti se devo swappare una pagina con dirty bit a 1 devo prima copiarla in memoria, mentre se devo swappare una pagina con dirty bit a 0 invece posso farlo gratis senza dover fare una scrittura su disco (che è lenta). Per questo un algoritmo modificato potrebbe prima fare un giro per vedere di trovare una pagina con entrambi i bit a 0 e poi una con solo il reference bit a 0 mettendo a 0 quelli a 1 che incontra.
### Quando swappare
Il [[SO]] deve decidere anche quando portare una pagina in memoria (page selection policy). Nella maggior parte dei casi il sistema semplicemente carica le pagine quando vengono richieste, ma il [[SO]] potrebbe cercare di indovinare quando una pagina sta per essere richiesta e caricarla prima riducendo gli overhead in fase di esecuzione (chiamato prefetching). Il prefetching può essere fatto solo quando si è quasi sicuri che la pagina venga richiesta altrimenti ho fatto una read molto costosa per nulla.
### Come swappare
Bisogna inoltre decidere come scrivere su disco, un [[SO]] potrebbe decidere di scrivere dopo ogni swap oppure scegliere più pagine alla volta in modo da scriverle tutte assieme e farlo in modo più efficiente
### Trashing
Quando la memoria è troppo piena il sistema perde gran parte del suo tempo a fare swap. per evitare questa cosa ci sono vari approcci:
I primi sistemi avevano algoritmi complessi che permettevano di prevenire il trashing eseguendo un sottoinsieme di sottoprocessi alla volta sperando che non saturassero la memoria. Alcuni sistemi come alcune versioni di Linux invece usano un approccio più drastico scegliendo semplicemente un [[processo]] che usa tanta memoria e killandolo. Tutto divertente finchè non viene ucciso il server grafico.