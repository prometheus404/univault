# Esercizio calcolo Random e Sequetial bandwidth
- average seek time = 7ms
- average rotational delay = 3ms
- transfer rate of [[HDD|disk]] = 50MB/s
##### Sequential workload
patiamo con l'ipotesi che venga richiesto un chunk molto grosso e contiguo
$$ \text{time to transfer data} = \frac{10MB}{50MB/s} = \frac{1}{5}s = 200ms$$
$$ \text{time to access} = 200ms + 7ms + 3ms = 210ms$$
$$S = \frac{\text{amount of data}}{\text{time to access}} = \frac{10MB}{210ms} = 47.62 MB/s$$
##### Random workload
Calcoliamo ora il caso di piccole richieste in location diverse
$$ \text{time to transfer data} = \frac{10KB}{50MB/s} = 0.195ms$$
$$R = \frac{\text{amount of data}}{\text{time to access}} = \frac{10KB}{10.195ms} = 0.981 MB/s$$