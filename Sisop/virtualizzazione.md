Il [[SO]] prende una risorsa fisica e tramite [[astrazioni|astrazione]] la trasforma in una sua versione virtuale permettendone quindi una maggior facilita di utilizzo.
A seconda della risorsa virtualizzata vedremo:
- [[virtualizzazione cpu]]
- [[virtualizzazione memoria]]

Per virtualizzare il [[SO|Sistema Operativo]] deve fare uso sia di [[meccanismi]] sia di [[policy]].