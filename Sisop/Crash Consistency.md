# Crash consistency
A differenza di molte strutture dati che abbiamo visto finora il filesystem deve essere persistente. Una delle principali difficoltà che si incontrano nel costruire un file system è come aggiornare strutture dati persistenti nonostante la presenza di crash: se accadono durante l'aggiornamento di strutture dati possono lasciare il filesystem in uno stato inconsistente.

Immaginiamo di voler aggiungere un singolo blocco in coda a un file su un file system come quello che abbiamo visto in [[vsfs]].
In questo caso dobbiamo aggiornare la data bitmap, l'inode bitmap e il blocco dati che abbiamo aggiunto. Questo vuol dire fare tre scritture che però non è assolutamente detto siano atomiche. Cosa succede se avviene un crash mentre stiamo eseguendo queste scritture? Si possono verificare i seguenti scenari:
- Viene scritto solo il blocco dati: ci sono ma non si possono leggere perchè non ci sono strutture che li puntano, quindi il sistema è consistente e va tutto bene (tranne per l'utente che ha perso i dati)
- Viene scritto solo l'inode: in questo caso se ci fidiamo del puntatore dell'inode troviamo un blocco spazzatura, inoltre sulla bitmap c'è scritto che il nodo puntato dall'inode è libero e quindi può essere allocato.
- Viene scritta solo la bitmap: in questo caso il blocco allocato non verrà più utilizzato dal sistema perchè considerato allocato nonostante fosse libero.
- Vengono scritti sa inode che bitmap: in questo caso i metadati sono consistenti ma abbiamo ancora un problema di dati spazzatura
- Vengono scritti inode e dati: in questo caso l'inode punta ai dati corretti ma la bitmap non è aggiornata e segna il blocco dati come libero
- Vengono scritti sia bitmap che dati: il blocco è allocato correttamente ma non si può sapere a che file appartiene.

Quello che vorremmo fare idealmente è avere un sistema che si sposti da uno stato consistente a un altro in modo atomico

## Prima soluzione: File system Checker
I primi file system risolvevano con questo approccio molto semplice: lasciavano che si formasse lo stato inconsistente e poi lo sistemavano in fase di boot. Questo approccio non risolve tutti i problemi, per esempio nel caso dei dati spazzatura non ha modo di capire in che modo sono diversi dai dati utente. L'unico scopo è rendere consistenti i metadati.
Quello che fa fsck è controllare:
- Superblocco: per prima cosa controlla il superblocco per controllare che sembri sensato, in caso contrario il sistema o un amministratore decide se usarne un'altra copia
- Blocchi liberi: dopodichè fsck controlla gli inode (tutti i blocchi compresi quelli indiretti) per creare delle bitmap corrette che sostituisce a quelle errate. Fsck si fida degli inode.
- Stato inode: Ogni inode viene controllato per vedere se è corrotto. Se un problema non può essere fixato facilmente fsck lo cancella e aggiorna la bitmap.
- Link degli inode: Controlla anche che il link count di ogni inode allocato corrisponda a numero di riferimenti
- Duplicati: nel caso inode diversi si riferiscano allo stesso blocco controlla se un inode è visibilmente corrotto, altrimenti copia il blocco permettendo ad entrambi di averlo
- Bad block: controlla i puntatori a blocco corrotti e li rimuove
- Directory check: fsck non comprende il contenuto dei file utente e non può controllare che siano corrette, ma le cartelle sono formattate in modo standard quindi possono essere controllate.

fsck è troppo lento, soprattutto con i dischi moderni, bisogna trovare una nuova soluzione.

## Seconda soluzione: Journaling
Quando viene eseguito un aggiornamento del [[HDD|disco]]. prima di sovrascrivere le strutture dati scriviamo una nota descrivendo quello che abbiamo intenzione di fare. In questo modo nel caso di crash si può ripetere quello che abbiamo scritto in queste note e avere un sistema consistente

| super | journal | group0 | group1 | ... | GroupN | 
| ----- | ------- | ------ | ------ | --- | ------ |
struttura di un file system che esegue journaling

### Data Journaling
Prima di scrivere qualsiasi cambiamento su [[HDD|disco]] lo scriviamo nel log:

| journal: | TxB | I[V2] | B[V2] | Db  | TxE |
| -------- | --- | ----- | ----- | --- | --- |

abbiamo scritto quattro blocchi. L'inizio della transazione (txb) include informazioni sull'update, i tre blocchi in mezzo contengono i blocchi che vorremmo scrivere su disco mentre il blocco di fine transazione indica che la transazione è finita.
Le operazioni che seguiamo sono quindi:
1. Journal write: Scriviamo la transazione
2. Checkpoint: scriviamo gli aggiornamenti nelle loro posizioni definitive su disco.

Però le cose sono un po' più complesse quando il crash avviene durante la scrittura sul log. Immaginiamo che vengano scritti tutti i blocchi tranne uno tra i tre in mezzo. Questo è un grande problema di consistenza perchè poi all'avvio eseguiremo delle operazioni non corrette.
Per evitare questo problema scriviamo prima tutti i blocchi eccetto quello di fine transazione. Quando la scrittura di questi blocchi è completa scriviamo anche la fine transazione. Siccome il disco garantisce che le scritture da 512 byte sono atomiche siamo sicuri che una transazione sia chiusa in modo atomico. Il nuovo protocollo è quindi:
1. Journal write: Scriviamo la transazione
2. Journal commit: scriviamo la fine transazione su disco
3. Checkpoint: scriviamo gli aggiornamenti nelle loro posizioni definitive su disco.

Per recuperare da un crash:
se la transazione non è stata completata scartiamo tutto, altrimenti rieseguiamo in ordine le transazioni.
Siccome questo protocollo può causare molte scritture molti filesystem non eseguono un commit alla fine di ogni singolo update ma ne raccolgono più di uno in una singola transazione globale.
![[data_journaling_timeline.png]]
#### Problemi di memoria
Ci sono due problemi quando il log inizia a riempirsi: più è pieno più tempo ci vorrà all'avvio per fare recovery, inoltre se si riempe completamente non sarà più possibile aggiungere altre transazioni. Per questo vengono usate strutture circolari per memorizzare i log, in questo modo il nostro protocollo aggiornato diventa:
1. Journal write: Scriviamo la transazione
2. Journal commit: scriviamo la fine transazione su disco
3. Checkpoint: scriviamo gli aggiornamenti nelle loro posizioni definitive su disco.
4. Free: liberiamo lo spazio della transazione aggiornando il superblocco quando siamo sicuri che sia stato scritto su disco.

### Metadata journaling
Nel data journaling però dobbiamo scrivere due volte ogni scrittura su disco. Un modo più semplice e comune per fare journaling è il metadata journaling o ordered journaling. Il concetto di base è circa lo stesso, ma invece di copiare tutto nel journal mettiamo solo i metadati e scrivere direttamente su disco nel posto corretto i dati. Quando vanno scritti i blocchi dati? L'ordine è importante: i dati vanno scritti per primi, in modo che se recupero una transazione scrivendo i metadati corretti sono sicuro che i dati siano già presenti in memoria (altrimenti rischieremmo di avere dati non consistenti). Il nuovo protocollo diventa quindi:
1. Data write
2. Journal metadata write: Scriviamo la transazione
3. Journal commit: scriviamo la fine transazione su disco
4. Checkpoint: scriviamo gli aggiornamenti nelle loro posizioni definitive su disco.
5. Free: liberiamo lo spazio della transazione aggiornando il superblocco quando siamo sicuri che sia stato scritto su disco.

Da notare che non è necessario aspettare tra il primo e il secondo punto, l'importante è che i dati siano scritti **prima** che avvenga il commit della transazione.
#### Riutilizzo di un blocco
Immaginiamo ora di avere una directory chiamata foo. L'utente aggiunge una entry a foo e quindi il contenuto di foo viene scritto nel log (il contenuto delle cartelle viene considerato metadata)

| TxB | I[foo] | D[foo] | TxE |
| --- | ------ | ------ | --- |

Se a questo punto l'utente cancellasse tutti i file nella directory e la directory stessa e creasse un nuovo file che utilizza lo stesso blocco i suoi metadati verrebbero scritti su disco (ma non i suoi dati)

| TxB | I[foo] | D[foo] | TxE | TxB | I[foobar] | TxE | 
| --- | ------ | ------ | --- | --- | --------- | --- |

Se a questo punto avvenisse un crash il replay del log sovrascriverebbe il blocco dati del file foobar scrivendo il contenuto della cartella foo. Ci sono vari modi per aggirare questo problema (come per esempio non utilizzare i blocchi appena cancellati finchè i cambiamenti non sono effettivamente scritti su disco). Quello che fa ext3 è aggiungere un record revoke che indica quale record del log viene cancellato. La prima cosa che viene fatta a boottime è controllare questi revoke e ignorare i record scritti.

![[metadata_journaling_timeline.png]]

## Altri approcci
### Soft update
ordina tutte le write in modo che le strutture dati non vegano mai lasciate in uno stato inconsistente ma è complesso da implementare
### COW
copy-on-write non sovrascrive mai aree di memoria ma mette gli aggiornamenti in nuove aree di memoria. Dopo un certo numero di cambiamenti cambia i puntatori delle strutture dati per fare un commit dei cambiamenti.
### BBC
Include un puntatore in ogni struttura puntata che punta alla struttura da cui è puntata.



