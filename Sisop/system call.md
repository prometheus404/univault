---
aliases: [system call, syscall]
---
#meccanismo
# System call
Le system call permettono al sistema di esporre certi pezzi chiave delle funzioni del [[SO|sistema operativo]] ai programmi user.
Per eseguire una system call un programma deve eseguire una speciale istruzione di [[trap]] che salta in un punto preciso del kernel e setta il flag del processore in [[kernel e user mode|kernel mode]] e una volta finito il [[SO]] esegue un'istruzione di [[return from trap]] che fa tornare tutto come prima e riporta il processore in user mode.
Ovviamente il [[processo]] chiamante non può specificare un qualsiasi indirizzo del kernel a cui saltare, altrimenti potrebbe per esempio saltare tutti i controlli e eseguire solo la parte di codice che gli interessa anche se non ha i permessi per farlo, perciò il [[kernel]] specifica una [[trap table]] in [[boot time]] (e quindi in [[kernel e user mode|kernel mode]]) con le posizioni in memoria dei trap handlers.
Per sapere quale operazione eseguire a ogni system call viene assegnato un numero che viene specificato in fase di chiamata invece del vero indirizzo in memoria.