# Virtualizzazione memoria
Sui primi sistemi il [[SO]] era un insieme di librerie che si trovava in memoria e c'era un solo [[processo]] che veniva eseguito (quindi si trovava in memoria fisica) e aveva accesso a tutta la memoria (eccetto la parte dedicata al [[SO]]). Nell'era del time-sharing ovviamente tutto questo cambia: vari processi sono in memoria pronti per essere eseguiti e il sistema operativo si occupa di [[scheduling|schedularli]] in modo da permettere maggiore interattività. Un possibile metodo per virtualizzare la memoria sarebbe usare anche in questo caso la tecnica di [[time sharing]], dando ad ogni [[processo]] completo accesso alla memoria fintanto che è in esecuzione e poi salvare lo stato su un qualche disco esterno quando il SO esegue uno switch di processi. Ovviamente questa soluzione è troppo lenta (salvare grosse porzioni di dati su disco è costoso), quindi dobbiamo lasciare i processi in memoria mentre cambiamo quello attualmente in esecuzione. Dobbiamo però aggiungere dei controlli, perchè non vogliamo che i processi siano in grado di leggere la memoria di altri processi o ancora peggio del sistema operativo, nasce quindi l'astrazione di [[address space]]. In questo modo un programma in esecuzione pensa di essere in memoria in un particolare indirizzo (per esempio a partire da 0) e di avere un grande [[address space]] (per esempio 64 bit) mentre in realtà si trova in un punto in cui il SO ha trovato del posto libero e con uno spazio molto ridotto.
## Goals
- **trasparenza:** è il principale obiettivo della virtualizzazione della memoria, il sistema operativo dovrebbe implementarla in un modo che non sia visibile al programma in esecuzione.
- **efficienza:** il SO dovrebbe cercare di rendere la virtualizzazione il più effciente possibile.
- **protezione:** Il SO deve cercare di proteggere un [[processo]] da un altro e in particolare proteggere sè stesso da processi malevoli o bacati. Per questo un [[processo]] non dovrebbe essere in grado di scrivere sulla memoria di un altro, rispettando la proprietà di **isolamento** tra processi.
## Implementazione
### Ipotesi
Partiamo come al solito con delle iptesi che rilasseremo nel tempo
1. Lo user [[address space ]] deve essere un unico blocco indivisibile
2. Non deve essere grosso. In particolare deve essere più piccolo della dimensione della memoria
3. Ogni [[address space]] ha esattamente la stessa dimensione.

### Base and Bounds
È un'idea molto semplice chiamata anche Dynamic (Hardware-based) Relocation. Avremo bisogno di due registri: un base register e un bound. Ogni programma viene scritto e compilato come se venisse caricato all'indirizzo zero e successivamente, durante l'esecuzione, ogni riferimento a memoria viene tradotto semplicemente traslando gli indirizzi di una quantità pari alla base:
```
physical address = virtual address + base
```
La trasformazione avviene attraverso l' [[hardware-based address translation]].
Il secondo registro invece serve per garantire la **protezione**, infatti viene usato per controllare che l'indirizzo sia all'interno della sua area di memoria.
```
if (virtual address > bounds)
	out of bounds exception
else
	physical address = virtual address + base
```
Ovviamente in questo caso la CPU deve essere ulteriormente modificata aggiungendo questi due registri, delle istruzione per modifcarli e la possibilità di generare eccezioni nel caso di out of bound e di tentato accesso non privilegiato ai base and bounds.
Dal punto di vista del SO bisogna invece avere la capacità di gestire queste eccezioni e aggiungere base e bounds nel [[PCB]], inoltre ad ogni [[context switch]] deve caricare i registri base and bounds del processo che sta per essere eseguito.
#### Problemi di questo approccio
nonostante sia **trasparente**, **efficiente** e garantisca **protezione** ha un grosso problema: se un processo non usa tutta la sua memoria quella inutilizzata sarà sprecata, quindi eseguire tanti processi piccoli porta a **internal fragmentation**
### Segmentation
Per risolvere qesto problema e anche quello delle ipotesi 3 e 1 nasce la segmentazione: invece di avere un solo registro base e un solo registro bound nella [[MMU]] abbiamo una coppia di registri per ogni **segmento** dell'[[address space]]. Nella nostra visione canonica dell'[[address space]] quest'ultimo è formato da tre diversi segmenti: codice, stack e heap. Con la segmentazione ognuno di questi segmenti può essere caricato in memoria indipendentemente. 
#### Indirizzare il codice 
Quando viene indirizzato un indirizzo nel segmento codice di memoria l'hardware  aggiungerà il corretto valore di base all'offset dato dal processo, controllerà che sia all'interno del bound corrispondente e quindi eseguirà l'accesso a memoria.
#### Indirizzare lo heap
Nel caso di un segmento di heap seguendo questo metodo non otterremmo il corretto indirizzo. Quello che dobbiamo fare è per prima cosa sottrarre all'offset l'indirizzo della base del segmento di heap, a quel punto possiamo sommarlo alla base e continuare come sopra.
#### Indirizzare lo stack
Lo stack è un problema perchè cresce al contrario, quindi dobbiamo aggiungere un registro che oltre a base e bound indichi in che direzione cresce il segmento. Per ottenere il corretto offset negativo dobbiamo sottrarre la massima dimensione del segmento dall'offset, a questo punto sommiamo con base per ottenere il corretto indirizzo fisico
```
negatve offset = offset - address size
if (|negative offset| > bound )
	segmentation fault
else
	phisical address = negative offset + base
```
#### Come capire a cosa ci riferiamo
Come fa l'hardware a capire in che segmento deve andare? ci sono due approcci:
- **esplicito:** utilizzo i primi bit di un indirizzo virtuale per specificare in che segmento voglio andare (in questo modo i check dei bound dello heap diventano più semplici). Notare che se uso due bit e ho tre segmenti perdo dello spazio indirizzabile (una delle quattro combinazioni dei due bit) perciò alcuni sistemi usano un solo bit e accorpano codice e heap.
- **implicito:** l'hardware ipotizza il segmento a partre da dove la richiesta è arrivata: se arriva dal program counter(fetch) allora cerca nel codice, se usa lo stack pointer o il base pointer sarà nello stack altrimenti sarà sempre nello heap.
#### Supporto per lo sharing
A volte è comodo condividere alcune aree di memoria, in particolare il codice (per evitare che n programmi con la stessa code base debbano ricopiarla). Per poter condividere il codice abbiamo bisogno di un altro po' di supporto dall'hardware tramite i protection bit. Possiamo quindi permettere a più programmi di leggere le stesse aree ma a solo un sottoinsieme di modificarle.
#### Fine-grained e Coarse grained segmentation
Questo tipo di segmentazione descritta finora è di tipo Coarse-grained, infatti permettiamo di suddividere lo spazio in tre segmenti di memoria abbastanza grandi. Alcuni sistemi invece permettevano di suddividere con più precisione i segmenti (fino a supportare migliaia di segmenti). Per fare ciò però abbiamo bisogno di ulteriore supporto da parte dell'hardware sotto forma di segment table in memoria.
#### Problemi
Questo approccio ci permette di salvare molta memoria internamente a un segmento, ma ha un costo. In precedenza quando dovevamo trovare in memoria dello spazio per un nuovo processo era semplice visto che erano tuti della stessa dimensione. Ora però hanno dimensioni diverse e si crea il problema della **external fragmenation** infatti se il SO deve trovare posto a un segmento di dimensione 8Byte e lo posiziona in una zona di memoria da 10B, restano due Byte vuoti che difficilmente saranno riempiti, quindi altro spreco. Una soluzione sarebbe far girare ogni tanto un programma che compatta la memoria ma è molto dispendioso. Sono stati anche sviluppati algoritmi di scelta della posizione molto efficienti ma nonostante questo la frammentazione continuerà ad esistere e un buon algoritmo cerca solo di minimizzarla.
### Paging
Per evitare la frammentazione esterna l'unico vero modo è avere spazi di dimensione fissa, da questa intuizione nasce la paginazione. Vediamo la memoria come un array di slot di dimensione fissata chiamati **page frames**. Il principale vantaggio della paginazione rispetto alla segmentazione è la **flessibilità** e la **semplicità**, infatti un processo può essere diviso in tante piccole parti senza doversi preoccupare della direzione in cui crescono i vari blocchi e in modo assolutamente trasparente, inoltre al SO basta avere una lista delle pagine disponibili per gestire la memoria e nessun algoritmo complesso di best fit. Per sapere dove si trova ogni virtual page del processo il sistema operativo deve tenere traccia delle traduzioni degli indirizzi in una struttura (una per processo) chiamata page table
Immagniamo ora di avere un processo con un piccolo [[address space]] (64 bytes)
```
movl <virtual address>, %eax
```
per tradurre questo indirizzo bisogna dividere il ``virtual address`` in due parti: il **virtual page number(vpn)** e l'**offset**
nel caso del nostro esempio il virtual address sarà di 6 bit ($2^6 = 64$)

| Va5 | Va4 | Va3 | Va2 | Va1 | Va0 |
| --- | --- | --- | --- | --- | --- |
Sappiamo che le pagine del nostro sistema sono di 16byte ($2^4$), quindi
Virtual page number

| Va5 | Va4 |
| --- | --- |

offset (indirizza tutta la pagina)

| Va3 | Va2 | Va1 | Va0 |
| --- | --- | --- | --- |

Dopodichè avverrà la traduzione da VPN a phisical frame number e l'indirizzo fisico sarà dato da

| PFN | OFFSET |
| --- | ------ |    
#### Problema di spazio
Le page table possono diventare terribilmente grandi: immagina un sistema a 32 bit e pagine da 4Kb ($2^{12}$) quindi con un VPN da 20 bit e un offset da 12. 20 bit VPN implica $2^{20}$ traduzioni, se ogni entry occupa 4byte una page table occupa 4MB di spazio. Per ogni processo! Ovviamente non possiamo avere un buffer di memoria speciale o regstri di queste dimensioni, quindi dobbiamo mettere tutto in memoria.
#### Cosa sono esattamente le page tables
è una struttura dati usata per mappare da indirizzi virtuali a indirizzi fisici, quindi ogni struttura dati può andare bene. La forma più semplice è chiamata linear page table ed è semplicemente un array.
Ogni page table entry contiene un valid bit che indica se la traduzione è valida (per esempio lo spazio inutilizzato e che quindi non ha una pagina in memoria attiva viene segnato come non valido), Dei protection bit che indicano se la pagina può essere letta, scritta o eseguita, un present bit che indica se la pagina è realmente presente in memoria o se è stata swappata, un dirty bit che indica se la pagina è stata modificata dall'ultima volta che è stata messa in memoria e infine un reference bit che idica se è stato fatto un accesso alla pagina di recente (utile per decidere che pagina [[swap|swappare]])
#### Traduzioni lente
Durante un accesso a memoria il sistema deve prima tradurre il virtual address, quindi deve conoscere la posizione della page table (per ora immaginiamo un semplice page table base register), dividere in VPN e offset l'indirizzo e usare il VPN come indice dell'array puntato dal page table base register, a quel punto riformare l'indirizzo con il PFN e l'offset. Solo a questo punto possiamo accedere al dato desidrato in memoria. Dobbiamo quindi usare il doppio degli accessi a memoria (anche solo per recuperare il codice da eseguire) quindi il peso sulla performance è troppo.
Per questo si ricorre al [[TLB]]
#### Page table troppo grosse
Come abbiamo già detto per un sistema a 32bit tipico le pagine occuperebbero 4Mb per ogni processo (per mille processi una memoria da 4 gb sarebbe occupata solo da page table senza altro contenuto)
##### Pagine più grosse
La soluzione più semplice che viene in mente è quella di aumentare la dimensione delle pagine, il problema principale con questo approccio è che aumentando la dimensione delle pagine aumenta anche la possibilità di frammentazione interna
##### Approccio Ibrido
Quando si hanno due approcci contrastanti come per esempio la paginazione e la segmentazione può avere senso unirli in un unico approccio ibrido. Riserviamo alcuni bit dell'indirizzo virtuale per indicare il segmento.

| SN  | VPN | OFFSET |
| --- | --- | ------ |

Immaginando di avere i soliti tre segmenti, ogni processo avrà quindi tre page table di dimensione variabile e dei registri base and bound che indicano rispettivamente l'indirizzo base della page table e la loro dimensione. In questo modo per esempio non vengono allocate tutte le traduzioni tra l'heap e lo stack perchè l'ultima pagina allocata nell'heap è pari a `base[heap] + bound[heap]` . Questo approccio però ha i suoi problemi: primo tra tutti che la segmentazione non è flessibile come ci piacerebbe (per esempio con un heap sparso avremmo comunque tanto spreco di memoria) e  in secondo luogo la dimensione variabile delle page table rende più complesso trovargli posto in una memoria divisa in pagine, rendendo quindi di nuovo un problema l'external fragmentation.

##### Multilevel Page Tables
Per usare questo approccio dobbiamo prima dividere le page table in unità di dimensione pari a una pagina, poi se una pagina intera contiene solo indirizzi non validi questa non verrà proprio allocata. immaginiamo che all'inizio un programma abbia qualche pagina occupata da codice e heap (la parte di page table di quegli indirizzi è meno di 4kb) e lo stack (anche in questo caso la parte di page table contenente lo stack occupa meno di una pagina). Creo quindi una struttura chiamata Page Directory che funziona esattamente come una page table per le page table. in questa struttura avrò solo due entry occupate: una per la page table dello stack e una per la parte di page table di codice e heap, in questo modo non ho dovuto allocare tutta la page table (4mb) ma solo tre pagine (assumendo che la page directory occupi solo una pagina). Ogni entry di una page directory contiene
- valid bit che indica che almeno una delle pagine puntate dalla page table puntata è valida
- page frame number che indica dove si trova la pagina

Se costruite bene ogni porzione della page table entra in una pagina e quindi a differenza di una linear page table (per cui devo trovare 4mb di memoria contigua) posso spezzare la page table e trovargli spazio in memoria più facilmente (oltre che poterle mettere nella kernel virtual memory e swapparle in caso ci serva spazio).
Questo però ha un costo: devo fare due accessi a memoria per recuperare una traduzione, inoltre durante un tlb miss devo fare operazioni più complicate.
Per tadurre uso semplicemente una parte del VPN per indirizzare l'entry nella Page Directory (Page Directory Index) e una parte per indirizzare la page table puntata (se valida) chiamato Page Table Index.
Ovviamente anche le page directory possono diventare grandi creando lo stesso problema delle page table, quindi posso usare altri livelli di indirezione creando page table a 3 o più livelli.
##### Inverted Page Table
Un modo ancora più estremo per ridurre la dimensione delle page table è con le inverted page table. Invece di avere molte page table (una per processo) teniamo una semplice page table con una entry per ogni pagina fisica sul sistema, questa ci dice che processo sta usando la pagina e che pagina virtuale il processo mappa su quella pagina fisica. Trovare la traduzione a un VPN è semplicemente questione di cercare che pagina fisica ha quel valore. Una scansione lineare ovviamente ci metterebbe troppo, quindi viene usata una hash table