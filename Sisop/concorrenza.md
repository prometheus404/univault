## Non-Deadlock Bug
Sono la maggior parte dei bug presenti nel codice concorrente. I principali tipi di non-deadlck bug sono
### Atomicity-violation bugs
```c
Thread1::
if (thd->proc_info){
	...
	fputs(thd->proc_info, ...)
	...
}

Thread2::
thd->proc_info = NULL;
```
Se per caso il [[Thread]] 1 venisse interrotto dopo il controllo ma prima di `fputs` il secondo thread potrebbe essere eseguito in mezzo settando `proc_info` a `NULL` creando un errore. La sezione di codice è quindi pensata per essere atomica ma l'atomicità non è richiesta durante l'esecuzione.
La soluzione è smplicemente aggiungere un [[Lock]] attorno a tutto il blocco dell'if e alla parte di codice del thread 2 che cambia il valore di `proc_info`.
### Order-Violation Bugs
Capita quando A deve essere sempre eseguto prima di B ma questo ordine non viene richiesto.
```c
Thread 1::
void init(){
	...
	mThread = PR_CreateThread(mMain, ...);
	...
}
Thread 2::
void mMain(...){
	...
	mState = mThread->State;
	...
}
```
Il Thread 2 sembra assumere che la variabile `mThread` sia settata ma se il thread 2 viene eseguito immediatamente appena creato il valore di mThread non sarà settato quando verrà richiesto all'interno di mMain() nel thread2. Per risolvere questo tipo di bug bisogna far rispettare l'ordine tramite le [[Condition Variables]].
## Deadlock Bug
un prooblema classico che si presenta in molti sistemi con complessi protocolli di locking è il deadlock. Capita qando T1 ha il lock L1 e cerca di ottenere L2, che però è in possesso di T2 che sta aspettando che si liberi L1. Da notare che se anche il codice può presentare deadlock non sempre lo farà. Se T1 e T2 cercassero di accedere ai lock nello stesso ordine il dadlock non potrebbe capitare, questo però non è semplice da far rispettare in sistemi complessi con molte dipendenze. Inoltre l'encapsulation rende difficile la prevenzione di questo tipo di bug perchè non so qual'è la logica di funzionamento dei blocchi chiusi che utilizzo. Per esempio AddAll della classe java Vector viene chiamato così:
```java 
Vector c1, v2;
v1.Addall(v2);
```
Ovviamente per poter essere multithread safe vengono chiamati dei lock sui vettori per essere sicuri che altri thread non li stiano usando. Peccato che in questo caso vengano richiesti due lock e se qualche altro thread ha contemporaneamente chiamato ``v2.AddAll(v1)`` c'è la possibilità che si verifichi un deadlock.

### Condizioni affinchè si verifichi un Deadlock
- Mutual Exclusion: i thread desiderano avere controllo esclusivo su una risorsa
- Hold and wait: i thread prendono la risorsa mentre aspettano che siano disponibili le altre che gli servono
- No preemption: Le risorse non possono essere tolte forzatamente ai thread
- Circular Wait: esiste una catena circolare di thread tale che ogni thread ha una o più risorse che sono richieste dal thread successivo nella catena

Se anche una sola di queste condizioni non è verificata il Deadlock non può verificarsi.

### Prevenzione dei deadlock
#### Evitare il circular wait
Una soluzione è scrivere tutto il codice in modo che non si possa verificare circular wait. Il modo più semplice è fare in modo che ci sia un ordine globale di acquisizione dei lock. Per esempio se ho solo due lock L1 e L2 faccio in modo che L1 venga sempre richiesto prima di L2. Ovviamente non sempre questo è possibile perchè possono esserci in gioco un numero enorme di lock, per questo un'altra soluzione è l'ordinamento parziale, ovvero ordinare solo quei lock che vengono richiesti insieme.
#### Evitare hold and wait
Possiamo evitare casi di hold and wait prendendo possesso di tutti i nostri lock in maniera atomica
```c
pthread_mutex_lock(prevention);
pthread_mutex_lock(L1);
pthread_mutex_lock(L2);
...
pthread_mutex_unlock(prevention);
```
#### Evitare no Preemption
Bisogna trovare un modo di togliere le risorse a un thread se questo non riesce a prendere possesso di tutti i lock
```c
top:
	pthread_mutex_lock(L1);
	if(pthread_mutex_trylock(L2) != 0){
		pthread_mutex_unlock(L1);
		goto top;
	}
```
Però così nasce un altro problema: il livelock. È possibile, anche se estremamente improbabile, che due thread continuino ripetutamente a cercare di ottenere entrambi i lock senza riuscirci. Sono entrambi vivi ed entrambi eseguono codice ma non si va avanti nell'escuzione. Una soluzione potrebbe essere aggiungere un delay random prima di ripetere il tentatvo. Questo approccio è complesso nel caso dell'incapsulazione: se un lock si trova sotto una chiamata a un metodo per esempio è difficile implementare  un modo per tornare all'inizio della sezione di codice. Inoltre non aggiunge preemption, da solo la possibilità al programmatore di uscire in una maniera controllata da un lock.
#### Evitare la mutua esclusione
Usando istruzioni hardware abbastanza complesse si possono creare strutture dati senza l'utilizzo di lock.
Per esempio supponendo di aver una compare-and-swap implementata dall'hardware nel seguente modo:
```c
int CompareAndSwap(ind *address, int expected, int new){
	if(*address == expected){
		*address = new;
		return 1 		//success
	}
	return 0; 			//failure
}
```
se ora volessimo incrementare un valore di una certa quantità potremmo scrivere:
```c
void AtomicIncrement(int* value, int amount){
	do{
		int old = *value;
	} while (CompareAndSwap(value, old, old + amount) == 0);
}
```
In questo modo non è più possibile avere deadlock (ma non ci sbarazziamo del livelock). Questo metodo si può usare per cose anche più complicate come per esempio inserire elementi in una lista.
```c
void insert(int value){
	node_t *n = malloc(sizeof(node_t));
	assert( n != NULL);
	n->value = value;
	do{
		n->next = head;
	}while(CompareAndSwap(&head, n->next, n) == 0)
}
```
Ovviamente per avere una lista funzionante servono anche altre cose e costruire una lista dove si possono effetture ricerche, aggiungere e eliminare non è banale.
### Deadlock avoidance via scheduling
Invece di prevenire deadlock, in alcuni scenari è preferibile evitarli. per esempio assumiamo di avere due processori e quattro thread che devono essere schedulati tra di loro. I thread richiedono i segueti lock:

| x  | T1 | T2 | T3 | T4 |
|----|----|----|----|----|
| L1 | si | si | no | no |
| L2 | si | si | si | no |

uno [[scheduling|scheduler]] intelligente schedulerebbe in modo da non sovrapporre due thread che richiedono gli stessi lock.
![[scheduler_deadlock_avoidance.png]]
da notare che va bene che due thread si sovrappongano a patto che non richiedano più di un lock contemporaneamente (deve esserci una race condition su almeno due lock per provocare deadlock)

### Detect And Recover
un'ultima strategia generale è lasciare che i deadlock ci siano e poi fare qualcosa quando vengono riconosciuti. Se i deadlock sono rari queste non soluzioni possono essere la soluzione migliore. Molti database fanno così: un deadlock detector viene eseguito in background periodicamente alla ricerca di cicli nel grafo delle dipendenze. Se ne trova riavvia il sistema.