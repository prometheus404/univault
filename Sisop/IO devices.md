---
aliases: I/O
---
# I/O devices
Iniziamo a vedere un classico diagramma di un tipico sistema. C'è una singola CPU connessa alla memoria tramite a un qualche tipo di memory bus. Poi ci sono dei device connessi tramite un I/O generico (nei sistemi moderni PCI) e infine più in basso ancora bus periferici (SATA, SCSI, USB)  che connettono device lenti. Il perchè di questa struttura gerarchica è dato da due fattori: fisici(i memory bus ad alte prestazioni non possono connettere molti dispositivi) e di costo(sviluppare bus ad alte prestazioni è estremamente costoso).
## Un tipico device
Un tipico device ha due componenti importanti:
- una interfaccia hardware che presenta al resto del sistema
- la sua struttura interna (device semplici sono formati da uno o pochi chip che implementano le funzionalità, mentre device molto complessi includono CPU sulla quale gira il firmware ovvero software interno all'hardware, memoria, ...)

## Protocollo canonico
Il protocollo canonico ha quattro step:
1. il [[SO]] aspettta finchè il device è pronto leggendo ripetutamente il registro di stato (**polling**)
2. il SO manda dei dati sul registro dati
3. Il SO scrive un comando sul command register, in questo modo il device sa in modo implicito che i dati sono pronti e in modo esplicito l'operazione da eseguire
4. Il SO aspetta che il device finisca il comando

Questo protocollo base ha di positivo il fatto di essere semplice e di funzionare. Però ci sono delle inefficienze a partire dal polling.
## I/O interrupt
Invece di fare polling, il SO può mandare una richiesta, mettere il processo chiamante in attesa a dormire e fare [[context switch]] su un altro task. Quando il device ha finito fa scattare un interrupt hardware che farà faltare il SO in una predeterminata routine per gestire l'interrupt (interrupt handler). in questo modo gli interrupt permettono di sovrapporre la computazione e l'I/O
situazione con polling:

| CPU | 1   | 1   | 1   | 1   | p   | p   | p   | p   | 1   | 1   | 1   | 1   |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| DISK| -   | -   | -   | -   | 1   | 1   | 1   | 1   | -   | -   | -   | -   |

situazione con interrupt:


| CPU  | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   | 1   | 1   | 1   | 1   |
| ---- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | 
| DISK | -   | -   | -   | -   | 1   | 1   | 1   | 1   | -   | -   | -   | -   |

Da notare che usare gli interrupt non è sempre la soluzione migliore. Per esempio immagina un device che esegue i suoi task in modo molto veloce, in questo caso eseguire lo switch a un altro processo potrebbe rallentare il sistema perchè potrebbe essere più lento del device.
Perciò se la velocità del dispositivo non è conosciuta o non è costante, può essere una buona soluzione usare un approccio ibrido che fa polling per un po' e poi passa ad usare gli interrupt.
Inoltre nelle reti se arriva un gran numero di pacchetti è possibile che il sistema vada i livelock perchè processa solo gli interrupt senza lasciare che il processo possa effettivamente servire le richieste. In questo caso è meglio controllare occasionalmente tramite polling cosa sta succedendo e far accumulare paccheti.
Una ottimizzazione che possiamo fare in questo senso è far aspettare un po' al device prima di mandare l'interrupt alla CPU in modo da far accumulare le richieste e accorpare tutto in un solo interrupt.

## Direct Memory Access
C'è un altro problema del protocollo standard: nel caso di grandi quantità di dati la cpu deve passare una quantità di tempo considerevole a spostare quei dati sul device.


| CPU  | 1   | 1   | c   | c   | c   | 2   | 2   | 2   | 2   | 1   | 1   | 1   |
| ---- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| DISK | -   | -   | -   | -   | -   | 1   | 1   | 1   | 1   | -   | -   | -   |

La soluzione a questo problema è il DMA, ovvero un device specifico su un sistema che si occupa dei trasferimenti:
Per trasferire dei dati al device per esempio il SO programma il DMA dicendo dove inizia la memoria, quanti dati spostare, a che device mandarli. il DMA controller si occuperà di tutto e manderà un interrupt a trasferimento completo.

| CPU  | 1   | 1   | 2   | 2   | 2   | 2   | 2   | 2   | 2   | 1   | 1   | 1   |
| ---- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| DMA  | -   | -   | c   | c   | c   | -   | -   | -   | -   | -   | -   | -   |
| DISK | -   | -   | -   | -   | -   | 1   | 1   | 1   | 1   | -   | -   | -   |

## Metodi di interazione con i device
Inizialmende esistevano esplicite istruzioni di I/O, per esempio per mandare dati a un device, il chiamante doveva specificare il registro da cui scrivere il dato e una porta che indicava il device su cui scrivere.
Queste istruzioni necessitano solitamente di privilegi.
Il secondo metodo è conosciuto come memory mapped I/O. L'hardware rende accessibili i registri del dispostivo come se fossero posizioni di memoria.
Non c'è particolare vantaggio in un approccio o l'altro.

## Driver
Un ultimo problema è che vorremmo costruire un file system che lavori sui dischi a prescindere dal fatto che siano USB, IDE, ... e vorremmo che il file system sia all'oscuro dei dettagli tecnici. usiamo come sempre la tecnica dell'astrazione con un software nel SO che conosce il device e ci sa comunicare (device driver) e che incapsula le operazioni specifiche del device fornendo solo un'interfaccia (o più di una, per esempio raw interface per file system checker) con cui comunicare. l'astrazione ha i suoi lati negativi: se per esempio un particolare device ha capacità speciali ma deve presentare un'interfaccia generica al resto del kernel quelle capacità andranno perse

## IDE Disk
un disco IDE ha quattro registri:
- control
- command block
- status
- error

il protocollo base per interagire con il device è il seguente
1. aspetta che il device sia pronto (polling)
2. Scrivi i parametri sul registro dei comandi
3. inizia I/O scrivendo read/write sul command register
4. Data transfer: aspetta che lo stato sia `READY` e `DRQ` (richiede dati) e poi scrivi i dati sulla porta dati
5. Nel caso più semplice gestisci l'interrupt per ogni settore trasferito (approcci più complessi permettono di accorpare interrupt, ecc)
6. error handling se il bit `ERROR` è 1

#TODO copia il codice
```c
static int ide_wait_ready() {
	while (((int r = inb(0x1f7))&IDE_BSY) || !(r&IDE_DRDY))
		; //loop until drive isn't busy
}

static void ide_start_request(struct buf *b) {
	ide_wait_ready();
	outb(0x3f6, 0);    //generate interrupt
	outb(0x1f2, 1);    //how many sectors?
	outb()
}
```