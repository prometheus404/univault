# Lock
Un lock non è altro che una variabile che contiene lo stato del lock in un qualsiasi momento nel tempo. Questo può essere sbloccato oppure bloccato e quindi esattamente un [[Thread]] tiene il lock. Nella struttura del lock possiamo aggiungere delle informazioni, come per esempio chi ce l'ha in quel momento o una coda contenente i thread che intendono acquisirlo ma queste informazioni sono nascoste all'utilizzatore del lock. L'utilizzo di un lock è semplice: un thread chiama la routine di `lock()` per tentare di bloccare il lock, se nessun programma lo sta tenendo in quel momento il thread acquisisce il lock e entra nella sezione critica, altrimenti la chiamata non ritorna finchè il lock è in possesso di un altro thread.
Quando il thread che possiede un lock chiama `unlock()` se non ci sono thread in attesa lo stato viene cambiato a free, altrimenti il lock viene passato a uno dei thread in attesa, facendolo ritornare dalla chiamata di `lock()`.
La versione posix dei lock si chiama [[API|pthread_mutex_lock]].
## Valutare i lock
prima di iniziare a costruire dei lock dovremmo capire prima quali sono i nostri obiettivi, vorremmo infatti che un lock:
- permettesse la mutua esclusione
- permettesse a ogni thread di avere accesso alla regione critica (fairness)
- non avesse un grosso impatto sulle performance

## Disabilitare gli interrupt
Una delle prime soluzioni al problema della mutua esclusione era quella di disabilitare gli interrupt in una determinata sezione critica del codice
```c
void lock(){
	DisableInterrupts();
}
void unlock(){
	EnableInterrupts();
}
```
Il principale lato positivo di questo approccio è la sua semplicità, però ha molti lati negativi.
Innanzitutto questo approccio richiede di chiamare operazioni privilegiate fidandoci del programma. Infatti un programma malevolo potrebbe chiamare `lock()` e non chiamare più `unlock()` prendendo così possesso della CPU (i [[context switch]] avvengono grazie a interrupt). In secondo luogo questo tipo di approccio non potrebbe funzionare su un sistema multiprocessore. Inoltre disabilitare gli interrupt può voler dire perdere degli interrupt importanti come per esempio quelli di fine [[IO devices|I/O]], senza i quali non risveglieremmo più un [[processo]] che li stava attendendo. Infine questo approccio non è nemmeno efficiente, infatti il codice che maschera e riabililta gli interupt tende a essere eseguito piú lentamente rispetto alle normali istruzioni dai moderni processori.

## usare solo Load/Store
iniziamo a cercare di costruire un semplice lock usando solo una singola variabile di flag
```c
typedef struct __lock_t {
	int flag;
} lock_t;

void init(lock_t *mutex){
	mutex->flag = 0;
}

void lock(lock_t *mutex){
	while(mutex->flag == 1)
		; 				//spin wait
	mutex->flag = 1;
}
void unlock(lock_t *mutex){
	mutex->flag = 0;
}
 ```
 
il primo thread entra nella sezione critica, chiama `lock()` che controlla se flag è a 1 (in questo caso no), setta flag a 1 e ottiene il lock. finita la sezione critica il thread chiama `unlock()`. Se un altro thread vuole accedere al lock aspetta in spin wait, appena la flag viene cambiata esce dal loop, setta la flag a 1 e entra nella sezione critica. Il codice però ha due problemi: Il primo è che se c'è un interrupt tra il controllo `while(flag==1)` e il momento in cui il thread setta il flag a 1, un altro thread può infilarsi e ottenere il lock. Se a quel punto il primo thread viene eseguito nuovamente questo tornerà dove si era fermato, setterà il flag a 1 e ottiene un lock già in possesso di un altro thread. Inoltre abbiamo un problema di performance perchè lo spin waiting spreca risorse senza fare nulla.
## Spin Lock con Test-And-Set
Come al solito abbiamo bisogno di un po' di aiuto da parte dell'hardware. L'istruzione di cui abbiamo bisogno è un'istruzione `TestAndSet(int *ptr, int new)`. Questa ritorna il valore puntato dal puntatore ``ptr`` e **simultaneamente** esegue un update al valore `new`. Facendo in modo che il controllo e l'aggiornamento siano eseguite atomicamente siamo sicuri che un solo thread ottenga il lock. Per funzionare correttamente richiede che lo [[scheduling|scheduler]] sia preemptive, altrimenti lo spinlock non funziona (se non viene interrotto da un interrupt continua in loop per sempre).
```c 
typedef struct __lock_t {
	int flag;
} lock_t;

void init(lock_t *mutex){
	mutex->flag = 0;
}

void lock(lock_t *mutex){
	while(TestAndSet(&mutex->flag, 1) == 1)
		; 				//spin wait
	mutex->flag = 1;
}
void unlock(lock_t *mutex){
 	mutex->flag = 0;
}
```
La proprietà più importante per un lock è la correttezza e in questo caso è garantita in quanto garantisce la mutua esclusione. Per quanto riguarda la fairness purtroppo questo lock non ci soddisfa pienamente in quanto un thread potrebbe non riuscire mai ad ottenere il lock nel caso sfortunato in cui lo scheduler scheduli sempre un altro processo appena si libera il lock.
Anche per quanto riguarda le performance ci sono brutte notizie. Nel caso di singola CPU lo spinlock spreca tempo prezioso aspettando un cambiamento di un processo che però non è in esecuzione in quel momento. Nel caso di molte cpu questa cosa è molto meno pesante perchè le sezioni critiche sono (si spera) brevi e i due thread possono essere eseguiti in contemporanea (quindi spreca meno di un quanto di tempo)
## Compare-And-Swap
L'idea del compare and swap è quella di testare se il valore puntato da `ptr` è uguale a `expected`, se è così aggiorna il valore in memoria con il nuovo valore, altrimenti non fa nulla.
```c
int CompareAndSwap(int *ptr, int expected, int new);
```
in entrambi i casi ritorna il valore letto in memoria. Il tutto in modo atomico.
Quindi in questo caso la nostra routine di lock diventa:
```c
void lock(lock_t *lock){
	while (CompareAndSwap(&lock->flag, 0, 1) == 1)
		; 		//spin
}
```
Il resto del codice è lo stesso del test and set. Questa istruzione è molto più potente di Test-And-Set e la vedremo per lock-free syncronization.
## Yield
Come abbiamo visto lo spinlock è abbastanza inefficiente. Proviamo con una soluzione semplice: quando il thread sta per fare spinning cede invece il controllo a un altro thread con la primitiva di sistema ``yield()`` che sposta il chiamante dallo stato running allo stato ready.
```c
void init(){
	flag = 0;
}
void lock(){
	while(TestAndSet(&flag, 1) == 1)
		yeld();
}
void unlock() {
	flag = 0;
}
```
Questo non risolve del tutto i problemi di performance però. Immaginiamo un caso in cui ci sono 100 thread che cercano di entrare in una sezione critica. Se un thread ha il lock e viene interrotto prima che possa rilasciarlo, gli altri 99 chiameranno `lock()`, troveranno il lock occupato e lasceranno la CPU a qualcun altro. Assumendo un semplice scheduler round-robin tutti e 99 i tread eseguiranno questo pattern prima che il thread che ha il lock lo rilasci. Questo approccio è sempre meglio che lo spin-wait dove buttiamo un intero slice per thread ma comunque molto costoso. Inoltre non abbiamo risolto il problema della starvation.
## Code e sleep
Introduciamo le primitive `park()`e `unpark(threadID)` per mettere a dormire il thread attuale e risvegliare un determinato thread. Combiniamo la nostra precedente implementazione con TestAndSet con una coda di processi in attesa e aggiungiamo la possibilità di chiamare le due primitive appena riportate.
```c
typedef struct __lock_t {
	int flag;
	int guard;
	queue_t *q;
} lock_t;

void lock_init(lock_t *m){
	m->flag = 0;
	m->guard = 0;
	queue_init(m->q);
}

void lock(lock_t *m){
	while(TestAndSet(&m->guard, 1) == 1)
		; //acquire guard by spinning
	if(m->flag==0){
		m->flag = 1;
		m->guard = 0;
	}else{
		queue_add(m->q, gettid());
		m->guard = 0;
		park();
	}
}

void unlock(lock_t *m){
	while(TestAndSet(&m->guard, 1) == 1)
		;
	if(queue_empty(m->q))
		m->flag = 0;
	else
		unpark(queue_remove(m->q));
	m->guard = 0;
}

```
Questo approccio non evita completamente lo spin-wait, infatti un thread potrebbe essere interrotto mentre sta prendendo o rilasciando il lock e questo fa aspettare in spinwait gli altri lock. Da notare che se avessimo rilasciato la guardia dopo `park()` il thread sarebbe andato a dormire senza che nessuno potesse svegliarlo.
Inoltre il flag non viene settato a 0 quando un altro thread viene svegliato perchè quando un thrad si risveglia alla park() non avrebbe la guardia, quindi rischierebbe di cambiare il valore del flag insieme a un altro thread.
Con un po' di sfortuna però un thread potrebbe essere in procinto di chiamare ``park()`` perchè crede di dover dormire finchè il lock non viene rilasciato, ma avviene un [[context switch]] a un altro thread che ha il lock e lo rilascia. Immaginando che la coda dei thread sia vuota questa condizione porta a un problema, perchè poi il thread non eseguirà altri controlli, andrà a dormire e non si sveglierà più. Solaris risolve aggiungendo una terza [[system call|syscall]] chiamata ``setpark()``, con la quale un thread indica che è intenzionato a chiamare una park, se viene interrotto in quel momento la park che viene chiamata dopo non avrà effetto.
```c
queue_add(m->q, gettid());
setpark();
m->guard = 0;
```
Un'altra possibile soluzione sarebbe passare la guardia nel kernel, il quale potrebbe  occuparsi di rilasciare in modo atomico il lock e togliere dalla coda il thread.