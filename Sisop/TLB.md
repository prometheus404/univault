# TLB
Per evitare il problema delle traduzioni troppo lente introduciamo un buffer nella [[MMU]] chiamato Translation Lookaside Buffer che contiene un sottoinsieme della page table con le traduzioni più popolari, in questo modo se l'entry desiderata si trova in questo buffer possamo eseguire la traduzione in modo veloce senza dover fare l'accesso extra in memoria. Se invece non dovessimo trovare la nostra pagina nel buffer l'hardware va a cercare nella page table per trovare la traduzione corretta e se è valida e accessibile la carica nel TLB. il TLB si basa sui concetti di spatial locality (se accedo a un array in modo sequenziale avrò miss ogni volta che cambio pagina e hit in tutti gli altri casi) e temporal locality (se ho un buffer abbastanza grande e eseguo un programma per due volte di fila alla seconda avrò tutti hit perchè le pagine sono già salvate in memoria).
## Chi gestisce i TLB miss
Potrebbe essere sia l'hardware che il software. Storicamente i processori avevano complesse istruzioni (CISC) che gestivano in completa autonomia i tlb miss. Le architetture più moderne invece hanno istruzioni più ridotte (RISC) e delle TLB gestite dal software. Durante un miss l'hardware genera un'eccezione e quindi lascia il controllo al [[SO]] saltando a un trap handler, il quale userà speciali istruzioni per aggiornare il TLB e farà un return from trap. Questo però è un po' diverso: infatti non deve tornare all'istruzione successiva a quella che ha generato l'eccezione come si fa di solito ma deve rieseguire l'istruzione che ha causato la trap. Inoltre il [[SO|sistema operativo]] deve essere cauto e non causare una catena infinita di TLB miss, in particolare cosa succede se dopo un miss non trovo nel buffer la posizione dell'handler di TLB miss? quindi o lascio questi codici speciali in memoria fisica (senza mapparli in memoria virtuale) oppure riservo alcune entry del TLB per questi casi che potrebbero creare problemi.
Il vantaggio dell'approccio via software è la flessibilità: il [[SO]] può usare qualsiasi struttura dati voglia per le page table inoltre è un approccio più semplice e quindi l'hardware può essere più snello.
## Cosa c'è in un TLB
Un tipico TLB ha un piccolo numero di entry (32-128) e è **fully associative**, Vuol dire che le traduzioni possono essere in qualsiasi posizione e l'hardware cerca in parallelo in tutto il TLB per cercare la traduzione. Una TLB entry è formata in questo modo:

| VPN | PFN | OTHER BITS |
| --- | --- | ---------- |

Negli altri bit solitamente abbiamo 
- un valid bit che indica se l'entry ha una traduzione valida o no (da non confondere con il valid bit della page table dove vuol dire che la pagina non è allocata, qui invece la pagina potrebbe esserci solo che l'indirizzo di traduzione appartiene a un altro [[processo]] o è stato cambiato quindi genero un miss)
- [[address space]] identifier
- dirty bit
- ...

## TLB e [[context switch]]
Le traduzioni in un TLB però sono valide solo per il [[processo]] attuale, in caso di switch devo assicurarmi di usare traduzioni nuove. Una possibile soluzione è quella di flushare il TLB a ogni [[context switch]] settando i valid bit a 0. Però in caso di switch frequenti questo provoca pesanti overhead per via dei miss, quindi alcune implementazioni hardware forniscono un [[address space]] Identifier o **ASID** simile come concetto al PID ma con meno bit. In questo caso il [[SO|Sistema Operativo]] deve settare opportunamente un registro con l'ASID del [[processo]] durante il [[context switch]]
## Replacement Policy
Abbiamo detto che in caso di miss l'hardware o il software devono occuparsi di aggiungere la pagina mancante. Ma se il TLB è già pieno? Un approccio comune è usare un campo last-recent-used per capire quale è stato usato meno di recente (temporal locality). Un'alternativa è a caso (utile perchè abbastanza fair e esclude scenari particolari dove una policy come LRU porterebbe ad avere performance terribili)