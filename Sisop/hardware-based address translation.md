---
aliases: [address translation]
---
#meccanismo 
# Hardware-based address translation
Come dice il nome stesso, con questa tecnica la [[MMU]] trasforma ogni accesso a memoria cambiando il [[virtual adress]] fornito dall'istruzione in un [[physical address]] dove si trova effettivamente l'informazione richiesta. Il [[SO]] deve intervenire in punti chiave per settare l'hardware in modo che contenga l'indirizzo corretto tramite delle istruzioni. Questo permette di ottenere **trasparenza** rispetto al programma utente perchè l'hardware si mette in mezzo tra l'utente e la memoria traducendo in automatico come gli è stato detto dal [[SO|sistema operativo]].