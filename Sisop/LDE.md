---
aliases: [limited direct execution, LDE]
---
#cpu_virtualization
# Limited Direct Execution
L'idea alla base della LDE è semplice: eseguire un [[processo]] direttamente sulla cpu per un po', eseguirne un altro per un altro po', e così via...
facendo [[time sharing]] sulla CPU tramite [[context switch]] si ottiene la [[virtualizzazione]].
Bisogna però capire come mantenere il controllo sulla CPU (senza il quale un [[processo]] potrebbe monopolizzarla) mantenendo però alte le [[prestazioni]] (non vogliamo che i nostri controlli pesino troppo sul sistema).

## Restricted Operations
Una possibile soluzione a questo problema potrebbe essere lasciare che il [[processo]] possa fare tutte le operazioni che vuole, ma questo non ci permette di controllare i permessi prima di garantirgli l'accesso a determinati file.
Perciò dobbiamo introdurre delle modifiche hardware ovvero l'aggiunta di due modalità del processore: la [[kernel e user mode]].
In User mode il programma non può fare richieste di [[IO devices|I/O]], mentre in kernel mode può fare tutto quello che vuole. Per fare in modo che un programma possa fare richieste di I/O deve eseguire una [[system call]] che da il controllo al [[SO|sistema operativo]], il quale controlla i permessi, esegue l'operazione e restituisce il controllo al [[processo]] chiamante.

## Switch tra processi
Se un [[processo]] è in esecuzione su una CPU allora il [[SO]] non è in esecuzione come fa quindi a svolgere il suo lavoro? Semplice, non può. Quindi ha bisogno di trovare un modo per ottenere il controllo della CPU e per farlo si possono usare diversi approcci.

### Approccio cooperativo
Questo approccio (utilizzato dai vecchi MAC) consiste nello sperare che i [[processo|processi]] si comportino in maniera sensata. Infatti la maggior parte dei [[processo|processi]] trasferisce il controllo al [[SO|sistema operativo]] abbastanza di frequente tramite le [[system call]] (in particolare su sistemi come questo la [[system call]] yeld viene usata per cedere il controllo al [[SO]] senza fare nulla) oppure tramite le eccezioni (le quali come [[system call]] generano una [[trap]]). Ovviamente questo approccio non è il massimo in quanto un [[processo]] che va in loop (malignamente o per errore) non cede il controllo al [[SO]] e quindi blocca tutto il sistema

### Approccio con time interrupt
In questo caso viene apportata una modifica hardware: viene aggiunto un timer programmato per generare un interrupt ogni x millisecondi (settato a [[boot time]]), a quel punto il [[processo]] in esecuzione viene interrotto e viene eseguito un interrupt handler preconfigurato dal [[SO|sistema operativo]]. Da notare che l'hardware deve poter salvare in qualche modo lo stato del programma in modo che possa essere correttamente ripreso dalla return from [[trap]]. A questo punto il [[SO|sistema operativo]] deve decidere se continuare l'esecuzione del [[processo]] corrente o passare ad un altro [[processo]], se sceglie questa seconda opzione deve eseguire un pezzo di codice di basso livello: il [[context switch]].