# virtualizzazione cpu
#cpu_virtualization
Il [[SO]] utilizza questa tecnica per creare l'illusione che la CPU sia sempre disponibile per tutti [[processo|processi]].
Per farlo alterna i [[processo|processi]] in esecuzione direttamente sul processore abbastanza vlocemente da dare l'impressione che stiano girando contmporaneamente ([[LDE|limited direct execution]]) .
Per far credere ad ogni [[processo]] di avere a disposizione tutta la CPU il [[SO]] deve far uso di meccanismi a basso livello come il [[context switch]] e policy ad alto livello come quelle di [[scheduling]].