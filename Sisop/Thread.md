# Thread
È una nuova astrazione per un [[processo]] che permette più punti di esecuzione all'interno di uno stesso programma. Un Thread assomiglia molto a un processo separato dagli altri thread, con la differenza che i thread condividono lo stesso [[address space]]. Un'altra dfferenza è che l'address space di un processo multithread ha più di un solo stack, infatti ne ha uno per thread. Come con i processi i thread vengono schedulati e devono avere una struttura dati simile a quella del PCB per tenerne traccia. Nel caso dei thread si chiama Thread Control Block.
## Vantaggi
I thread permettono di parallelizzare i compiti, per esempio con due thread su due processori si può eseguire una [[ricerca binaria]] in metà del tempo necessario. Inoltre nel caso un processo faccia molte richieste di I/O, risulterebbe molto lento perchè a ogni richiesta andrebbe a dormire in attesa della risposta. Con i thread posso eseguire altre operazioni **all'interno dello stesso processo** senza dover aspettare gli I/O (come il multiprogramming fa con i processi).
## Condivisione dei dati
Immaginiamo un semplice esempio dove due thread cercano di incrementare una variabile globale in comune. Usiamo dei wrapper, che hanno lo stesso nome ma con la prima lettera maiuscola, che si occupano di controllare che la chiamata sia andata a buon fine e nel caso contrario terminano il programma.
Invece di usare due corpi di funzione diversi per le funzioni dei thread ne usiamo una sola a cui passiamo due stringhe diverse. Scriviamo un programma che crea due thread e incrementa un contatore comune 10.000.000 volte per thread.
``` c
#include <stdio.h>
#include <pthread.h>
#include "mythreads.h" 	//wrapper

void *mythread(void *arg){
	printf("%s: begin\n", (char *) arg);
	int i;
	for (i = 0; i < 1e7; i++){
		counter = counter + 1;
	}
	printf("%s: done\n", (char *) arg);
	return NULL;
}

int main(int argc, char *argv[]){
	pthread_p p1, p2;
	printf("main: begin (counter = %d)\n", counter);
	Pthread_create(&p1, NULL, mythread, "A");
	Pthread_create(&p2, NULL, mythread, "B");
	Pthread_join(p1, NULL);
	Pthread_join(p2, NULL);
	printf("main: done with both (counter = %d)\n", counter);
	return 0;
}
```
A questo punto eseguendolo ci aspetteremmo di ottenre 20.000.000 eppure esguendolo varie volte otteniamo risultati diversi e quasi mai quello giusto.
Questo perchè l'istruzione `counter = counter + 1` in realtà è data da un insieme di tre istruzioni diverse in assembly: una per caricare il valore attuale del contatore in un registro, una per sommarci 1, una per caricare il valore aggiornato in memoria. Immaginiamo però che il thread T1 esegua le prime due istruzioni, caricando il valore attuale (50) e incrementandolo. Ora il registro di T1 ha 51 come valore. Prima che riesca a scriverlo però viene interrotto dallo [[scheduling|scheduler]] che esegue T2, il quale carica in memoria il valore del contatore (ancora 50), lo incrementa e lo carica in memoria. A questo punto lo scheduler lo interrompe ed esegue T1 di nuovo. T1 doveva eseguire la terza istruzione, quindi carica in memoria il suo valore del registro. Alla fine delle operazioni il contatore vale 51, nonostante sia stato incrementato due volte. Questa situazione è chiamata **race condition** e la porzione di codice che può causarla viene chiamata **sezione critica**. Vorremmo garantire che solo un thread alla volta possa accedere a questa sezione critica (mutua esclusione).
Per risolvere vorremmo che l'istruzione venga eseguita atomicamente, solo che non possiamo avere un'istruzione hardware per ogni oprazione che vogliamo fare (non ha senso avere un'istruzione che in automatico aggiorna un albero). Quindi l'hardware fornisce delle istruzioni atomiche base che possiamo usare per costruire un set di [[primitive di sincronizzazione]].