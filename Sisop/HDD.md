---
aliases: [hard disk, disk, hard drive, disco]
---
# Hard disk drives
## Interfaccia
L'interfaccia per i moderni disk drive consiste in un gran numero di settori (blocchi da 512byte), ognuno dei quali più essere letto o scritto. I settori sono numerati da 0 a n-1 su un disco con n settori, qundi possiamo vedere un disco come un array di settori (da 0 a n-1 è il suo [[address space]]). L'unica garanzia che abbiamo è che una singola scrittura in un singolo settore è atomica. C'è anche la regola non scritta che si possa assumere che accedere a due blocchi vicini sarà più veloce che accedere a due blocchi lontani.
## Struttura
Il disco è formato da una superficie circolare rigida chiamata **Piatto** sulla quale vengono scrtti i dati. Queste sono unite insieme una sull'altra attorno allo **spindle**, che è connesso a un motore e permette al disco di girare. La velocità di rotazione è spesso misurata in RPM. I dati sono scritti su ogni faccia del platter in cerchi concentrici chiamati tracce. Il processo di lettura e scrittura è fatto dalle testine, le quali sono attaccate tramite un braccio e leggermente sollevate dal disco. C'è una testina per faccia quindi due per platter.
![[simple_disk.png]]
## Rotational delay
prendiamo in esempio un semplice disco contenente una sola traccia. Per lettura e scrittura il disco dovrà attendere che il settore richiesto sia sotto la testina. Questo tempo di attesa si chiama **rotational delay** e nel nostro esempio il tempo medio di rotazione è $\frac{R}{2}$ dove R è il tempo di un giro completo.
## Seek time
![[hdd.png]]
Aggiungiamo ora due tracce al disco. Per poter leggere uno dei blocchi su questi dischi dobbiamo spostare il braccio in modo che la testina si trovi sulla traccia corretta. Il tempo per eseguire questo movimento è chiamato **seek time**. Da notare che il processo di seek è composto da varie fasi:
- accelerazione: fase in cui si mette in moto il braccio
- coasting: fase in cui raggiunge la sua velocità che manterrà fino a quando sarà sulla posizione corretta
- decelerazione: rallentamento per arrivare sulla traccia giusta
- settling: tempo per trovare la traccia corretta e assicurarsi che sia posizionata nel modo giusto. Questo tempo è abbastanza significativo

Importante notare che durante il seek time il disco non smette di ruotare, quindi ci siamo spostati sia da una traccia a un'altra sia di qualche blocco nella rotazione.
Per questo motivo a volte il numero dei settori è posizionato in modo sfalsato (altrimenti cambiando traccia nelle letture sequenziali nel tempo di seek time supereremmo il blocco desiderato e dovremmo aspettare una rotazione intera per ritrovarlo).
![[skew.png]]

## Cache
Una parte importante dei moderni dischi è la cache o track buffer che contiene i dati letti o da scrivere su disco .
Durante le scritture il disco ha una scelta: deve riportare subito che la scrittura è completa appena ha messo i dati nella memoria o dopo averli riportati su disco? la prima è chiamata write back (sembra più veloce ma è pericolosa per il journaling) e la seconda write through.
## I/O time
$$ T_{I/O} = T_{seek} + T_{rotation} + T_{Transfer}$$
a partire da questa possiamo semplicemente calcolare il rate di I/O
$$R_{I/O} = \frac{Size_{Transfer}}{T_{I/O}}$$

## Disk scheduling
A causa dell'elevato costo di [[IO devices|I/O]] il SO ha sempre avuto un ruolo fondamentale nel decidere in che ordine schedulare gli I/O su disco. A differenza dello [[scheduling]] dei processi possiamo avere una buona stima di quanto un _job_ impiegherà ad essere eseguito, quindi lo scheduler cercherà di seguire SJF.
### Shortest Seek Time first
schedula come prossimo I/O la richiesta più vicina a quella appena fatta (in modo che si riduca seektime). Ha due problemi: 
- il SO non sa come effettivamente è implementato all'interno un disco e quindi i blocchi potrebbero non essere effettivamente vicini o potrebbero essere troppo vicini e non sfalsati.
- Potrebbe esserci starvation

### Elevator
questo algoritmo (originariamente chiamato SCAN) muove semplicemente avanti e indietro sul disco servendo le richieste in ordne lungo le tracce. Se arriva una richiesta per una traccia che è già stata servita in questo sweep del disco non viene servita subito ma messa in coda fino al prossimo sweep.
Un'alternativa comune è C-SCAN (C sta per circular) che invece di fare sweep in entrambe le direzioni lo fa solo dall'interno all'esterno e poi resetta la posizione della testina. Questo approccio è più fair perchè non avvantaggia le tracce in centro al disco e funziona sul presupposto che muovere la testina nella posizione iniziale sia molto veloce. 
Sfortunatamente queste policy hanno un problema: ignorano la rotazione.
### SPT
Shortest positioning first calcola anche il rotation time a differenza di SSTF ma è ancora più complesso da impelmentare internamente a un SO, quindi viene solitamente implementato all'interno dei dischi.
### Altri problemi
Come già anticipato nei sistemi moderni lo scheduling viene fatto all'interno dei dischi e il compito del SO è scegliere un tot di richieste tra quelle fatte (quelle che crede siano più vicine) e le manda al disco che fa il resto nel modo migliore possibile.
Inoltre un disco puó decidre di unire due richieste in una sola in modo da renderele più veloci (xes se richiedo blocco 1 e 2 faccio una sola richiesta da due blocchi a partire dal blocco 1).
Un'altra domanda è quanto aspettare prima di fare richieste al disco e la risposta è dipende ma in molti casi convine almeno aspettare un po' per permettere di scegliere le richieste migliori.
