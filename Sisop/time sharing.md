# Time sharing
#cpu_virtualization
È una tecnica base usata dal [[SO|Sistema Operativo]] per condividere una risorsa, permettendo a ogni utente di accedere a quella risorsa per un periodo limitato di tempo e passando al prossimo utente alla fine del [[quanto]]. La sua controparte è lo [[space sharing]] dove invece una risorsa è suddivisa contemporaneamente dando un pezzetto ad ogni utente.