---
aliases: [api, process api, create, fork, exec, wait, destroy]
---
# API
## Process API
![[posix_syscall.png]]
### Create
Chiamandola permette di creare un nuovo [[processo]].
In unix non esiste una vera e propria create ma si ottiene lo stesso risultato (e molto altro) con due [[system call]] separate
#### Fork
Questa [[system call]] viene usata per creare nuovi [[processo|processi]] ma con un plot twist. Il [[processo]] creato è una copia (quasi) estatta del [[processo]] padre che quindi riprende l'esecuzione dallo stesso punto in cui era stata chiamata la ``fork()`` e non dal ``main()``. L'unica cosa che distingue padre e figlio è (oltre il [[PID]]) il valore ritornato dalla ``fork()``. Nel caso del padre questo è infatti il [[PID]] del figlio, nel caso del figlio è 0, mentre nel caso di errore è un numero < 0. Questo valore di ritorno viene utilizzato nel codice per dividere l'esecuzione in due rami diversi a seconda del fatto che uno dei due sia il padre o il figlio.
#### Exec
Viene utilizzata per eseguire un programma che è diverso dal programma chiamante: 
serve infatti per 
- caricare il codice (e i dati statici) del programma eseguibile sovrascrivendoli a quelli del programma in esecuzione
- ri-inizializzare lo [[stack]], lo [[heap]] e altre parti della memoria
- far eseguire al [[SO]] il programma passando gli argomenti come _argv_ del [[processo]]. **Una call a ``exec()`` se ha successo non ritorna mai**

Ma perchè tutto questo? non bastava avere una semplice create che facesse il [[processo]] in automatico? Questa particolare implementazione è fondamentale per il funzionamento della shell unix perchè permette di eseguire del codice **dopo** aver chiamato la ``fork()`` ma **prima** di chiamare ``exec()`` permettendo quindi di modificare l'environment  permettendo per esempio di ridirezionare lo standard output
### Destroy
Allo stesso modo deve essere presente un modo per distruggere un [[processo]] nel caso esso non riesca a terminare nel modo corretto.
### Wait
Con questa API il [[processo]] che la chiama attende che il [[processo]] specificato termini la propria esecuzione
### Controlli vari
il [[SO| Sistema operativo]] fornisce dei metodi per sospendere e riprendere il [[processo]]
### Stato
interfacce per vedere lo stato di un [[processo]] come per esempio da quanto è in esecuzione e se è attualmente in esecuzione, sospeso, ecc...
### Pipe
Le pipe sono implementate come code sulle quali un [[processo]] scrive e un altro legge

---

## Memory API
### Malloc
La chiamata a malloc funziona passando una quantità di memoria che vorresti allocare e il valore ritornato sarà un puntatore alla nuova area di memoria allocata o ```NULL``` nel caso la chiamata fallisca.
Un uso tipico avviene in tandem all'uso di ``sizeof()`` perchè non tutti i tipi hanno la stessa implementazione su tutte le macchine.
```c
double *d = (double *) malloc(sizeof(double))
```
Da notare che la memoria viene allocata durante l'esecuzione, mentre ``sizeof()`` viene valutato durante la compilazione.
#### Errori comuni
##### Dimenticarsi di allocare memoria
```c
char *src = "hello";
char *dst; 				//non allocata
//char *dst = (char *) malloc(strlen(src)+1) //soluzione al problema
strcpy(dst,src); 		//quando prova a copiarci dentro hello va in segfault
```

##### Allocare poca memoria
```c
char *src = "hello";
char *dst = (char *) malloc(strlen(src)) //troppo breve (dovrebbe essere strlen(src) + 1)
strcpy(dst,src);
```
A seconda  dell'implementazione della ``malloc()`` e del caso questa chiamata potrebbe non dare problemi (potrebbe per esempio sovrascrivere una variabile inutilizzata). Se invece sovrascrivesse qualcosa di importante può portare a conseguenze gravi [[buffer overflow attack]]
##### Dimenticarsi di inizializzare la memoria allocata
Chiamando la ``malloc()`` la memoria restituita ha al suo interno valori casuali (dipendenti dal vecchio contenuto) quindi leggere prima di inizializzare può portare a comportamenti randomici del programma.
### Free
La chiamata a ``malloc()`` dovrebbe sempre essere seguita da una chiamata a ``free()``, quest'ultima serve infatti a liberare lo spazio allocato durante una ``malloc()``
#### Errori Comuni
##### Dimenticarsi di liberare la memoria
Anche chiamato **memory leak**. Se l'applicazione termina in fretta non crea problemi, ma se il [[processo]] deve essere eseguito per tanto tempo (per esempio il [[SO|sistema operativo]]) errori come questo possono portare a sprecare buona parte della memoria.
##### Liberare la memoria prima di aver smesso di utilizzarla
Questo porta ad avere **dangling poiter** ovvero puntatori che puntano ad aree di memoria non corrette e possono portare a segmentation fault.
##### Liberare più volte la memoria
Il risultato di una **double free** è indefinito e può far avvenire molte cose strane.
##### Chiamare free in modo sbagliato
Chamare la free su valori strani (invece che puntatori ad aree di memoria) può far succedere cose imprevedibili e brutte.
### Calloc
Alloca memoria come la malloc e la riempe di zeri.
### Realloc
Cambia la dimensione di un'area di memoria allocata.

---
## Thread API
### Thrad Creation
```c
#include <pthread.h>
int pthread_create(pthread_t * [[Thread]], 				// puntatore alla struttura del thrad
 				   const pthread_attr_t * attr, 	// attributi del thread (stack size, scheuling priority,...)
				   void * (*start_routine)(void*), 	// la funzione che il thread deve eseguire
				   void* arg); 						// argomenti da passare alla funzione eseguita dal thread
```

### Thread completion
La funzione ``pthread_join()`` può essere utilizzata per aspettare che un thread finisca l'esecuzione.
```c
int pthread_join(pthread_t thread, void **value_ptr);
```
Questa funzione richiede due argomenti: la struttura dati del thread da aspettare e un puntatore a una zona di memoria in cui salvare il valore di ritorno del thread.
```c
#include <pthread.h>
typedef struct __myarg_t{
	int a;
	int b;
} myarg_t;

typedef struct __myret_t{
	int x;
	int y;
}

void *mythread(void* arg){
	myarg_t *m - (myarg_t *) arg;
	printf("%d %d\n", m->a, m->b);
	myret_t *r = malloc(sizeof(myret_t));
	r->x = 1;
	r->y = 2;
	return (void *) r;
}

int main(int argc, char *argv[]){
	pthreat_t p;
	myret_t *m;
	
	myarg_t args;
	args.a = 10;
	args.b = 20;
	pthread_create(&p, NULL, mythead, &args);
	pthread_join(p, (void **) &m);
	printf("returned %d %d\n", m->x, m->y);
	free(m);
	return 0;
}
```
Bisogna prestare particolare attenzione a come i valori vengono restituiti da un thread. In particolare mai restituire un puntatore che punta a qualcosa allocato nello stack.
```c
void *mythread(void* arg){
	myarg_t *m - (myarg_t *) arg;
	printf("%d %d\n", m->a, m->b);
	myret_t r; 				//allocato sullo stack
	r.x = 1;
	r.y = 2;
	return (void *) &r;
}
```
- quando termina il valore sullo stack viene automaticamente deallocato, di conseguenza il valore restituito punta a una zona di memoria deallocata, con tutti i possibili errori che questo può portare.
### [[Lock]]
Permettono la mutua esclusione da zone di codice critiche.
```c
int pthread_mutex_lock(pthread_mutex_t *mutex);
int pthread_mutex_unlock(pthread_mutex_t *mutex);
```
Queste due routine vengono usate in coppia per circondare la sezione critica
```c
pthread_mutex_t lock;
pthread_mutex_lock(&lock);
x = x + 1; 				//sezione critica
pthread_mutex_unlock(&lock);
```
Se nessun altro thread ha il lock quando ``pthread_mutex_lock()`` viene chiamata, il thread acquisisce il lock. Se un altro thread prova a ottenere il lock non ritorna dalla chiamata finchè non l'ha preso (quindi un altro thread deve prima rilasciarlo).
Ci sonno due modi per inizializzare i lock.
```c
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
//oppure
int rc = pthread_mutex_init(&lock, NULL);
assert(rc == 0);
```
Oltre a inizializzare, per ogni lock che non si usa più si dovrebbe chiamare ``pthread_mutex_destroy()``
Altre due funzioni interessanti sono:
```c
int pthread_mutex_trylock(pthreat_mutex_t *mutex);
int pthread_mutex_timedlock(pthread_mtex_t *mutex, struct timespec *abs_timeout)
```
Entrambe permettono di non rimanere bloccati se non si resce a ottenere il lock (utile in alcuni casi per evitare deadlock). Nel primo caso ritorna un errore se non riesce a ottenere il lock, nel secondo caso la funzione ritorna o dopo aver ottenuto il lock o con un errore dopo un timeout.
### [[Condition Variables]]
Utili quando deve avvenire un qualche tipo di segnalazione tra thread.
```c
int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex);
int pthread cond_signal(pthread_cond_t *cond);
```
La prima mette a dormire il thread chiamante se la condizione non è verificata in attesa che un altro thread lo svegli.
```c
pthread_mutex_t lock = PTHREAD_MUTEX_TINITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_lock(&lock);
while (ready == 0)
	pthread_cond_wait(&cond, &lock);
pthread_mutex_unllock(&lock);
```
il codice per svegliare un thread invece è:
```c
pthread_mutex_lock(&lock);
ready = 1;
pthread_cond_signal(&cond);
pthread_mutex_unlock(&lock);
```
Da notare che ci assicuriamo sempre di ottenere il lock. Inoltre la chiamata che mette a dormire un thread ha come arcomento una condition e un lock, mentre quella per svegliare solo una condition. Questo perchè prima di andare a dormire un thread deve rilasciare il lock (altrimenti causerebbe deadlock) e riottenerlo prima di svegliarsi. Da notare anche che usiamo un while loop per controllare la variabile ready perchè è più sicuro nei casi in cui il thread viene svegliato per errore.

---
## File API


### Creare file
Per creare file viene usata la call `open()` passando il flag `O_CREAT`.
```c
int fd = open("foo", O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|IWUSR)
```
in questo esempio crea il file se non esiste (`O_CREAT`), si assicura che possa solo essere scritto (`O_WRONLY`) e se il file esiste già lo tronca a una dimensione di 0 byte (`O_TRUNC`). Il terzo parametro invece serve per i permessi in questo caso rende il file leggibile e scrivibile dal proprietario.
`open()` restituisce un file descriptor, ovvero un intero unico per ogni processo che viene usato per i sistemi unix per accedere ai file. I file descriptor sono gestiti dal SO nella structure `proc`. Può essere implementato con un semplice array in cui ogni entry punta a un a struct file che contiene tutte le informazioni necessarie.
### Leggere file
Prima di leggere un file dobbiamo aprirlo con `open()` con `O_RDONLY`, il quale restituisce un file descriptor ( partirà da 3 perchè ogni processo ha di default aperti  i file standard input, standard output e standard error).
Per leggere usiamo la system call `read()` che prende come argomenti il file descrptor, il buffer in cui mettere il risultato della read e infine la lunghezza del buffer. `read()` restituisce il numero di byte letti.
Per leggere un file non in sequenza dobbiamo prima chiamare `lseek()`
```c
off_t lseek(int fildes, off_t offset, int whence);
```
Il primo argomento è ovviamente un file descriptor, il secondo è un offset che posiziona il **file offset** in un particolare posto all'interno del file, il terzo argomento specifica come questa seek deve essere fatta:
- `SEEK_SET`: l'offset viene settato a `offset` byte
- `SEEC_CUR`: l'offret viene settato alla posizione corrente più `offset` byte
- `SEEK_END`: l'offset viene impostato alla dimensione del file più `offset` byte

Ogni read e write aumenta l'offset(gestito dal SO e messo nella struct file) in modo implicito.

### Scrivere file
Per scrivere usiamo invece la chiamata `write()`
### File condivisi
In alcuni casi le entry del file descriptor sono mappate uno a uno con le entry della open file table. Infatti se due processi aprono lo stesso file entrambi avranno la loro personale file descriptor table che punta a due entry diverse sulla open file table. In questo modo ogni lettura da file è indipendente e ognuno ha il suo current offset. Ci sono alcuni casi in cui una entry della open file table è condivisa: capita quando un genitore crea un figlio con una `fork()` in questo modo padre e figlio condividono la stessa entry nella open file table e quindi lo stesso offset.
una entry della open file table avrà quindi un campo reference count che indica quanti processi stanno condividendo quel file, in modo che la entry possa essere eliminata solo quando entrambi i processi chiudono il file o terminano. È utile per fare in modo che più processi possano collaborare a un file senza coordinamenti extra. Un altro caso di sharing avviene con la chiamata `dup()` la quale crea un altro file descriptor che si riferisce a una stessa open file table.
### Scrittura immediata
Solitamente quando un programma chiama una write() il SO non scrive subito quei dati ma li mette in un buffer per un po' di tempo. Alcune applicazioni richiedono una maggiore velocità in scrittura su disco, per questo esiste `fsync()` che forza il SO a copiare tutti i dati dirty su disco. Da notare che chiamare la fsync sul file appena creato potrebbe non bastare perchè va fatto anche sulla cartella che lo contiene (altrimenti potrebbe esserci il caso di un file che si trova nel filesystem ma in giro a caso e non nella cartella)
### Rinominare file
`rename(char *old, char *new)`
Questa chiamata rinomina in modo atomico un file. Per questo motivo viene spesso sfruttata per quei file che vogliono mantenere l'atomicità degli aggiornamenti:
- inizialmente creo un secondo file che è una copia di quello che voglio modificare
- modifico quello
- rinomino il secondo file con il nome del primo

In questo modo non ho vie di mezzo: o il file viene aggiornato o no.
### Informazioni sui file
Il file system tiene traccia di una grande quantità di informazioni sul file chiamati metadati (solitamente tenuti nell'inode). per leggerli possiamo usare le system call `stat()` o `fstat()`.
### Rimuovere file
Per rimuovere file usiamo la syscall `unlnk()` che richiede come argomento il nome del file da rimuovere.
### Creare directories
Da notare che non si può mai creare una directory piena direttamente perchè il contenuto delle directory è considerato metadata del filesystem, quindi si possono solo creare directory vuote e successivamente crare oggetti a loro interno. Una directory vuota ha due entry: `.` e `..` una che si riferisce a se stessa mentre l'altra che si riferisce al genitore.
### Leggere directory
Per leggere il contenuto di una directory dobbiamo
1. aprire la directory con `opendir()`
2. leggere il contenuto con `readdir()`
3. chiudere il tutto con `closedir()`

Ogni directory entry è una struct dirent (una linked list)
```c
struct dirent{
	char 				d_name[256]; //filename
	in_t				d_ino; 		 //inode number
	off_t 				d_off;		 //offset to the next dirent
	unsigned short 		d_reclen; 	 //lunghezza del record
	unsigned char 		d_type;		 //tipo del file
}
```
per informazioni maggiori si usa `stat()` su ogni file della drectory.
### Rimuovere directory
`rmdir()` ma solo se è vuota
### Hard link
la chiamata `link()` prende due argomenti: un vcchio pathname e uno nuovo e crea un nuovo modo per riferirsi allo stesso file. Semplicemente crea un altro nome in una directory che fa riferimento allo stesso inode number. Per questo motivo la chiamata per rimuovere file si chiama `unlink()`, la quale rimuove il link scelto e diminuisce il reference count. Quando questo raggiunge 0 il filesystem libera lo spazio usato per quel file.
Siccome l'inode number è unico per un determinato filesystem non si possono creare hard link su partizioni diverse. Non si può nemmeno creare hard link a directory perchè questo potrebbe creare cicli nell'albero
### Symbolic link
Per superare le limitazioni dell'hard link nasce il symbolic link. Il symbolic link è un file a sè stante che contiene nei dati il pathname del file a cui fa riferimento. Per questo motivo però possono portare a **dangling reference** nel caso in cui il file a cui si collega venga rimosso.
### Permessi
Il file system presenta una vista virtuale del disco che è diversa dall'astrazione della CPU e della memoria, in quanto i file sono condivisi tra processi. Quindi è necessario avere dei meccanismi di protezione. Un metodo sono i permission bit di unix che determinano cosa può fare il proprietario, cosa può fare il gruppo e cosa può fare chiunque altro.
sono tre bit read, write e execute. Quest'ultimo indica il permesso di eseguire i file normali mentre per le directory indica se ci si può muovere al suo interno. Alcuni file system usano strutture più complesse come le access control list (ACL)
### Creare e montare un file system
per creare un filesystem si usa `mkfs` mentre per potervi accedere bisogna montarlo in qualche punto del file system tree. Questo viene fatto tramite  `mount` il quale monta il filesystem (che risulterà come una directory).

