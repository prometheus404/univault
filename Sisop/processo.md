---
aliases: [processo, processi]
---
#astrazione #cpu_virtualization
# Processo
È una delle principali [[astrazioni]] che il [[SO|sistema operativo]] mette a dsposizione dell'utente. Se un programma è semplicemente del codice, il processo è un programma in esecuzione. Su una stessa CPU possono essere in esecuzione vari programmi contemporaneamente tramite la [[virtualizzazione cpu]].

Per capire meglio cos'è un processo dobbiamo capire il suo **machine state**.

## machine state
Lo stato di un processo durante l'esecuzione è composto da:
- la sua **memoria**
- i suoi **registri**
- informazioni su **[[IO devices|I/O]]**

Il [[SO|sistema operativo]] deve inoltre fornire all'utente delle interfacce per interagre con i processi ovvero le [[API]].

## creazione dei processi
La prima cosa che deve fare il [[SO|sistema operativo]] per eseguire un processo è caricare il suo codice e tutti i dati statici dal [[HDD|disco rigido]] alla memoria. Nei primi [[SO|sistemi operativi]] questo veniva fatto in una volta sola all' apertura dei programmi, mentre quelli moderni lo fanno in modo lazy caricando solo i pezzi necessari in quel momento.
Dopodichè deve allocare della memoria per lo [[stack]] del processo e lo inizializzerà con delle variabili (per esempio _argc_ e _argv_ della funzione main). Il [[SO|sistema operativo]] potrebbe inoltre dover allocare anche lo [[heap]] del programma.
Deve anche aprire i [[file descriptor]] per standard input, output and error e infine eseguire il programma facendo un jump all'entry point (`main()`) trasferendo in questo modo il controllo al processo appena creato.

## Stato del processo
Un processo (semplificando molto) può trovarsi in uno di questi stati:
- **RUNNING:** in questo stato il processo è in esecuzione su un processore e quindi sta eseguendo delle istruzioni
- **READY:** è pronto per essere eseguito ma per qualche motivo il [[SO|sistema operativo]] ha deciso di non eseguirlo in questo momento
- **BLOCKED:** il processo ha eseguito una qualche operazione che lo terrà in attesa di qualcosa (per esempio una richiesta di [[IO devices|I/O]])
- **INITIAL STATE:** stato in cui viene creato il processo.
- **FINAL/ZOMBIE STATE:** stato in cui si trova il processo dopo aver terminato il suo ciclo vitale. È utile per permettere al processo genitore di leggere eventuali return code (0 se va tutto bene, negativo se va male qualcosa)

Il [[SO|sistema operativo]] può [[scheduling|schedulare]] o de-schedulare un processo spostandolo quindi tra running e ready.
Per tenere traccia dello stato di un processo il [[SO|Sistema Operativo]] deve tenere apposite [[strutture dati]], come per esempio una lista dei processi per sapere quali processi sono ready e delle informazioni aggiuntive per sapere quali processi sono invece già in esecuzione, quali sono bloccati, il contenuto dei registri di un processo per eseguire [[context switch]], ecc... tutte queste informazioni vengono scritte nel [[PCB|process control block]]
###### correlati