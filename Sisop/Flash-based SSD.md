# Flsh-based SSD
Sono dispositivi senza parti in movimento e basati su transistor (come RAM) ma permettono di mantenere le informazioni anche senza corrente (come gli [[HDD]]).
I chip flash sono pensati per contenere uno o più bit su un singolo transistor.
- single-level cell(SLC): un solo bit per transistor
- multi-level cell (MLC): due bit per transistor
- triple-level cell (TLC): tre bit per cella

meno bit per cella più alto il costo per lo spazio ma migliori le performance.
Gli SSD sono organizzati in banks o planes formati da nun gran numero di celle. Un bank può essere acceduto in due diverse dimensioni: i block (tipicamente 128kb o 256kb) e le pagine (tipicamente 4kb). Per scrivere una pagina in un blocco bisogna prima cancellare l'intero blocco.
## Operazione base
Ci sono tre operazioni a basso livello che un chip flash supporta:
- Read: Un client del chip può leggere una qualsiasi pagina semplicemente specificando il numero della pagina Questa operazione è veloce (10micros) indipendentemente dalla posizione del dato. Quindi possiamo dire che è un device ad accesso random.
- Erase: Prima di scrivere una pagina bisogna cancellare l'intero blocco. Questo comando è abbastanza dispendioso (qualche ms)
- Program Può essere usata per cambiare alcuni degli 1 all'interno di una pagina con degli 0. è meno dispendioso di un Erase ma più di una read (100micros)

| page 0   | page1    | page 2   | page 3   |
| -------- | -------- | -------- | -------- |
| 00011000 | 11001110 | 00000001 | 00111111 |
| VALID    | VALID    | VALID    | VALID    |

Immaginiamo di avere queste 4 pagine all'interno di un blocco da 4 pagine. Ognuna di queste è valid perchè è stata programmata. Se vogliamo scrivere 00000011 sulla pagina 0 dobbiamo prima cancellare il blocco.

| page 0   | page1    | page 2   | page 3   |
| -------- | -------- | -------- | -------- |
| 11111111 | 11111111 | 11111111 | 11111111 |
| INVALID  | INVALID  | INVALID  | INVALID  | 

A questo punto possiamo scrivere nella pagina 0

| page 0   | page1    | page 2   | page 3   |
| -------- | -------- | -------- | -------- |
| 00000011 | 11111111 | 11111111 | 11111111 |
| VALID    | INVALID  | INVALID  | INVALID  |

Peccato che in questo modo abbiamo perso tutto il contenuto delle pagine 1,2,3

## Performance e Affidabilità
A differenza dei dischi meccanici che possono rompersi per una varietà di fattori gli SSD hanno principalmente un solo problema: il consumo.
Quando un blocco viene cancellato e riprogrammato accumula lentamente della carica extra che con il tempo rende indistinguibile un 1 da uno 0.
Un altro problema minore è il disturbo: quando si cerca di accedere a una particolare pagina facendo un flash è possibile che alcuni bit finiscano in una page vicina per sbaglio ^558d33

## Da memorie flash a SSD
un SSD è costituito da un certo numero di memorie flash, della memoria non persistente per fare caching e un FTL (flash translation layer) che si occupa di tradurre le richieste su blocchi logici in comandi base su blocchi e pagine fisiche. Questo deve cercare di ridurre la write amplification ($\frac{\text{byte inviati ai flash chip}}{\text{byte inviati come logical block}}$) e distribuire equamente il consumo sui blocchi. Per quanto riguarda il disturbo si risolve programmando le pagine dalla più bassa alla più alta.

## Primo tentativo di FTL
Il modo più semplice per organizzare un FTL sarebbe una cosa che viene chiamata direct mapped. una lettura sulla pagina logica N viene mappata sulla pagina fisica N. Una scrittura sulla pagina logica N è più complicata perchè dobbiamo leggere l'intero blocco, cancellare il blocco e programmare le vecchie pagine e quella nuova. 
In questo modo però abbiamo delle performance terribili in fase di scrittura, addirittura peggio di un hard disk. Inoltre se i file con i metadata di sistema vengono sovrascritti spesso questo causa un consumo di un blocco molto più intenso degli altri, quindi aumenta il rischio di rottura.

## Log-Structured FTL
Per questo motivo la maggior parte degli SSD ai giorni nostri sono strutturati a log. Durante una scrittura sul blocco logico N il dispositivo scrive sul primo spazio vuoto. Per fare ciò dobbiamo avere una tabella che mappa l'indirizzo logico in un indirizzo fisico. Il tutto in modo trasparente : all'utilizzatore sembra un normale disco in cui fare read e write su settori da 512byte.
Immaginiamo che il nostro SSD contenga un gran numero di blocchi da 16kb suddivisi in pagine da 4kb e che il client richieda le seguenti operazioni:
- write(100) con contenuto a1
- write(101) con contenuto a2
- write(2000) con contenuto b1
- write(2001) con contenuto b2
Immaginiamo che la memoria sia tutta al momento non valida. Per prima cosa cancelliamo il primo blocco e otteniamo


| Page:   | 00  | 01  | 02  | 03  | 04  | 05  | 06  | 07  | 08  | 09  | 10  | 11  |
| ------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| content | -   | -   | -   | -   | -   | -   | -   | -   | -   | -   | -   | -   |
| State   | E   | E   | E   | E   | i   | i   | i   | i   | i   | i   | i   | i   |
| Blocco: | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   | 

A questo punto possiamo programmare il blocco 0

| Page:   | 00  | 01  | 02  | 03  | 04  | 05  | 06  | 07  | 08  | 09  | 10  | 11  |
| ------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| content | a1  | -   | -   | -   | -   | -   | -   | -   | -   | -   | -   | -   |
| State   | V   | E   | E   | E   | i   | i   | i   | i   | i   | i   | i   | i   |
| Blocco: | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   |

nella tabella delle traduzioni avremo:

| Virtual | Phisical |
| ------- | -------- |
| 100     | -> 0     | 

alla fine dei comandi avremo

| Page:   | 00  | 01  | 02  | 03  | 04  | 05  | 06  | 07  | 08  | 09  | 10  | 11  |
| ------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| content | a1  | a2  | b1  | b2  | -   | -   | -   | -   | -   | -   | -   | -   |
| State   | V   | V   | V   | V   | i   | i   | i   | i   | i   | i   | i   | i   |
| Blocco: | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   |

nella tabella delle traduzioni avremo:

| Virtual | Phisical |
| ------- | -------- |
| 100     | 0        |
| 101     | 1        |
| 2000    | 2        |
| 2001    | 3        | 

In questo modo dobbiamo cancellare i blocchi solo una volta ogni tanto e abbiamo allungato la vita al dispositivo.
Sfortunatamente questo approccio ha dei lati negativi:
Il primo è che sovrascrivere un blocco porta alla formazione di dati spazzatura. Il device deve fare ogni tanto della garbage collection per trovare blocchi spazzatura e liberarli per le future scritture.
Il secondo sono le mapping tables che possono essere estreamamente grosse: su un SSD da un terabite ci vorrebbero 1GB di memoria per queste mappe.

## Garbage collector
#TODO 

## Block based Mapping
Un approccio per ridurre il costo del mapping è quello di tenere un puntatore al blocco del device invece che alla pagina. Sfortunatamente però usare questo tipo di mapping non funziona molto bene nel caso delle small write. In questo caso FTL dovrebbe leggere una gran quantità di dati ancora corretti dal vecchio blocco e copiarlo in quello nuovo.

Immaginiamo che il client abbia scritto i blocchi logici 2000 - 2003 con i contenuti a,b,c,d e questi sono locati al blocco fisico 1 sulle pagine fisiche 4,5,6,7. FTL deve solo associare una singola traduzione. L'indirizzo logico ora è fatto da due parti:

| Chunk number | Offset | 
| ------------ | ------ |

Per leggere le pagine è semplice: come siamo già abituati il chunk number viene tradotto e si unisce la traduzione con l'offset
Ma cosa succede se il client vuole scrivere c' al posto di c? FTL deve leggere tutto il vecchio blocco e copiarlo in un nuovo blocco scrivendo c' al posto di c. Questo porta a grossi problemi di performance.

## Mapping ibrido
Con questo approcio FTL tiene un certo numero di blocchi cancellati e manda tutte le scritture a loro (log block).
FTL mantiene due tipi di mapping in memoria: un piccolo set di map per pagina chiamata log table e un set  più grosso indirizzato a blocchi chiamato data-table. Quando bisogna cercare un particolare blocco logico FTL consulta prima la log table e se non lo trova lì consulta il data table.
Lo scopo è mantenere piccola la log table e muovere quando possibile il contenuto in posti dove possa essere indirizzato da un singolo puntatore a blocco.
Immaginiamo che la FTL sia nella seguete situazione:

| Block:  | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   |
| ------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Page:   | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | 10  | 11  |
| Content |     |     |     |     |     |     |     |     | a   | b   | c   | d   |
| State:  | i   | i   | i   | i   | i   | i   | i   | i   | V   | V   | V   | V    |

Log table:
vuota
Data table:

| logical | physical |
| ------- | -------- |
| 250     | 8        | 

Ora immaginiamo che il cliente riscriva ognuno dei blocchi scrivendo a',b',c',d' nelle stesse posizioni di prima. La situazione sarà:

| Block:  | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   |
| ------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Page:   | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | 10  | 11  |
| Content | a'  | b'  | c'  | d'  |     |     |     |     | a   | b   | c   | d   |
| State:  | V   | V   | V   | V   | i   | i   | i   | i   | V   | V   | V   | V   |

Log table:

| logical | physical |
| ------- | -------- |
| 1000    | 0        |
| 1001    | 1        |
| 1002    | 2        |
| 1003    | 3        |

Data table:

| logical | physical |
| ------- | -------- |
| 250     | 8        | 

A questo punto siccome questi blocchi sono stati scritti esattamente come prima FTL può eseguire uno switch merge al termine del quale lo stato è:

| Block:  | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 1   | 2   | 2   | 2   | 2   |
| ------- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Page:   | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | 10  | 11  |
| Content | a'  | b'  | c'  | d'  |     |     |     |     |     |     |     |     |
| State:  | V   | V   | V   | V   | i   | i   | i   | i   | i   | i   | i   | i   | 

Log table:
vuota
Data table:

| logical | physical |
| ------- | -------- |
| 250     | 0        |

Sfortunatamente non sempre la FTL è così fortunata se per esempio dalla stessa posizione di partenza il client avesse riscritto solo due blocchi deve eseguire quello che viene chiamato un merge parziale: deve leggere dalla memoria il vecchio blocco, aggiungere i dati mancanti al log e poi eseguire lo switch. Nel caso di full merge FTL deve prendere più blocchi. Immagina per esempio cosa succede se nel log vengono scritti i blocchi logici 0,4,8,12 ognuno di loro va su un blocco diverso quindi devo leggere e scrivere quattro blocchi.

## Page mapping e caching
Vista la complessità dell'approccio ibrido si può cercare di ovviare ai lati negativi usando una cache in memoria per le mappe e tenendo le altre in flash. Questo approccio può funzionare bene ma se la memoria non può contenere tutta la zona che serve ci sarà bisogno di liberare una page map dalla memoria, scriverla sul device nel caso sia stata modificata e caricare quella necessaria.

## Distribuzione del consumo
L'approccio strutturato a log fa un buon lavoro all'inizio ma a volte un blocco può essere riempito con dati che non vengono modificati per lungo tempo e quindi l'utilizzo di quel blocco è minore. Per questo FTL riscrive questi blocchi in un'altra posizione. Questo processo aumenta la write amplification 

