# Semafori
Dijkstra e i suoi colleghi inventarono i semafori come una primitiva per tutte le possibili problematiche relative alla sincronizzazione. Sono oggetti con un valore intero che possiamo manipolare con due routine: `sem_post()` e `sem_wait()`. Come vedremo il valore iniziale di un semaforo determina il suo comportamento, quindi per prima cosa dobbiamo inizializzarlo a un quache valore.
```c
#include<semaphore.h>
sem_t s;
sem_init(&s, 0, 1);
```
Il primo argomento indica il semaforo da inizializzare, il secondo se messo a zero indica che il semaforo è condiviso tra tutti i [[Thread]] dello stesso [[processo]], mentre il terzo argomento è il valore che assegnamo al semaforo.
Dopo aver inizializzato il semaforo possiamo chiamare una delle due funzioni che abbiamo presentato all'inizio
```c
int sem_wait(sem_t *s){
	//decrement the value of the semaphore s by one
	//wait if the value of the semaphore s is negative
}

int sem_post(sem_t *s){
	//increment the value of semaphore s by one
	//if there are one or more threads waiting wake one
}
```
`sem_wait()` può ritornare subito (se il valore del semaforo era maggiore o uguale a 1 quando abbiamo chiamato `sem_wait()`) o fa sospendre l'esecuzione del chiamante mettendolo in coda per aspettare di essere svegliato.
`sem_post()` invece ritorna sempre subito.

## Semafori binari (Lock)
Come abbiamo detto i semafori possono essere usati un po' per fare tutto, compreso sostituire i [[Lock]]. Basta circondare la sezione critica con `sem_wait()` e `sem_post()` e inizializzare il semaforo a 1.
```c
sem_t m;
sem_init(&m, 0, 1);

sem_wait(&m);
//critical section
sem_post(&m);
```
Infatti quando un thread chiama `sem_wait()` decrementa il valore del semaforo che diventa zero. Siccome $0 \geq 0$ la funzione ritorna e il thread entra nella sezione critica. Quando il thread T0 ha il lock e un altro thread T1 cerca di entrare nella sezione critica chiamando `sem_wait()`, decrementa il valore del semaforo a $-1 < 0$, si addormenta e aspetta che T0 chiami `sem_post()` riportando il valore del semaforo a 0 e risvegliando il thread. Quando anche T1 avrà finito di eseguire la sezione critica chiamerà `sem_post()` e il semaforo avrà nuovamente valore 1.

## [[Condition Variables]]
I semafori sono anche utili per ordinare gli eventi in un programma concorrente. Per esempio, un thread potrebbe volere che una lista diventi non vuota prima di eliminare un elemento da essa. Anche in questo caso possiamo usare i semafori.
```c
sem_t s;
void * child(void *arg){
	printf("child\n");
	sem_post(&s);
	return NULL;
}

int main(int argc, char *argv[]){
	sem_init(&s, 0, 0);
	printf("parent:begin\n");
	pthread_t c;
	pthread_create(&c, NULL, child, NULL);
	sem_wait(&s);
	printf("parent: end\n");
	return 0;
}
```
Da notare che abbiamo inizializzato il semaforo a 0 perchè se il padre dovesse cercare di superare il `sem_wait()` prima del figlio decrementa il semaforo a -1 e si blocca. Successivamente il figlio termina, fa `sem_post()` e sveglia il padre. Nel caso in cui il figlio esegue prima del padre incrementa il semaforo a 1 e termina. Quando tocca al padre, questo decrementa il semaforo e visto che $0 \geq 0$ `sem_wait()` ritorna subito.
## Produttore consumatore
```c
int buffer[MAX];
int fill = 0;
int use = 0;

void put(int value){
	buffer[fill] = value; 		//F1
	fill = (fill + 1) % MAX; 	//F2
}

int get(){
	int temp = buffer[use]; 	//G1
	use = (use + 1) % MAX; 		//G2
}

sem_t empty;
sem_t full;

void *producer(void *arg){
	int i;
	for (i = 0;i < loops; i++) {
		sem_wait(&empty); 		//P1
		put(i); 				//P2
		sem_post(&full); 		//P3
	}
}

void *consumer(void *arg){
	int i, tmp = 0;
	while(tmp != -1){
		sem_wait(&full); 		//C1
		tmp = get(); 			//C2
		sem_post(&empty); 		//C3
		printf("%d\n", tmp);
	}
}

int main(int argc, char *argv[]){
	// ..
	sem_init(&empty, 0, MAX);
	sem_init(&full, 0, 0);
}

```
Il nostro primo tentativo consiste nell'avere due semafori: `empty` e `full`, che i thread useranno per comunicare che il buffer è stato svuotato o riempito. Il producer aspetta che il buffer diventi vuoto prima di inserire dei dati e allo stesso modo il consumer aspetta che il buffer si riempa. Quando il consumer arriva alla riga C1, siccome `full` è stato inizializzato a 0, la chiamata lo decrementa a -1 bloccando il consumer e aspettando che qualche altro thread chiami `sem_post()`. Se esegue il produttore arriverà alla riga P1 chiamando `sem_wait(&empty)` che è stato inzializzato al valore `MAX` (ovvero 1 in questo caso). Quindi il semaforo viene decrementato e il producer inserirà i suoi dati nel buffer. Infine esegurà la riga P3 svegliando il consumer. Se il Producer continuasse la sua esecuzione tornerebbe in loop a P1 dove si bloccherebbe. Immaginiamo che però `MAX > 1` e che ci siano più producer e più consumer. In questo caso abbiamo una race condition. Immaginiamo infatti di avere due produttori che chiamano la put() circa nello stesso momento e che il primo abbia iniziato a scrivere nel buffer ma sia stato interrotto mentre stava per aggiornare il valore di `fill`. Se il secondo produttore scrivesse ora sovrascriverebbe i dati del primo.
```c
sem_t mutex;
sem_t empty;
sem_t full;

void *producer(void *arg){
	int i;
	for (i = 0;i < loops; i++) {
		sem_wait(&mutex); 		//P0
		sem_wait(&empty); 		//P1
		put(i); 				//P2
		sem_post(&full); 		//P3
		sem_post(&mutex); 		//P4
	}
}

void *consumer(void *arg){
	int i, tmp = 0;
	while(tmp != -1){
		sem_wait(&mutex); 		//C0
		sem_wait(&full); 		//C1
		tmp = get(); 			//C2
		sem_post(&empty); 		//C3
		sem_post(&mutex); 		//C4
		printf("%d\n", tmp);
	}
}

```
Con questo secondo tentativo ci avviciniamo alla soluzione ma abbiamo ancora un prblema: questo codice può provocare deadlock. Immaginiamo infatti il caso in cui il consumer esegue per primo. Acquisisce il mutex e chiama ``sem_wait(&full)`` bloccandosi perchè il buffer è vuoto. A questo punto un producer esegue e rimane bloccato a `sem_wait(&mutex)` in quanto il mutex è ancora del processo che sta dormendo. La soluzione però è semplice: basta ridurre la dimensione della sezione critica.
```c
sem_t mutex;
sem_t empty;
sem_t full;

void *producer(void *arg){
	int i;
	for (i = 0;i < loops; i++) {
		sem_wait(&empty); 		//P1
		sem_wait(&mutex); 		//P1.5
		put(i); 				//P2
		sem_post(&mutex); 		//P2.5
		sem_post(&full); 		//P3
	}
}

void *consumer(void *arg){
	int i, tmp = 0;
	while(tmp != -1){
		sem_wait(&full); 		//C1
		sem_wait(&mutex); 		//C1.5
		tmp = get(); 			//C2
		sem_post(&mutex); 		//C2.5
		sem_post(&empty); 		//C3
		printf("%d\n", tmp);
	}
}

int main(int argc, char *argv[]){
	// ..
	sem_init(&empty, 0, MAX);
	sem_init(&full, 0, 0);
}
```
## Reader writer lock
In questo caso essendo solo letture possiamo permettere a più thread di leggere in contemporanea a patto che non ci siano inserimenti in corso
```c
typedef struct _rwlock_t{
	sem_t lock; 			//semaforo binario per l'accesso alla struttura dati
	sem_t writelock; 		//permette l'accesso a UN writer o TANTI rader
	int readers; 			//numero di reader nella sezione critica
} rwlock_t;

void rwlock_init(rwlock_t *rw){
	rw->readers = 0;
	sem_init(&rw->lock, 0, 1);
	sem_init(&rw->writelock, 0, 1);
}

void rwlock_acquire_readlock(rwlock_t *rw){
	sem_wait(&rw->lock);
	rw->readers++;
	if(rw->readers == 1)
		sem_wait(&rw->writelock);
	sem_post(&rw->lock);
}

void rwlock_release_readlock(rwlock_t *rw){
	sem_wait(&rw->lock);
	rw->readers--;
	if(rw->readers == 0)
		sem_post(&rw->writelock);
	sem_post(&rw->lock);
}

void rwlock_acquire_writelock(rwlock_t *rw){
	sem_wait(&rw->writelock);
}

void rwlock_release_writelock(rwlock_t *rw){
	sem_post(&rw->writelock);
}
```
una volta che un reader ha il lock altri reader possono possedere il lock, ma se un writer vuole prendere il lock deve aspettare che tutti i reader abbiano finito. Con questa implementazione è facile che i writers starvino.

## Dining philosophers
Per risolvere questo problema basta che un filosofo inverta l'ordine con cui cerca di accedere alle posate.
## Implementazione: zemafori
```c
typedef struct __Zem_t{
	int value;
	pthread_cond_t cond;
	pthread_mutex_t lock;
}Zem_t;

//solo uno può chiamarla
void Zem_init(Zem_t *s, int value){
	s->value = value;
	Cond_init(&s->cond);
	Mutex_init(&s->lock);
}

void Zem_wait(Zem_t *s){
	Mutex_lock(&s->lock);
	while(s->value <= 0)
		Cond_wait(&s->cond, &s->lock);
	s->value--;
	Mutex_unlock(&s->lock);
}
void Zem_post(Zem_t *s){
	Mutex_lock(&s->lock);
	s->value++;
	Cond_signal(&s->cond);
	Mutex_unlock(&s->lock);
}
```