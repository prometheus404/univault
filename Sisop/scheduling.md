---
aliases: [scheduler, scheduling, round robin, mlfq, fifo, sjf, response time]
---
#policy #cpu_virtualization
# Scheduling
Mentre il [[context switch]] si occupa dei meccanismi di basso livello lo scheduling si occupa delle policy applicate per cambiare il [[processo]] in esecuzione. Prima di iniziare a vedere come funziona lo scheduling però facciamo delle ipotesi sul carico di lavoro (workload) che rilasseremo piano piano per arrivare ad una rappresentazione più realistica di come funziona un algoritmo di scheduling:
1. ogni _job_ viene eseguito per la stessa quantità di tempo
2. tutti i _job_ arrivano in coda nello stesso momento
3. una volta partito un _job_ viene eseguito fino in fondo
4. tutti i job usano solo la CPU (no I/O)
5. Si conosce il tempo di esecuzione dei _job_

## Turnaround time
Inoltre dobbiamo introdurre il concetto di metrica per poter valutare l'efficienza dei metodi che stiamo prendendo in considerazione.
il **turnaround time** è la prima metrica che introduciamo e viene calcolata in questo modo:
$$T_{turnaround} = T_{completion} - T_{arrival}$$
e siccome per le nostre attuali assunzioni ogni _job_ arriva allo stesso momento, possiamo considerare $T_{arrival} = 0$ e quindi $T_{turnaround} = T_{completion}$.

## FIFO
È l'algoritmo più basilare che ci possa venire in mente: il primo che arriva viene eseguito per primo. In questo caso arrivano tutti nello stesso momento e FIFO li esegue nell'ordine A,B,C quindi $T_{avgturnaround} = \frac{10 + 20 + 30}{3}=20$
Per ora fila tutto liscio, ma iniziamo a rilassare l'ipotesi 1 e ipotizziamo che A duri 100 secondi invece che 10 come gli altri. A questo punto il tempo di turnaround medio è estremamente alto ($\frac{100+110+120}{3}=110s$). Questo effetto viene generalmente chiamato **effetto convoglio** e per evitarlo dobbiamo cercare un algoritmo un po' più sofisticato.

## Shortest Job First
Come dice il nome questo algoritmo esegue prima il _job_ più breve, quindi in questo caso l'ordine di esecuzione sarebbe B,C,A e come effetto il tempo di turnaround diventa $\frac{10+20+120}{3}=50$ e non a caso è dimostrabile che, fatte le ipotesi che abbiamo fatto noi, questo è l'algoritmo ottimale per risolvere questo problema. Rilassiamo però ulteriormente le ipotesi, in particolare l'ipotesi 3. Immaginiamo poi che A,B,C siano dimensionati come nell'ultimo esempio ma questa volta A arrivi a $t=0$ mentre B e C arrivino a $t=10$. In questo modo si verifica ancora l'effetto convoglio e $T_{turnaround} = \frac{100+(110-10)+(120-10)}{3} = 103.33s$

## Shortest Time-to-Completion First
Per risolvere questo problema dobbiamo rilassare un'altra ipotesi (la 4) e trasformare lo scheduler da non-preemptive a preemptive. Quando arrivano B e C infatti lo scheduler può mettere in pausa A e decidere di eseguire un altro _job_ riprendendo A in seguito. Quale sceglierà? Come dice il nome STCF, ogni volta che arriva un nuovo _job_ controlla quale termina prima ed esegue quello. Nel nostro caso a $t=10$ lo scheduler metterà in pausa A ed eseguira B e C fino al loro completamento, poi porterà a termine l'esecuzione di A. Il risultato è un turnaround medio di $T_{avgturnaround} = \frac{(120 - 0)+(20-10)+(30-10)}{3} = 50$ il che è decisamente un miglioramento rispetto al tempo precedente.

## Response Time
Nei sistemi moderni però diventa sempre più importante che siano **responsivi**: l'interazione con l'utente è ora fondamentale e quindi è necessario che il computer sia in grado di rispondere in fretta in casi in cui la latenza è un fattore determinante (come per esempio mouse e tastiera). Introduciamo perciò una nuova metrica: il Response Time.
$$ T_{response} = T_{start} - T_{arrival}$$

## Round Robin
RR è uno scheduler che cerca di ottimizzare il Response Time. Invece di eseguire un _job_ fino alla fine (sempre che non ne arrivi uno più breve), RR esegue un _job_ per un quanto di tempo e poi passa al _job_ successivo nella coda. Da notare che la lunghezza di un quanto deve essere un multiplo della durata di un [[LDE|timer-interrupt]]. Per esempio in un sistema con tre _job_ A,B,C che arrivano nello stesso momento e devono essere eseguiti per 5 secondi nel caso di RR con time slice di 1s $T_{avgResponse} = \frac{0+1+2}{3}=1$, mentre per SJF (e in questo caso anche STCF) $T_{avgResponse} = \frac{0+5+10}{3} = 5$.
In un sistema il dimensionameto di un quanto è critico perchè se è troppo corto il costo dei [[context switch]] supera il tempo di esecuzione, mentre se è troppo lungo ammortizza il costo dei [[context switch]] ma peggiora il response time.

## I/O
Liberiamoci dell'ipotesi che i _job_ utilizzino esclusivamente la CPU. Durante un I/O un [[processo]] non usa la CPU, quindi il [[SO|sistema operativo]] può mettere il [[processo]] in uno stato blocked in cui attende la fine dell'I/O. Trattando i burst di utilizzo della CPU come _job_ separati inoltre aumentiamo la responsiveness e ci assicuriamo che programmi interattivi siano eseguiti frequentemente e in fretta. Con questo possiamo inoltre rilassare anche l'iptesi che lo scheduler sia un oracolo

## MLFQ
Con le Multilevel feedback Queue vorremmo ottimizzare il turnaround time che, come abbiamo visto, si può fare eseguendo prima i _job_ più corti. Il problema è che il [[SO]] non ha modo di capire quanto tempo manca perchè un programma termini. Inoltre vorremmo che il nostro sistema sia anche interattivo e quindi abbia un buon response time ma questo sappiamo che inciderà sul turnaround time.
Iniziamo a costruire il nostro algoritmo passo passo. Come dice il nome MLFQ ha un numero di code distinte a ognuna delle quali è assegnato un diverso livello di **priorità**. In ogni momento ogni _job_ è su una singola coda e l'algoritmo sceglie di eseguire il _job_ presente nella coda a priorità più alta. Ovviamente più job possono essere presenti su una stessa coda, quindi MLFQ esgue tutti i job in RR.
1. Priority(A) > Priority(B) -> esegue A e non B
2. Priority(A) = Priority(B) -> esegue A e B in Round Robin

Invece di assegnare la priorità in modo statico però MLFQ cerca di imparare dal comportamento osservato: se un _job_ esegue tante richieste di I/O frequentemente sarà considerato un _job_ interattivo e quindi manterrà una priorità alta, mentre se invece fa lunghe elaborazioni e solo alla fine mostra il risultato la sua priorità sarà più bassa.
### Primo tentativo di cambio priorità
aggiungiamo le seguenti regole
3. Quando un _job_ entra nel sistema viene messo nella coda a priorità più alta.
4. (a) se un _job_ usa tutto il suo time slice la sua priorità viene ridotta, (b) se un _job_ non finisce il suo time slice e lascia prima la CPU (per I/O) la sua priorità rimane invariata

#TODO inserire delle immagini degli esempi
#### Problemi con questo approccio
Peccato che questo approccio abbia delle falle:
- starvation: se ci sono troppi programmi può capitare che questi consumino tutta la CPU non permettendo che i [[processo|processi]] più pesanti vengano eseguiti.
- gaming the scheduler: si possono riscrivere i programmi in modo che chiamino una richiesta di I/O appena prima di terminare il proprio quanto facendo quindi pensare allo scheduler di essere dei [[processo|processi]] interattivi e di dover stare nelle code più alte mentre invece monopolizzano la CPU

### Secondo tentativo: Priority Boost
Per risolvere la starvation aggiungiamo una regola
5. dopo un periodo di tempo S, tutti i _job_ vengono nuovamente spostati nella coda a priorità più alta
In questo modo abbiamo anche la garanzia che la coda di priorità rappresenti il comportamento **attuale** del [[processo]], evitando che [[processo|processi]] che hanno avuto una sola fase di calcolo pesante siano penalizzati per tutto il resto della loro esecuzione. S è una [[voo-doo costants]]: troppo basso e non c'e abbastanza tempo per distinguere tra [[processo|processi]] responsivi e pesanti e troppo alto e si rischia la starvation.

### Terzo tentativo: better Accounting
Per evitare che un [[processo]] malevolo possa monopolizzare la CPU dobbiamo modificare la regola 4:
4. Quando un [[processo]] usa il suo quanto di tempo (indipendentemente da quanti burst di CPU abbia impiegato per finirlo) la sua priorità viene ridotta

### Tuning
Come bisogna scegliere le variabili di MLFQ? non ci sono risposte semplici, sono quasi tutte delle [[voo-doo costants | voo-doo costant]], si impara con l'esperienza. In alcune implementazioni i quanti variano a seconda della coda (più brevi per le code più alte e più lunghi per quelle più basse), altre implementazioni non usano le regole che abbiamo descritto ma calcolano la priorità tramite formule matematiche (per esempio lo scheduler di FreeBSD), altre ancora usano metodi più complessi (per esempio assicurandosi che il [[SO|sistema operativo]] abbia priorità migliori), altre permettono all'utente di modificare al volo le priorità in fase di esecuzione

### Regole
1. Priority(A) > Priority(B) -> esegue A e non B
2. Priority(A) = Priority(B) -> esegue A e B in Round Robin
3. Quando un _job_ entra nel sistema viene messo nella coda a priorità più alta.
4. Quando un [[processo]] usa il suo quanto di tempo (indipendentemente da quanti burst di CPU abbia impiegato per finirlo) la sua priorità viene ridotta
5. Dopo un periodo di tempo S, tutti i _job_ vengono nuovamente spostati nella coda a priorità più alta