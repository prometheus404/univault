# The fast file system
Le performance del primo file system unix (simile a [[vsfs]]) erano terribili, al punto che con il tempo arrivavano a usare solo il 2% della banda disponibile. Il problema era che il [[HDD|disco]] veniva trattato come se fosse ram. Infatti spesso i file erano molto lontani dal loro inode (incrementando il seek time) e anche peggio i dati finivano sparpagliati per il disco perchè lo spazio non era gestito bene. Infatti per la gestione dello spazio veniva usata una free list che teneva traccia dei blocchi liberi e veniva semplicemente preso il primo libero.

| A1  | A2  | B1  | B2  | C1  | C2  | D1  | D2  | 
| --- | --- | --- | --- | --- | --- | --- | --- |

Se per esempio da questa configurazione del disco venivano cancellati (liberando i rispettivi blocchi) i file B e D, per allocare successivamente il file E da quattro blocchi il risultato era una frammentazione sul disco del file E:

| A1  | A2  | E1  | E2  | C1  | C2  | E1  | E2  | 
| --- | --- | --- | --- | --- | --- | --- | --- |

Per evitare questo problema naquero programmi di deframmentazione dei dischi ma era solo un modo per tamponare il problema perchè erano lenti e dispendiosi in termine di risorse.
Nasce così FFS, dall'idea di creare un sistema che fosse a conoscienza della forma del disco e che di conseguenza fosse più veloce. Tutto questo mantenendo le stesse interfacce, permettendo quindi di essere cambiato senza scrivere programmi ad hoc (tutti i nuovi filesysem cercano di fare questa cosa).

## Cylinder group
FFS divide il disco in un numero di gruppi di cilindri. Un singolo cilindro è un set di tracce su superfici diverse di un [[HDD]] che hanno la stessa distanza dal centro del disco e che quindi possono essere letti contemporaneamente senza seek. Da notare che i moderni dischi non permettono più questa visione del disco perchè nascondono l'implementazione interna, quindi filesystem come ext2-4 organzzano il sistema in gruppi di blocchi.
A prescindere dal nome e dall'implementazione l'idea è la stessa: mettere due file all'interno dello stesso gruppo vuol dire che la distanza tra uno e l'altro sarà minore.
In ogni gruppo FFS include tutte le strutture tipiche di un file system:

| S   | ib  | db  | inodes | Data | 
| --- | --- | --- | ------ | ---- |

- Una copia del superblocco per affidabilità
- una inode bitmap del gruppo
- una data bitmap del gruppo
- gli inode dei dati nel gruppo
- i dati

Notare che la bitmap è un'ottima struttura dati per gestire lo spazio perchè ci permette di trovare chunk contigui in cui mettere un file per diminuire gli overhead.

## Allocation Policy
Lo scopo è sempre lo stesso: mettere vicine le cose vicine. Di conseguinza per l'allocazione delle directory FFS cerca un gruppo con un numero di directory allocate basso (per bilanciare il numero di directory tra gruppi) e un numero di blocchi liberi alto (per poter allocare i file). 
Per i file invece fa due cose: prima si assicura di allocare i blocchi dati nello stesso gruppo in cui ha allocato l'inode, poi mette i file nello stesso blocco in cui ha messo la directory. Queste policy non sono basate su studi estensivi sui carichi dei filesystem ma semplicemente sul buon senso.
Infatti analizzando un tipico carico di un file system ci accorgiamo che la maggior parte delle richieste hanno località due o meno. La località tra un file e se stesso è 0, tra un file e un altro nella stessa directory è 1, e così via.
A questa regola però c'è un'eccezione importante per i file di grosse dimensioni: dopo un certo numero di blocchi allocati nel primo gruppo FFS mette il prossimo chunk del file in un altro (magari scelto perchè utilizzato poco). Scegliere la dimensione dei chunk è fondamentale perchè permette di ammortizzare il costo dei seek tra un gruppo e l'altro: più tempo passo a trasferire il pezzo di file contiguo meno peserà il salto al prossimo.

## Altre inovazioni

Per il supporto a dati di piccole dimensioni vennero introdotti i sottoblocchi da 512byte. Se il file cresce abbastanza da occupare 4kb di dati FFS trova un blocco da 4KB, copia i sottoblocchi e libera i sottoblocchi. Nel caso generale questo comportamento però viene evitato con un sistema di caching per evitare problemi di performance.

Venne inoltre introdotta la parametrizzazione dei dischi. Se richiedo di leggere il blocco 1 e poi chiedo il 2 può capitare che il disco sia già andato troppo avanti e devo aspettare un intero giro per potter leggere il blocco 2. Quidi i blocchi venivano organizzati in modo diverso:

| 1   | 7   | 2   | 8   | 3   | 9   | 4   | 10  | 5   | 11  | 0   | 6   | 
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |

in questo modo si ovviava a questo problema (notare che questo non è più necessario con i moderni dischi perchè hanno dei buffer molto grossi in cui tengono tutta la traccia letta nel caso venisse chiesto un settore già letto)

Infine venne introdotto il [[API|symbolyc link]]