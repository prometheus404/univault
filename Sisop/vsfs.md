# Very simple file system
Il file system è puro software, non abbiamo bisogno di aiuti esterni dall'hardware.
## Organizzazione
La prima cosa che dobbiamo fare è dividere il disco in blocchi. File system semplici usano solo un blocco(scegliamo una dimensione comune da 4kb) come blocksize e lo faremo anche noi. Iniziamo a riservare una parte del disco ai dati dell'utente (si spera sia la maggior parte del disco). Dopodichè siccome dobbiamo tracciare tutta una serie di informazioni sul file riserviamo dello spazio per la inode table. Immaginiamo che i nostri inode occupino 256byte, quidi un blocco può contenere 16 inode nel nostro sistema abbiamo lasciato 5 blocchi per questa parte quindi ci possono stare in totale 80 inode (quindi 80 file nel sistema). Manca una struttura che tenga traccia degli inode liberi e una per i datablock liberi. Potremmo usare una free-list ma usiamo due bitmap una per la regione dati e una per gli inode. Nel nosrtro caso assegniamo un blocco ciascuna (è tanto per il nostro sistema ma è per semplicità). Teniamo inoltre un blocco per il superblocco che contiene informazioni su questo tipco filesystem come per esempio quali inode e quanti blocchi dati ci sono e anche un magic number per identificare il tipo di filesystem. Quando un filesystem viene montato viene prima letto il superblock per inizializzare i vari parametri. il nostro filesystem sarà quindi fatto in questo modo (ogni casella è un blocco):

| -   | -            | -           | -   | -   | -   | -   |
| --- | ------------ | ----------- | --- | --- | --- | --- |
| S   | inode bitmap | data bitmap | i   | i   | i   | i   |
| D   | D            | D           | D   | D   | D   | D   |
| D   | D            | D           | D   | D   | D   | D   |
| D   | D            | D           | D   | D   | D   | D   |
| D   | D            | D           | D   | D   | D   | D   |
| D   | D            | D           | D   | D   | D   | D   |
| D   | D            | D           | D   | D   | D   | D   |
| D   | D            | D           | D   | D   | D   | D   |

## Inode
È l'abbreviazione di index node (perchè storicamente si trovavano in un array). Ogni inode viene indicizzato da un numero (chiamato i-number) che abbiamo detto è il nome a basso livello del file. Dato un i-number è facile calcolare direttametne la posizione dell'inode in memoria
![[inode_table.png]]

Per esempio nel nostro sistema (inode table di 20kb (5 blocchi da 4k)) sappiamo che ci sono 80 inode da 256 byte a partire da 12kb (super block a 0kb, inode bitmap a 4k, data bitmap a 8kb). Per leggere l'inode 32 devo calcolare l'offset nella inode region (32 * sizeof(inode)) e aggiungerla all'inizio della inode table (12kb). siccome i dischi non sono byte addressable ma sono fatti da settori (solitamente 512byte) dobbiamo dividere il numero ottenuto per 512, quindi otteniamo 40. In generale:
```c
blk = (inumber * sizeof(inode_t))/blockSize
sector = ((blck*blockSize)+ inodeStartAddr) / sectorSize;
```
Una delle scelte di design più importanti è quella di decidere come l'inode indica dove si trovano i blocchi dati. Un approccio semplice è avere uno o più puntatori diretti (puntano a indirizzi su disco) ma se la dimensione dei file è grande questo diventa un problema perchè dovrei avere inode più grandi.
Per questo una strategia comune è avere un puntatore speciale chiamato indirect pointer che invece di puntare a un blocco che contiene dati punta a un blocco che contiene altri puntatori. Siccome però nei sistemi moderni i file possono essere grandi vengono aggiunti anche un double indrect pointer (che punta a un blocco che contiene puntatori indiretti) e un triple indirect pointer.
Questo approccio si basa sul fatto che la maggior parte dei file su un sistema è di piccole dimensioni e quindi ha senso ottimizzare l'accesso a questo tipo di file.
## Directory
le directory come già detto sono solo file che contengono una lista di coppie (entry name, inode number). 

| inum | reclen | strlen | name |
| ---- | ------ | ------ | ---- |
| 5    | 12     | 2      | .    |
| 2    | 12     | 3      | ..   |
| 12   | 12     | 4      | foo  |
| 13   | 12     | 4      | bar  | 

Notare che cancellare un file può lasciare uno spazio vuoto in questo file e quindi deve esistere un modo per segnalarlo(per esempio inode number a 0) e reclen serve per fare in modo che se una nuova entry usa meno spazio ?????.
Spesso i file system trattano le directory come un tipo di file speciale. I blocchi contenenti il contenuto della directory si trovano nella regione dati. Invece che questa struttura lineare se ne possono usare altre (tipo alberi) che magari sono più veloci per cercare il nome di un file al loro interno.
## Gestione dello spazio libero
Un file system deve tenere traccia di quali inode e quali blocchi sono liberi e quali occupati. In vsfs abbiamo due bitmap per questo. Per esempio quando creiamo un file dobbiamo creare un inode. Il fs cercherà per un inode libero nella bitmap e lo marcherà come occupato (mette il bit a 1). Allo stesso modo farà per allocare i blocchi del file, anche se alcuni filesystem linux cercano una sequenza di blocchi per fare in modo che una porzione del file sarà contigua su disco.

## Access Path: Reading and Writing
### Reading
Quando viene chiamato
```c
open("/foo/bar". O_RDONLY)
```
il file system deve attraversare il pathname per trovare l'inode del file.
La posizione dell'inode della root directory è nota(in unix è l'inode 2), quindi la prima cosa che il fs fa è leggere i blocchi dati indirizzati dall'inode cercando al loro interno il nome `foo` e prendendo l'inode corrispondente. Continuando ricorsivamente trova il nome del file. A questo punto carica in memoria l'inode del file, controlla i permessi, alloca un file descriptor per il processo nella open file table del processo chiamante e restituisce il fd al processo chiamante. La prima lettura è a offset 0 (se non viene chiamato `lseek`) quindi viene letto il primo blocco del file (la sua posizione è nel primo puntatore nell'inode).
A un certo punto il file verrà chiuso e quindi viene deallocato il file descriptor. Notare che la quantità d I/O generato è proporzionale alla lunghezza del pathname. A peggiorare la situazione potrebbero esserci directory grandi che allungano il tempo di ricerca del nome del file.

### Writing
Prima di tutto il file va aperto, dopodichè viene chiamata una `write()`. La scrittura potrebbe allocare un nuovo blocco (a meno che sia solo una sovrascrittura dei blocchi già presenti). Quando si scrive su un nuovo file ogni write non solo deve scrivere dati su disco, ma deve anche decidere quale blocco allocare e aggiornare di conseguenza le strutture dati. Ogni write genera quindi 5 I/O. Due per leggere e aggiornare la bitmap, due  per leggere e poi scrivere l'inode e infine una per scrivere effettivamente il blocco. Per creare un file il fs non solo deve allocare l'inode ma anche allocare spazio nella directory che contiene il nuovo file, quindi il traffico totale diventa:
1. una read sulla inode bitmap per cercare un inode libero
1. una write sulla inode bitmap
2. una write sul  nuovo inode per inizializzarlo
3. una ai dati della directory (per aggiungere il link)
4. una read all'inode della directory
5. una write all'inode della directory (per aggiornarla)


## Cache e buffer 
Siccome leggere e scrivere file può essere un'operazione pesante molti file system fanno uso della memoria di sistema per mettere in cache i blocchi da importare. Senza il caching ogni apertura di file richiederebbe almeno due read per ogni livello di indirezione nella gerarchia delle directory. I primi file system avevano una cache di dimensione fissa per i blocchi popolari allocata a boot time e che occupava circa il 10% della memoria. Nei sistemi moderni si usa un approccio dinamico.
Il vantaggio del caching è che aspettando si possono accorpare le istruzioni di I/O in un numero più piccolo di istruzioni (per esempio due write allo stesso blocco diventano una sola write) e inoltre possono essere schedulate in modo più efficiente.