# RAID
Redundant Array of Inexpensive Disk è una tecnica che consente di usare più dischi insieme per ottenere un sistema più grosso, più veloce e più affidabile. Tutto questo in modo completamente trasparente!
Quando un file system chiama una richiesta logica di I/O, il RAID deve calcolare il [[HDD|disco]] o i dischi da accedere.
Internamente i raid sono abbastanza complessi: hanno microcontrollori, DRAM e anche buffer non volatili.
Per comparare diversi approcci creiamo un fault model semplice chiamato **fail-stop model** che si basa sul presupposto che  un disco possa essere in soli due stati: funzionante o non funzionante e nel secondo caso i dati su esso contenuto siano irrimediabilmente persi.
Valuteremo i RAID sulla base di:
- capacità: dato un set di N dischi da B blocchi, quanta capacità effettiva è utilizzabile dall'utente?
- affidabilità: quanti dischi si possono rompere prima di perdere dati?
- performance: Quanto ci vuole a leggere e a scrivere considerando workload sequenziali e random

## RAID 0: Striping
Il primo livello di raid in realtà non è raid in quanto non è ridondante.  però è un sistema ottimo dal punto di vista delle performance.

| Disk 0 | Disk1 | Disk2 | Disk3 |
| ------ | ----- | ----- | ----- |
| 0      | 1     | 2     | 3     |
| 4      | 5     | 6     | 7     |
| 8      | 9     | 10    | 11    |
| 12     | 13    | 14    | 15    |

Chiamiamo i blocchi nella stessa riga ma su dischi diversi una stripe (es blocchi 0 1 2 3 nell'esempio sopra)
### Chunk size
La dimensione dei chunk modifica le performance dell'array. Una chunk size piccola vuol dire che molti file verranno organizzati in stripe su molti dischi, incrementando il parallelismo delle read e write sul singolo file.
D'altro canto il tempo di posizionamento per accedere ai blocchi su dischi multipli aumenta perchè devo aspettare il disco più lento (nello scenario peggiore $T_{Positioning} = R + max(T_{seek})$)

### Valutazioni
#### Capacità
E perfetto. Dati N dischi di capacità B blocchi con lo striping otteniamo $N \times B$ blocchi utilizzabili.
#### Affidabilità
È perfetto... al contrario però. La rottura di un qualsiasi disco porta alla perdita di dati
#### Performance
Eccellente. Tutti i dischi vingono utilizzati, spesso in parallelo, per rispondere alle richieste. Ma valutiamo più nel dettaglio usando due diverse metriche della performance: single request latency (la latenza di una singola richiesta) e steady-state throughput (la banda totale di molte richieste concorrenti).
Immaginiamo che i dischi possano trasferire dati a S Mb/s in carico sequenziale e R Mb/s 
vedi:
[[Calcolo R e S |calcolare R e S]]

Da un punto di vista della latenza di una richiesta a singolo blocco è la stessa di un singolo disco, infatti RAID-0 la redirige semplicemente sul disco corretto. Dal punto di vista dello **steady-state throughput** ci aspettiamo di avere il massimo della banda del sistema, ovvero $N \times S$
Per un gran numero di richieste random possiamo ancora usare tutti i dischi quindi avere $N \times R \text{ MB/s}$

## Raid 1: Mirroring
Con un sistema mirrored abbiamo più di una copia di ogni blocco sul sistema. Ogni copia dovrebbe essere ovviamente su un disco separato. In questo modo possiamo tollerare la rottura dei dischi.

esempio di raid 10 (raid 1 + 0) dove fa il mirror e poi striping sulle coppie

| Disk 0 | Disk1 | Disk2 | Disk3 |
| ------ | ----- | ----- | ----- |
| 0      | 0     | 1     | 1     |
| 2      | 2     | 3     | 3     |
| 4      | 4     | 5     | 5     |
| 6      | 6     | 7     | 7     |

Quando il mirroring deve eseguire una lettura può farlo su entrambe le copie, mentre quando vuole scivere deve scrivere su entrambe in parallelo
### Valutazioni
#### Capacità
Da questo punto di vista il mirroring è abbastanza costoso visto che con un livello 2 di mirroring (due copie di ogni dato) su N dischi da B blocchi abbiamo solo $\frac{N \times B}{2}$ utilizzabili
#### Affidabilità
Da questo punto di vista fa decisamente meglio di raid 0, in quanto può tollerare la rottura di un qualsiasi disco. Con un po' di fortuna (tanta) può tollerare addirittura la rottura di N/2 dischi!
#### Performance
la latenza di una singola richiesta di lettura è uguale a quella di un disco singolo anche in questo caso, mentre quella di scrittura siccome deve fare due scritture in parallelo ha il problema che ci mette il tempo della scrittura più lenta.
Per quanto riguarda il **steady-state throughput** di un workload sequenziale ogni scrittura deve fare 2 scritture in parallelo, quindi la banda è $\frac{N\times S}{2}$. Nel caso delle read ci aspetteremmo miglioramenti ma non è così: immaginiamo di dover leggere i settori 0,1,2,3,4,5,6,7 e di leggere i pari dalla prima copia e i dispari dalla seconda, il problema è che la testina non salta il blocco che non legge e ci deve passare sopra lo stesso, quindi non abbiamo effettivamente un guadagno rispetto alle scritture.
Le letture random sono il caso migliore dove possiamo effettivamente sfruttare la doppia copia con $N\times R$ bandwidth, mentre le scritture come è facile pensare usano metà della banda

## Raid 4: Parity
Per cercare di risparmiare spazio ma mantenere l'affidabilità di un sistema mirrored proviamo a usare un bit di parità.
Per ogni stripe di dati aggiungiamo un singolo blocco di parità che ha informazioni ridondanti.

| Disk 0 | Disk1 | Disk2 | Disk3 |
| ------ | ----- | ----- | ----- |
| 0      | 1     | 2     | P0    | 
| 3      | 4     | 5     | P1    |
| 6      | 7     | 8     | P2    |
| 9      | 10    | 11    | P3    |

Per ricavare questo bit mettiamo semplicemente in xor bit a bit tutti gli altri blocchi. Se il bit è 0 vuol dire che nella stessa posizione negli altri blocchi ci sono un numero pari di 1. Se un disco si rompe basta rieseguire lo xor con anche il disco ridondante e si copia su un altro disco.

### Valutazioni
#### Capacità
Usa un disco di parità per ogni gruppo di dischi che protegge, quindi la capacità è $(N-1)\times B$
#### Affidabilità
Tollera la rottura di uno e un solo disco.
#### Performance
partiamo dallo **steady-state throughput** . Con lettura workload sequenziale può usare tutti i dischi eccetto quello della parità, quindi otteniamo $(N-1)\times S$ Mb/s
Quando un RAID-4 deve scrivere un grosso chunk di dati fa un'ottimizzazione chiamata **full-stripe-write** dove scrive in parallelo tutti i dischi (compreso quello di parità). Perciò la banda effettiva è $(N-1)\times S$ MB/s. Per quanto riguarda la performance di random read possiamo immaginare che siano sparse per tutti i dischi eccetto quello di parità, quindi abbiamo $(N-1)\times R$ MB/s di banda. Per quanto riguarda invece le random writes dobbiamo capire come avvengono le scritture di un singolo blocco. Ho due strade: leggere tuti i blocchi eccetto quello che sto sovarascrivendo e quello di parità, fare lo xor e metterlo nel blocco di parità(additive parity); oppure leggo il vecchio dato e la vecchia parità, e  fare $P_{new} = (C_{old} \oplus C_{new}) \oplus P_{old}$ (subtractive parity).
Immaginiamo inoltre che vengano richieste due write su due blocchi di due dischi diversi e su stripe diverse. In questo caso non posso eseguirle in parallelo perchè in entrambi i casi devo aggiornare il disco di parità.
Quindi abbiamo un pessimo (R/2) MB/s throughput con random write.
Infine la latenza di I/O. Nel caso di una singola lettura è equivalente a quella di un singolo disco mentre la latenza di una write è data dalla latenza di due read(che possono essere fatte in parallelo) e due write (anche loro possono essere fatte in parallelo).
## Raid 5: Rotating Parity
Funziona in modo identico al raid 4 ma con la differenza che ruota i bit di parità su i dischi

| Disk 0 | Disk1  | Disk2  | Disk3  |
| ------ | ------ | ------ | ------ |
| **P0** | 0      | 1      | 2      |
| 3      | **P1** | 4      | 5      |
| 6      | 7      | **P2** | 8      |
| 9      | 10     | 11     | **P3** |

### Valutazioni
#### Capacità
invariata
#### Affidabilità
invariata
#### Performance
La random read performance è un po' migliorata perchè può parallelizzare le richieste ottenendo $\frac{N}{4}\times R$MB/s (diviso 4 perchè genera 4 operazioni di I/O in totale)
Siccome è quasi sempre uguale alle performance del raid 4 tranne quando è meglio il Raid 5 ha del tutto sostituito il raid 4 