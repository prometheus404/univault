---
aliases: [SO, OS, Sistema operativo, sistemi operativi]
---
# Sistemi operativi
il Sistema operativo ha il compito di assicurarsi che il sistema operi correttamente e efficientemente in un modo semplice da usare.
Lo fa con tre tecniche fondamentali:
- [[virtualizzazione]]
- [[concorrenza]]
- [[persistenza]]

## Design goals
uno dei più basici obiettivi di un [[SO]] è quello di costruire [[astrazioni]] in modo da rendere il sistema più comodo e semplice da utilizzare ( per esempio virtualizzando un [[IO devices| device di I/O]] è possibile scrivere permanentemmente senza preoccuparsi del particolare tipo di disco utilizzato).
Bisogna inoltre tenere in considerazione le [[prestazioni]] minimizzando gli [[overhead]] del sistema operativo.
Un altro obiettivo da tenere in considerazione è la capacità di garantire la [[protezione]] tra applicazioni diverse: siccome abbiamo intenzione di far girare più applicazioni in contemporanea vogliamo essere sicuri che comportamenti accidentali o malevoli di un [[processo]] non abbiano ripercussioni sugli altri (e in particolar non abbiano ripercussioni sul sistema operativo). L'[[isolazione]] tra [[processo|processi]] è la chiave per garantire questa protezione.
Il [[SO]] deve inoltre girare senza fermarsi: un suo crash e *tutte* le applicazioni che girano sul sistema operativo crashano. In una parola il sistema operativo deve garantire [[affidabilità]].
Possono esserci anche altri obiettivi secondari come
- efficienza energetica
- sicurezza
- portabilità

## Storia
### Batch processing
all'inizio il sistema operativo non faceva molto, era solo un insieme di librerie di funzioni usate di frequente. Sui vecchi sistemi mainframe del tempo veniva eseguito solo un programma per volta fino al suo completamento e il controllo veniva eseguito da un operatore umano.
### Multiuser 
Quando i computer iniziarono ad essere più diffusi nelle aziende e utilizzati da utenti diversi ci si rese conto che il codice del sistema operativo era speciale perchè permetteva di controllare l'accesso alle zone separate di memoria tramite [[system call]] e la distinzione tra [[kernel e user mode]]
### Multiprogramming
Con l'avvento dei minicomputer, questi potevano essere assegnati a ogni utente, rendendo perciò necessario aumentre l'interattività di questi sistemi.
Perciò si cominciò a lavorare per rendere possibile l'esecuzione contemporanea di più [[processo|processi]], tecnica che prende il nome di _multiprogramming_.
Era importante inoltre ottimizzare le risorse perchè le periferiche di in/out erano molto lente ai tempi perciò si decise di permettere lo switch tra programmi per minimizzare i tempi di attesa durante l'in/out