# Condition variables
I [[Lock]] non sono l'unico tipo di primitiva che serve per costruire dei programmi concorrenti. Ci sono molti casi in cui un [[Thread]] vuole controllare se una condizione è vera prima di continuare la sua esecuzione (per esempio un genitore che aspetta la fine dell'esecuzione del figlio).
Potremmo usare una variabile condivisa, questa soluzione di solito funziona ma è estremamente inefficiente perchè il genitore spreca tepo CPU aspettando. Vorremmo quindi mettere a dormire il thread in attesa. Per questo definiamo le **Condition Variables**, che non sono altro che una coda esplicita in cui i thread possono mettersi in attesa che una certa condizione cambi. Una cv ha due operazioni associate: `wait(cond_t c, mutex_t m)` con la quale un programma può mettersi in coda e `signal(cond_t c)` con cui un thread può segnalare un cambiamento avvenuto e svegliare un thread dormiente.
La chiamata `wait()` ha come parametro anche un lock perchè da per scontato che il mutex sia blolccato quando viene chiamata. Il suo compito è quindi quello di sbloccare il lock, andare a dormire (atomicamente) e riprendere il lock quando si sveglia (atomicamente). Questo per evitare che si creino delle race condition.
```c
#include<stdio.h>
#include <pthread.h>
int done = 0;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;

void thr_exit(){
	pthread_mutex_lock(&m);
	done = 1;
	pthread_cond_signal(&c);
	pthread_mutex_unlock(&m);
}

void *child(void *arg){
	printf("child\n");
		thr_exit();
	return NULL;
}

void thr_join() {
	pthread_mutex_lock(&m);
	while(done == 0)
		pthread_cond_wait(&c, &m);
	pthread_mutex_unlock(&m);
}

int main(int argc, char *argv[]){
	printf("parent: begin\n");
	pthread_t p;
	pthread_create(&p, NULL, child, NULL);
	thr_join();
	printf("parent: end\n");
	return 0;
}
```
Ci si potrebbe chiedere perchè nel codice abbiamo bisogno della variabile di stato `done`. 
```c

void thr_exit(){
	pthread_mutex_lock(&m);
	pthread_cond_signal(&c);
	pthread_mutex_unlock(&m);
}
void thr_join() {
	pthread_mutex_lock(&m);
	pthread_cond_wait(&c, &m);
	pthread_mutex_unlock(&m);
}
```
Immaginiamo che il figlio venga eseguito per primo e chiami `thr_exit()` senza svegliare nessuno (nessuno è in coda). Quando viene eseguito il padre va a dormire e nessuno lo sveglierà.
Un'altra domanda è se abbiamo veramente bisogno del lock
```c
void thr_exit(){
	done = 1;
	pthread_cond_signal(&c);
}

void thr_join() {
	while(done == 0)
		pthread_cond_wait(&c, &m);
}
```
In questo caso se il genitore chiama `thr_join()` e controlla il valore di `done`, vedrà che è 0 e andrà a dormire. Ma appena prima di andare a dormire potrebbe essere intrerrotto dal figlio che cambia il valore della variabile a 1 e segnala (ma nessuno è ancora in attesa). Quando il genitore riprende l'esecuzione dorme per sempre
## Producer consumer
Immaginiamo ora di avere uno o più thread che producono una risorsa riempendo un buffer e uno o più thread che la consumano. Costruiamo quindi un buffer in comune (per semplicità usiamo un singolo intero) e due routine per riempire e svuotare il buffer
```c
int buffer;
int count = 0;

void put(int value){
	assert(count == 0);
	count = 1;
	buffer = value;
}

int get(){
	assert(count == 1);
	count = 0;
	return buffer;
}
```
Ora dobbiamo scrivre due routine che sappiano quando scrivere o leggere dal buffer
```c
void *producer(void *arg){
	int i;
	int loops = (int) arg;
	for(i = 0; i < loops; i++)
		put(i);
}

void *consumer(void *arg){
	int i;
	while(1){
		int tmp = get();
		printf("%d\n", tmp);
	}
}
```
Questo è il comportamento generico che vogliamo ottenere ma dobbiamo trovare un modo per proteggere la sezione critica. Ma mettere un lock attorno alla sezione critica non funziona, abbiamo bisogno di una condition variable.
Immaginiamo per ora di avere solo un producer e un consumer e iniziamo a provare una soluzione con una sola variabile `cond` e un lock `mutex`.
```c
void *producer(void *arg){
	int i;
	int loops = (int) arg;
	for(i = 0; i < loops; i++){
		pthread_mutex_lock(&mutex);
		if(count == 1)
			pthread_cond_wait(&cond, &mutex);
		put(i);
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&mutex);
	}
}

void *consumer(void *arg){
	int i;
	for( i = 0; i < loops; i++){
		pthread_mutex_lock(&mutex);
		if(count==0)
			pthread_cond_wait(&cond, &mutex);
		int tmp = get();
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&mutex);
		printf("%d\n", tmp);
	}
}
```
Però se abbiamo più di un thread la soluzione ha due problemi.
Immaginiamo di avere due consumer $T_{c1}$ e $T_{c2}$ e un producer $T_P$. Un consumer ($T_{c1}$) inizia l'esecuzione, acquisisce il lock, controlla se ci sono dei buffer pronti e vedendo che non ce ne sono va a dormire. Poi $T_P$ inizia l'esecuzione, prende il lock e controlla che i buffer non siano pieni. non essendo questo il caso va avanti e riempe il buffer, segnala che un buffer è disponibile ($T_{c1}$ è quindi in grado di eseguire ma non viene ancora eseguito) e continua finchè il buffer non è pieno e a quel punto si ferma. $T_{c2}$ inizia la sua esecuzione e siccome i buffer sono pieni consuma il loro contenuto e segnalando sulla condition variable `cond`. Però in questo modo non è possibile sapere che thread viene svegliato. Nel caso sia $T_{c1}$ esso si troverà buffer vuoti, triggererà l'assertion e terminerà l'esecuzione. La soluzione a questo problema è semplice: basta usare un while al posto dell'if, infatti signal è solo un suggerimento che lo stato è cambiato, mai una cosa certa. Però c'è ancora un bug. Se due consumer vengono eseguiti per primi e vanno a dormire il producer viene eseguito, riempe il buffer e segnala, a questo punto un consumer si sveglia, svuota il buffer e segnala ma sveglia un thread a caso. Se svegliasse il consumer questo andrebbe a dormire perchè non ci sono buffer disponibili, non segnalerebbe nulla e tutti i thread rimarrebbero a dormire.
Anche qui la soluzione è relativamente semplice: basta usare due condition variables per segnalare quando è `empty` e una per segnalar quando è `full`
```c
int loops;
cond_t empty, full;
mutex_t mutex;

void *producer(void *arg){
	int i;
	int loops = (int) arg;
	for(i = 0; i < loops; i++){
		pthread_mutex_lock(&mutex);
		while(count == 1)
			pthread_cond_wait(&empty, &mutex);
		put(i);
		pthread_cond_signal(&full);
		pthread_mutex_unlock(&mutex);
	}
}

void *consumer(void *arg){
	int i;
	for( i = 0; i < loops; i++){
		pthread_mutex_lock(&mutex);
		while(count==0)
			pthread_cond_wait(&full, &mutex);
		int tmp = get();
		pthread_cond_signal(&empty);
		pthread_mutex_unlock(&mutex);
		printf("%d\n", tmp);
	}
}
```
Possiamo adattare questo nostro esempio per avere buffer di dimensioni maggiori:
```c

void put(int value){
	buffer[fill_ptr] = value;
	fill_ptr = (fill_ptr + 1) % MAX;
	count++;
}

int get(){
	int tmp = buffer[use_ptr];
	use_ptr = (use_ptr + 1) % MAX;
	count--;
	return tmp;
}

int loops;
pthread_cond_t empty, full;
pthread_mutex_t mutex;

void *producer(void *arg){
	int i;
	int loops = (int) arg;
	for(i = 0; i < loops; i++){
		pthread_mutex_lock(&mutex);
		while(count == MAX)
			pthread_cond_wait(&empty, &mutex);
		put(i);
		pthread_cond_signal(&full);
		pthread_mutex_unlock(&mutex);
	}
}

void *consumer(void *arg){
	int i;
	for( i = 0; i < loops; i++){
		pthread_mutex_lock(&mutex);
		while(count == 0)
			pthread_cond_wait(&full, &mutex);
		int tmp = get();
		pthread_cond_signal(&empty);
		pthread_mutex_unlock(&mutex);
		printf("%d\n", tmp);
	}
}
```
## Covering Conditions
Immaginiamo un programma che alloca memoria. Quando si libera della memoria viene segnalato. Consideriamo il caso in cui due programmi cerchino di allocare rispettivamente 100M e 10M di memoria ma nessuna delle due richieste viene soddisfatta perchè la memoria è piena. A questo punto immaginiamo che vengano liberati 50M di memoria e quindi viene mandato un segnale sulla condizione. Se però si sveglia il primo thread (per l'allocazione di 100M) questo tornerà a dormire. La soluzione a problemi come questo è `pthread_mutex_broadcast()` che sveglia **tutti** i thread in attesa sulla condition variable.