Not all types of data have a natural representation as vectors in $\mathbb{R}^d$ (e.g. medical records) so [[KNN]] is not always feasible.
But we can simply solve this problem by subdividing the space (not in $\mathbb{R}^d$ but in d dimension each with different domain) using half-planes parallel to an axis. This way we don't need to apply linear combination to define the hyperplane.
A natural way to represent this cuts is by using an ordered tree (one where the children of any node are numbered consecutively) where each node is either a leaf tagged with a label or an internal node tagged with a test.
A test on attribute $i$ for an internal node with $k$ children is a function $f:\mathcal{X}_i\rightarrow\{1,..,k\}$ that maps each element of $\mathcal{X}_i$ to the corresponding node's children.
>[!note]
>this rappresentation is not less expressive than a set of half-spaces that delimits a voronoi region: in fact we can always represent a set of half-spaces as a binary tree where each test is if the point belongs to one side or the other of the half-space delimited by the plane

One big advantage of tree predictors is their *interpretability*.
The prediction is computed by checking the current node $v$:
- if $v$ is a leaf $\ell$ then the prediction is the label associated with $\ell$
- otherwise go to the $j$-th children where $j=f(x_i)$ where $f$ is the test function of the leaf $v$ and repeat the check

To construct a tree predictor:
- *initialization*: create T with only the root $\ell$ and let $S_{\ell}=S$. Let the label associated with the root be the most frequent label in $S_\ell$.
- *Main loop*: pick a leaf $\ell$ and replace it with an internal node $v$ creating two children $\ell'$ and $\ell''$. Pick an attribute $i$ and a test $f: \mathcal{X}_i \rightarrow{1,2}$, asociate the test to $v$ and partition $S_\ell$ in $S_{\ell'}=\{(x_t,y_t)\in S_\ell : f(x_{t,i})=1\}$ and $S_{\ell''}=\{(x_t,y_t)\in S_\ell : f(x_{t,i})=2\}$. Set the label of $\ell'$ as the most frequent label in $S_{\ell'}$ and do the same for $\ell''$ and $S_{\ell''}$