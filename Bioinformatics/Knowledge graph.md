permettono di unire varie fonti di dati in un'unico grafo facilmente consultabile e interpretabile che rappreseneta esplicitamente la conoscenza biologica e chimica rappresentata nelle fonti
hanno nodi di diverso tipo, diversi tipi di relazione e diversi tipi di proprieta base. 
Iniziamo identificando le source da cui estrarre i dati e identifichiamo l'ontologia del dominio. Ovvero una descrizione delle relazioni tra i dati (come per esempio un schema er)
Una volta creato un knowlenmge graph  dobbiamo creare un sistema di query e une, sistema di ai che utilizza queste query per fare predizioni.

# Data sources
le principali fonti di dati sono di tipo strutturato come per esempio quelli 
- pubblici: forniti da organizzazioni 
- interni: dati clinici prodotti da un ospedale o un laboratorio(lab data) o vecchi dati prodotti (legacy data) 
Vi sono anche dati non strutturati: #TODO trovare definizione

# Ontologie
sono descrizioni formali delle entita e concetti in un determinato dominio. Per esempio:
- un virus e' un agente infettivo
- le meningiti infettive sono un tipo di meningiti create da agenti infettivi
- le meningiti virali sono una sottoclasse di meningite infettive

**gene ontology**: vocabolario per descrivere i progotti dei geni in un organismo. E' organizzato come un grafo gerarchico aciclico direzionato e rappresenta i componenti cellulari, funzioni molecolari, processi biologici. Esistono due tipi di relazioni: 
- is-a
- part-of

sono state sviluppate varie entologie nell'ambito biomedico #todo aggiungere tabella slide

# Costruzione di un KG
dobbiamo tenere in considerazione l'utilizzo di diversi formati di rappresentazione dei dati (csv, json, ..) e diversi identifiers per rappresentare le stesse molecole o proteine (per alcune molecole non esiste uno standard quindi e' complesso stabilire quali notazioni sono corrispondenti).

Si possono usare varie tecniche per estrarre le informazioni dalle sources. I vantaggi di usare regole di mapping dichiarative e' la semplicita' di realizzazione dei programmi che estraggono i dati. Con queste regole posso creare una materializzazione di un KG in un singolo repository centralizzato con il vantaggio di dover creare una sola volta il grafo e limitarmi a fare query da esso, lo svantaggio e' quello di dover aggiornare la mia materializzazione ogni volta che i dati nelle mie source vengono modificati. In alternativa posso specificare un grafo virtuale: i dati stanno nei database originali ma vengono estratte e tradotte al momento. Questo e' piu complesso sia come sviluppo sia come tempo di esecuzione ma permette di accedere sempre a dati freschi.



# Data storage
possono essere utilizzati vari modi
- relational model
- database basati su grafi
- rdf database: triple oggetto, relation, modello
- altri modelli come monogDB, ...


# Deep learning
#TODO qui c'e' qualcosa in mezzo che mi sono perso

e' complesso applicare a un grafo di questo tipo perche estremamente coplesso sia come topologia (invece nelle immagini la struttura e' estremamente semplice con ogni pixel che ha un numero costante di vicini, e anche nel testo ogni lettera ha due vicini) sia come tipo di nodi e di relazioni (possono essere di vario tipo)

e' inoltre difficile creare un benchmark per capire quanto e' buona una soluzione

inoltre spesso questi algoritmi trovano correlazioni banali e gia conosciute dagli esperti

# graph database
sistema di data management che sfrutta il modello a grafo per rappresentaer informazioni
- grogerty graphs 
- RDF graphs
e' possibile passare dalla rappresentazione di un tipo all'altro
RDF ha semantica consistente e piu ricca ( #todo da capire)

### property graphs
- nodi: rappresenano gli oggetti del grafo e possono avere delle label
- relazioni: mettono in relazioni i nodi tramite il tipo di relazione e la direzione
- proprieta: sono proprieta appartenenti ai nodi
#todo immagine property graph

### RDF
resource description framework
e' uno standard W3C utilizzato per descrivere informazioni strutturate creato per essere letto e capito da computer ed e' il modo per rappresentare grafi in modo testuale.
#TODO finire di scrivere come funziona RDF