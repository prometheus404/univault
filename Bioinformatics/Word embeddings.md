
tecniche per convertire parole o, in generale, testi in vettori perche le macchine non sono in grado di interpretare la struttura delle parole ma quella binaria.

voglio assegnare una posizione in uno spazio tridimensionale a ogni parola facendo in modo che la vicinanza semantica tra due parole corrisponda a una vicinanza tra i punti nello spazio. Le parole possono avere varie relazioni tra di loro, in particolare analizziamo:
- similitudine -> non necessariamente sinonimi ma hanno ruoli simili nell'uso della frase e condividono qualche elemento nel significato (per esempio cane e gatto)
- relatedness -> sono parole che probabilmente si trovano nella stessa frase come per esempio macchina e benzina. 

Come faccio pero a incapsulare il significato di una parola in un vettore?
Un primo approccio e' quello di prendere tutte le parole che conosco, ordinarle e per ognuna mettere a 1 il bit corrispondente al numero della parola

|     | 1   | 2   | 3   | 4   | 5   |
| --- | --- | --- | --- | --- | --- |
|the| 1|0     |    0 | 0    | 0    | 0    |
| brown | 0| 1|0|0|0|
|fox| 0|0|1|0|0
|jumps| 0|0|0|1|0
|over| 0|0|0|0|1|

il problema di questo approccio e' che la vicinanza tra le parole e' sempre uguale e la dimensione del vettore diventa velocemente insostenibile.

## Document-Term Matrix
basandomi sull'ipotesi che parole vicinen si trovino in contesti vicini (vicino agli stessi gruppi di parole) provo un secondo approccio avendo una riga per ogni documento (contesto/fras) e una colonna per ogni parola e in ogni cella metto il numero di occorrenze della parola nel documento.
possiamo creare un vettore per ogni documento e confrontare la similitudine tra questi ultimi.
#todo aggiungere tabella e grafico dalle slide
da notare che la distanza euclidea non e' il miglior modo per calcolare la distanza tra questi due vettori, useremo invece il coseno per indicare la similitudine tra gli angoli.

Possiamo fare la stessa cosa con le parole creando vettori con le colonne e possiamo capire se le parolee sono collegate se appaiono con frequenza simile nei documenti.
## Term-context matrix
per la similitudine tra parole creo una matrice parole x parole in cui contiamo le volte che la parola 1 si trova nello stesso contesto della parola 2

## Word2vec
Il problema delle implementazioni precedenti e' la dimensione. L'idea di questo approccio e' avere una rete neurale che crea i vettori sotto forma di supervised task. Da notare che le annotazioni di questo task sono derivate dal Term-context matrix quindi non devono essere create manualmente (self-supervision). Per creare la rete ci servono pero anche la loss function e la struttura della rete.
### Training examples
Prendo una sliding window di dimensione fissata e per ogni parola del testo genero un training sample con le altre parole presenti nella finestra.
### Network structure
formato da input layer, un singolo hidden layer e un output layer. All'input layer viene data la one-hot encoding della parola in ingresso e in uscita verrà restituita la one-hot encoding della parola vicina. Quindi il vettore di ingresso e di uscita hanno la dimensione del vocabolario mentre quello interno ha dimensione $d$ a scelta ed e' la chiave dell'algoritmo perché il suo valore non e' altro che la rappresentazione che stiamo cercando.
#todo aggiungere formule slide
Siccome nell'input un solo elemento e' pari a 1 (elemento $i$esimo) il risultato del primo passaggio e' la $i$esima riga della matrice. La matrice w si chiama Embedding matrix e non e' altro che un array contenente le rappresentazioni vettoriali di tutte le parole del vocabolario. La prima matrice rappresenta la parola come centro della finestra, la seconda rappresenta la parola come contesto. La probabilità che una parola sia nelle vicinanze di una parola centrale e' il softmax #rivedere. Stesso concetto dell'autoencoder con la differenza che invece che cercare di trovare la stessa cosa come output vogliamo trovare la parola vicina, in questo modo non codifichiamo la parola in una dimensione minore ma codifichiamo il rapporto tra parole.
### Loss function
per ogni parola voglio massimizzare la probabilita di trovarla nell'intorno della parola centrale #rivedere . 
#todo aggiungere formula.
Siccome questa e' un problema complesso minimizzo invece il complementare. 
#todo aggiungere seconda formula.
Il problema e' che anche questa e' enorme perché prende in considerazione tutte le parole del vocabolario.

## Word2Vec 2.0
due miglioramenti rispetto al precedente:
- invece di usare tutte le parole faccio solamente un sampling e confronto quelle. Seleziono non casualmente ma in base alla frequenza perché parole come "is" saranno presenti spesso ma non mi aiutano particolarmente.
- cambiare la funzione obiettivo usando il negative sampling: 
	- prendiamo 1 possibile coppia e k esempi negativi secondo la probabilità proporzionale alla sua freqenza elevata a 3/4. In questo modo le parole meno frequenti saranno sampled piu spesso.
	- ...

Per ogni esempio positivo formato da una parola e una vicina la nuova loss function con k esempi negativi:
#todo formula slide

nelle slide e' presente il link alla implementazione di word2vec
