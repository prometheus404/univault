# embedding dei grafi
neighborhood aggregation anche chiamata in modo improprio neighborhood convolution( non e' davvero la convolution matematica)

vogliamo capire come aggregare i nodi. Un modo e' fare la media degli input dei nodi b, c, d e applicare una neural network 
#TODO  mettere immagine del network e della formula

propagando questa aggregazione per ogni livello tramite una appropriate function otteniamo un embedding finale

possiamo usare la stessa loss function usata da word2vec. In questo modo possiamo ottenere embedding in modo unsupervised.

un altro approccio e' allenare direttamente il modello per un supervised task usando una cross-entropy loss function

una volta allenato il neural network possiamo usare i layer che abbiamo allenato per predire anche i nodi che non sono stati usati durante la fase di embedding. da notare che questo non possiamo farlo con metodi basati sulle random walk.
Possiamo inoltre allenare su un grafo (per esempio protein-protein interaction negli umani) e generalizzarlo per un altro grafico (per esempio protein-protein interaction nei topi) ovviamente questo ha senso finche i due grafi hanno una semantica simile.
E' comodo anche nel caso arrivi un nuovo nodo, in tal caso non devo ricalcolare l'embedding da capo ma mi basta semplicemente calcolarlo al volo solamente per il nuovo nodo.

## GraphSAGE
un'idea piu generica e' quella di usare una aggregation function dei vicini invece di fare una media e invece di sommare questi embedding con il self embedding i due vettori vengono concatenati

### Alcune varianti
- Mean
- Pool
- LSTM -> le [lstm](https://en.wikipedia.org/wiki/Long_short-term_memory) sono delle recurrent neural network nelle quali e' possibile ottenere una memoria degli output degli step precedenti 
#TODO aggiungere le formule delle varianti

---
----
in generale i layer sono limitati perche altrimenti i modelli che creo sono soggetti a overfitting
una soluzione e' usare lo stesso neural network con gli stessi parametri su tutti i layer del modello (il problema e' che potrebbe essere underfitting)

un altro problema e' quello del vanishing/exploding gradient -> quindi l'apprendimento tende a rallentare molto o essere troppo veloce e far rimbalzare il risultato tra punti vicini al minmo


## Message-Pasing Neural Networks
1. prendere messaggi dai vicini allo step k ( #todo copiare formula )
2. aggiornare lo stato del nodo (funzione generica di update come LSTM o GRU)

## nighborhood attention
possiamo dare un peso maggiore a un vicino rispetto a un altro tramite una softmax function

# subgraph embedding
invece di fare l'embedding del singolo nodo all'interno del grafo si potrebbe voler ottenere un embedding di un sottografo del grafo originale. Questo potrebbe essere utile per classificare le molecole in base al grafo che descrive la loro struttura. 

Un primo approccio potrebbe essere sommare o fare la media degli embedding dei singoli nodi, per esempio facendo la media dell'embedding degli atomi di una molecola per ottenere l'embedding della molecola stessa.

Un secondo approccio potrebbe essere crerare un nodo virtuale che rappresenta il sottografo, connesso a tutti gli elementi del sottografo e usare un normale algoritmo di embedding e vedere come viene embeddato questo nodo virtuale

Un altro agoritmo e' 
1. usare GNN sul grafo per avere gli embeddings
2. usare digli algoritmi di clustering 
3. ripetere ricorsivamente considerando i cluster creati come nodi

---
# Info esame
30 maggio alle 14.30
l'esame potrebbe essere remoto e mandera una mail per info
presentare e discutere i risultati
fare grafici per far vedere
chiedere a jessica per altre info
prima parte di esposizione, seconda parte generale su quello che e' stato spiegato
domande generiche e se spieghiamo molto bene la prima parte la seconda sara piu corta
possono esserci domande anche su quello spiegato da altri tipi
solitamente non entra nei dettagli matematici ma e' possibile nel caso voglia chiarificare una risposta poco chiara e solitamente se le deve fare le fa a studenti di informatica (yeeee)
se si vuole fare il paper mandare email
se si fa l'esame di programmazione mandare risultati una settimana prima a lui e soprattutto a jessika
ricordarsi di iscriversi a giugno per verbalizzare quello di maggio